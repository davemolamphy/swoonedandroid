package com.panaceasoft.pshotels.ui.user;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.MainActivity;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentUserRegisterBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;
import com.panaceasoft.pshotels.viewobject.User;

/**
 * UserRegisterFragment
 */
public class UserRegisterFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentUserRegisterBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentUserRegisterBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_register, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }


    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().loginButton.setOnClickListener(view -> {

            if (connectivity.isConnected()) {

                if (getActivity() instanceof MainActivity) {
                    navigationController.navigateToUserLogin((MainActivity) getActivity());
                } else {

                    navigationController.navigateToUserLoginActivity(getActivity());

                    try {
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    } catch (Exception e) {
                        Utils.psErrorLog("Error in closing activity.", e);
                    }
                }
            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }

        });

        binding.get().registerButton.setOnClickListener(view -> registerUser());
    }


    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

    }

    //endregion


    //region Private Methods

    private void updateRegisterBtnStatus() {
        if (userViewModel.isLoading) {
            binding.get().registerButton.setText(getResources().getString(R.string.message__loading));
        } else {
            binding.get().registerButton.setText(getResources().getString(R.string.register));
        }
    }

    private void registerUser() {

        Utils.hideKeyboard(getActivity());

        String userName = binding.get().nameEditText.getText().toString().trim();
        if (userName.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__blank_name), Toast.LENGTH_SHORT).show();
            return;
        }

        String userEmail = binding.get().emailEditText.getText().toString().trim();
        if (userEmail.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__blank_email), Toast.LENGTH_SHORT).show();
            return;
        }

        String userPassword = binding.get().passwordEditText.getText().toString().trim();
        if (userPassword.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__blank_password), Toast.LENGTH_SHORT).show();
            return;
        }


        userViewModel.isLoading = true;
        prgDialog.get().show();
        updateRegisterBtnStatus();

        userViewModel.registerUser(new User("",
                "",
                "",
                userName,
                userEmail,
                "",
                userPassword,
                "",
                "",
                "",
                "",
                "",
                "",
                "")).observe(this, listResource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data" + listResource.message + listResource.toString());


                if (listResource.message != null && !listResource.message.equals("")) {
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                } else {
                    // Update the data
                    Toast.makeText(getContext(), getString(R.string.message__register_success), Toast.LENGTH_SHORT).show();

                    try {
                        if (getActivity() != null) {
                            pref.edit().putString("user_id", listResource.data.user_id).apply();
                            pref.edit().putString("user_name", listResource.data.user.user_name).apply();
                            pref.edit().putString("user_email", listResource.data.user.user_email).apply();
                        }

                    } catch (NullPointerException ne) {
                        Utils.psErrorLog("Null Pointer Exception.", ne);
                    } catch (Exception e) {
                        Utils.psErrorLog("Error in getting notification flag data.", e);
                    }


                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();

                    if (getActivity() instanceof MainActivity) {
                        navigationController.navigateToUserProfile((MainActivity) getActivity());
                    } else {
                        try {
                            getActivity().finish();
                        } catch (Exception e) {
                            Utils.psErrorLog("Error in closing parent activity.", e);
                        }
                    }
                }

            } else if (listResource != null && listResource.message != null) {
                Utils.psLog("Message from server.");
                Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                userViewModel.isLoading = false;
                prgDialog.get().cancel();
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");

            }

            updateRegisterBtnStatus();

        });

    }

    //endregion


}

