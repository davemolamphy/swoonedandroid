package com.panaceasoft.pshotels.ui.inquiry;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentInquiryBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.inquiry.InquiryViewModel;

/**
 * Created by Panacea-Soft on 11/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */


public class InquiryFragment extends PSFragment {

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private InquiryViewModel inquiryViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentInquiryBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentInquiryBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_inquiry, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);
        return binding.get().getRoot();
    }


    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().btnSubmit.setOnClickListener(view -> {

            if (connectivity.isConnected()) {

                String inqTitle = binding.get().inqTitle.getText().toString().trim();
                String inqMsg = binding.get().contactDescEditText.getText().toString().trim();
                String contactName = binding.get().contactNameTextInput.getText().toString().trim();
                String contactEmail = binding.get().contactEmailTextInput.getText().toString().trim();
                String contactPhone = binding.get().contactPhoneTextInput.getText().toString().trim();

                if (inqTitle.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__blank_title), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (inqMsg.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__blank_message), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (contactName.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__blank_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (contactEmail.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__blank_email), Toast.LENGTH_SHORT).show();
                    return;
                }


                if (!inquiryViewModel.isLoading) {

                    inquiryViewModel.inqName = inqTitle;
                    inquiryViewModel.inqDesc = inqMsg;
                    inquiryViewModel.contactName = contactName;
                    inquiryViewModel.contactEmail = contactEmail;
                    inquiryViewModel.contactPhone = contactPhone;
                    doSubmit();
                }

            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void initViewModels() {
        inquiryViewModel = ViewModelProviders.of(this, viewModelFactory).get(InquiryViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        if (getActivity() != null) {
            try {

                inquiryViewModel.hotelId = getActivity().getIntent().getStringExtra("hotelId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }

            try {

                inquiryViewModel.roomId = getActivity().getIntent().getStringExtra("roomId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }
        }
    }

    private void doSubmit() {

        prgDialog.get().show();

        inquiryViewModel.postInquiry(
                Config.API_KEY,
                inquiryViewModel.hotelId,
                inquiryViewModel.roomId,
                inquiryViewModel.userId,
                inquiryViewModel.inqName,
                inquiryViewModel.inqDesc,
                inquiryViewModel.contactName,
                inquiryViewModel.contactEmail,
                inquiryViewModel.contactPhone
        );

        inquiryViewModel.getLoadingStatus().observe(this, submitStatus -> {

            if (submitStatus != null) {

                // error
                String error = submitStatus.getErrorMessageIfNotHandled();

                if (error != null) {

                    Utils.psLog(error);

                    prgDialog.get().cancel();
                } else {

                    if (!submitStatus.isRunning()) {

                        // success
                        binding.get().inqTitle.setText("");
                        binding.get().contactNameTextInput.setText("");
                        binding.get().contactEmailTextInput.setText("");
                        binding.get().contactPhoneTextInput.setText("");
                        binding.get().contactDescEditText.setText("");

                        Toast.makeText(getContext(), getString(R.string.message__message_sent), Toast.LENGTH_SHORT).show();

                        prgDialog.get().cancel();
                    }
                }
            }

            binding.get().executePendingBindings();
        });
    }
}