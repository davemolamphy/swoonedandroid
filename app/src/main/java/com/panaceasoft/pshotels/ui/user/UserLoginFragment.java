package com.panaceasoft.pshotels.ui.user;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.panaceasoft.pshotels.MainActivity;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentUserLoginBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;
import com.panaceasoft.pshotels.viewobject.User;


/**
 * UserLoginFragment
 */
public class UserLoginFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentUserLoginBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;


    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentUserLoginBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_login, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().loginButton.setOnClickListener(view -> {

            Utils.hideKeyboard(getActivity());

            if (connectivity.isConnected()) {
                String userEmail = binding.get().emailEditText.getText().toString().trim();
                String userPassword = binding.get().passwordEditText.getText().toString().trim();

                Utils.psLog("Email " + userEmail);
                Utils.psLog("Password " + userPassword);

                if (userEmail.equals("")) {
                    Toast.makeText(getContext(), R.string.error_message__blank_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (userPassword.equals("")) {
                    Toast.makeText(getContext(), R.string.error_message__blank_password, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!userViewModel.isLoading) {

                    updateLoginBtnStatus();

                    doSubmit(userEmail, userPassword);

                }
            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }

        });

        binding.get().registerButton.setOnClickListener(view -> {

            if (getActivity() instanceof MainActivity) {
                navigationController.navigateToUserRegister((MainActivity) getActivity());
            } else {

                navigationController.navigateToUserRegisterActivity(getActivity());

                try {
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                } catch (Exception e) {
                    Utils.psErrorLog("Error in closing activity.", e);
                }
            }
        });

        binding.get().forgotPasswordButton.setOnClickListener(view -> {

            if (getActivity() instanceof MainActivity) {
                navigationController.navigateToUserForgotPassword((MainActivity) getActivity());
            } else {

                navigationController.navigateToUserForgotPasswordActivity(getActivity());

                try {
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                } catch (Exception e) {
                    Utils.psErrorLog("Error in closing activity.", e);
                }

            }
        });
    }

    private void updateLoginBtnStatus() {
        if (userViewModel.isLoading) {
            binding.get().loginButton.setText(getResources().getString(R.string.message__loading));
        } else {
            binding.get().loginButton.setText(getResources().getString(R.string.login));
        }
    }

    private void doSubmit(String email, String password) {

        prgDialog.get().show();
        userViewModel.setUserLogin(new User("",
                "",
                "",
                "",
                email,
                "",
                password,
                "",
                "",
                "",
                "",
                "",
                "",
                ""));

        userViewModel.isLoading = true;


    }


    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {
        userViewModel.getUserLoginStatus().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data" + listResource.message + listResource.toString());


                if (listResource.message != null && !listResource.message.equals("")) {
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                } else {
                    // Update the data

                    try {

                        if (getActivity() != null) {
                            pref.edit().putString("user_id", listResource.data.user_id).apply();
                            pref.edit().putString("user_name", listResource.data.user.user_name).apply();
                            pref.edit().putString("user_email", listResource.data.user.user_email).apply();
                        }

                    } catch (NullPointerException ne) {
                        Utils.psErrorLog("Null Pointer Exception.", ne);
                    } catch (Exception e) {
                        Utils.psErrorLog("Error in getting notification flag data.", e);
                    }

                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();

                    if (getActivity() instanceof MainActivity) {
                        ((MainActivity) getActivity()).setToolbarText(((MainActivity) getActivity()).binding.toolbar, getString(R.string.profile));
                        navigationController.navigateToUserProfile((MainActivity) getActivity());

                    } else {
                        try {
                            getActivity().finish();
                        } catch (Exception e) {
                            Utils.psErrorLog("Error in closing parent activity.", e);
                        }
                    }
                }


            } else if (listResource != null && listResource.message != null) {
                Utils.psLog("Message from server.");
                Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                userViewModel.isLoading = false;
                prgDialog.get().cancel();
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");

            }

            updateLoginBtnStatus();

        });
    }

    //endregion


}
