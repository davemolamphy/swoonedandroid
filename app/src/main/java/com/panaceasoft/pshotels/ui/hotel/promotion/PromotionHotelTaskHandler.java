package com.panaceasoft.pshotels.ui.hotel.promotion;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.PromotionHotelLoadingHolder;

public class PromotionHotelTaskHandler extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public PromotionHotelTaskHandler(HotelRepository repository) {
        super();
        this.repository = repository;
    }

    public void getNextPage(PromotionHotelLoadingHolder nextPageHolder) {

        if(nextPageHolder == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPagePromotionHotels(nextPageHolder);
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}
