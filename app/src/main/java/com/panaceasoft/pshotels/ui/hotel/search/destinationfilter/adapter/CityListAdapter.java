package com.panaceasoft.pshotels.ui.hotel.search.destinationfilter.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.CityViewBinding;
import com.panaceasoft.pshotels.ui.common.DataBoundListAdapter;
import com.panaceasoft.pshotels.ui.common.DataBoundViewHolder;
import com.panaceasoft.pshotels.utils.Objects;
import com.panaceasoft.pshotels.viewobject.City;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class CityListAdapter extends DataBoundListAdapter<City, CityViewBinding> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final CityClickCallback callback;
    private int lastPosition = -1;



    public CityListAdapter( android.databinding.DataBindingComponent dataBindingComponent,
                             CityClickCallback callback) {

        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;

    }

    @Override
    protected CityViewBinding createBinding(ViewGroup parent) {

        CityViewBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.city_view, parent, false,
                        dataBindingComponent);
        binding.getRoot().setOnClickListener(v -> {
            City city = binding.getCity();
            if (city != null && callback != null) {
                callback.onClick(city);
            }
        });
        return binding;

    }

    // For general animation
    @Override
    public void bindView(DataBoundViewHolder<CityViewBinding> holder, int position) {
        super.bindView(holder, position);

        setAnimation(holder.itemView, position);
    }

    @Override
    protected void bind(CityViewBinding binding, City item) {
        binding.setCity(item);

    }

    @Override
    protected boolean areItemsTheSame(City oldItem, City newItem) {
        return Objects.equals(oldItem.city_id, newItem.city_id);
    }

    @Override
    protected boolean areContentsTheSame(City oldItem, City newItem) {
        return Objects.equals(oldItem.city_id, newItem.city_id);
    }

    public interface CityClickCallback {
        void onClick(City city);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }else{
            lastPosition = position;
        }
    }

}
