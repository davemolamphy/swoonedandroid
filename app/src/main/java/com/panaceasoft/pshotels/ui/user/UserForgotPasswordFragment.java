package com.panaceasoft.pshotels.ui.user;


import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.MainActivity;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentUserForgotPasswordBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;

/**
 * UserForgotPasswordFragment
 */
public class UserForgotPasswordFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentUserForgotPasswordBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;


    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentUserForgotPasswordBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_forgot_password, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);


        //fadeIn Animation
        fadeIn(binding.get().getRoot());


        binding.get().loginButton.setOnClickListener(view -> {

            if (getActivity() instanceof MainActivity) {
                navigationController.navigateToUserLogin((MainActivity) getActivity());
            } else {

                navigationController.navigateToUserLoginActivity(getActivity());

                try {
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                } catch (Exception e) {
                    Utils.psErrorLog("Error in closing activity.", e);
                }
            }

        });

        binding.get().forgotPasswordButton.setOnClickListener(view -> {
            if (connectivity.isConnected()) {
                forgotPassword();
            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

    }

    //endregion


    //region Private Methods

    private void updateForgotBtnStatus() {
        if (userViewModel.isLoading) {
            binding.get().forgotPasswordButton.setText(getResources().getString(R.string.message__loading));
        } else {
            binding.get().forgotPasswordButton.setText(getResources().getString(R.string.forgot_password));
        }
    }

    private void forgotPassword() {

        Utils.hideKeyboard(getActivity());

        String email = binding.get().emailEditText.getText().toString().trim();
        if (email.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__blank_email), Toast.LENGTH_SHORT).show();
            return;
        }

        userViewModel.isLoading = true;
        prgDialog.get().show();
        updateForgotBtnStatus();

        userViewModel.forgotPassword(email).observe(this, listResource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data" + listResource.message + listResource.toString());


                if (listResource.message != null && !listResource.message.equals("")) {
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                } else {
                    // Update the data
                    Toast.makeText(getContext(), listResource.data.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                }

            } else if (listResource != null && listResource.message != null) {
                Utils.psLog("Message from server.");
                Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                userViewModel.isLoading = false;
                prgDialog.get().cancel();
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }

            updateForgotBtnStatus();

        });
    }

    //endregion


}
