package com.panaceasoft.pshotels.ui.welcome.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Panacea-Soft on 17/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */
public class WelcomeViewPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private int[] welcomeScreen;
    private Context context;

    public WelcomeViewPagerAdapter() {

    }

    public WelcomeViewPagerAdapter(int[] welcomeScreen, Context context) {

        this.welcomeScreen = welcomeScreen;
        this.context = context;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        assert layoutInflater != null;
        View view = layoutInflater.inflate(welcomeScreen[position], container, false);
        container.addView(view);
        return view;

    }

    @Override
    public int getCount() {
        return welcomeScreen.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view,@NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
