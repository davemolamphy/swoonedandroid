package com.panaceasoft.pshotels.ui.gallery.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ImageItemBinding;
import com.panaceasoft.pshotels.ui.common.DataBoundListAdapter;
import com.panaceasoft.pshotels.utils.Objects;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Image;

/**
 * Created by Panacea-Soft on 12/6/17.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class GalleryAdapter extends DataBoundListAdapter<Image, ImageItemBinding> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final ImageClickCallback callback;

    public GalleryAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                          ImageClickCallback callback) {
        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
    }

    @Override
    protected ImageItemBinding createBinding(ViewGroup parent) {
        ImageItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.image_item, parent, false,
                        dataBindingComponent);

        binding.getRoot().setOnClickListener(v -> {
            Image image = binding.getImage();
            if (image != null && callback != null) {
                Utils.psLog("Clicked Image"+ image.img_desc);

                callback.onClick(image);
            }
        });
        return binding;
    }

    @Override
    protected void bind(ImageItemBinding binding, Image item) {
        binding.setImage(item);
    }

    @Override
    protected boolean areItemsTheSame(Image oldItem, Image newItem) {
        return Objects.equals(oldItem.img_id, newItem.img_id);
    }

    @Override
    protected boolean areContentsTheSame(Image oldItem, Image newItem) {
        return Objects.equals(oldItem.img_id, newItem.img_id);
    }

    public interface ImageClickCallback {
        void onClick(Image item);
    }
}