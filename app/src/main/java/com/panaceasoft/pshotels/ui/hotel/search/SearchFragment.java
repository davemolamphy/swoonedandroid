package com.panaceasoft.pshotels.ui.hotel.search;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentSearchBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.searchlog.SearchLogViewModel;
import com.panaceasoft.pshotels.viewobject.SearchLog;

import static com.panaceasoft.pshotels.utils.Utils.RESULT_OK;

/**
 * SearchFragment
 */
public class SearchFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    @VisibleForTesting
    AutoClearedValue<FragmentSearchBinding> binding;

    private String selectedHotelStars = "";

    private Integer selectedPriceMin = 0;
    private Integer selectedPriceMax = 0;
    private Integer selectedRatings = 0;
    private String cityId = "";
    private String cityName = "";
    private String hotelName = "";
    private String filterByInfoType = "";

    private SearchLogViewModel searchLogViewModel;

    private SearchLog searchLog;


    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentSearchBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {

            if(requestCode == 0) {

                if (resultCode == RESULT_OK) {

                    selectedHotelStars = data.getStringExtra("hotelStars");
                    selectedPriceMin = data.getIntExtra("priceMin", 0);
                    selectedPriceMax = data.getIntExtra("priceMax", 0);
                    selectedRatings = data.getIntExtra("guestRatings", 0);

                    Utils.psLog("selectedHotelStars >>>>>>  " + selectedHotelStars);
                    Utils.psLog("selectedPriceMin >>>>>>  " + selectedPriceMin);
                    Utils.psLog("selectedPriceMax >>>>>>  " + selectedPriceMax);
                    Utils.psLog("selectedRatings >>>>>>  " + selectedRatings);

                    if(selectedHotelStars.equals("")) {
                        selectedHotelStars = "All";
                    }

                    String filters;
                    if(selectedRatings == 0) {
                        filters = getString(R.string.filter__stars) + selectedHotelStars + " " + getString(R.string.filter__price) + " " + selectedPriceMin + " - " + selectedPriceMax + " " + getString(R.string.filter__review_all);
                    } else {
                        filters = getString(R.string.filter__stars) + selectedHotelStars + " " + getString(R.string.filter__price) + " " + selectedPriceMin + " - " + selectedPriceMax + " " + getString(R.string.filter__review) + " " + selectedRatings + " +";
                    }

                    binding.get().txtFilters.setText(filters);

                }
            }else if(requestCode == 1) {
                cityId = data.getStringExtra("cityId");
                cityName = data.getStringExtra("cityName");

                // set city to ui
                updateCityName();
            }

        } catch (Exception e) {
            Utils.psErrorLog("Error In onActivityResult From SearchFragment",e);
        }
    }

    private void updateCityName() {
        this.binding.get().findYourLocationTextView.setText(cityName);
    }

    @Override
    protected void initUIAndActions() {

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        // Search Action
        binding.get().searchButton.setOnClickListener(view -> {
            if(checkValidation(cityId)) {
                Utils.psLog("Open Search Result Activity.");

                if(!connectivity.isConnected()) {
                    Toast.makeText(getContext(), getString(R.string.error_message__no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                hotelName = binding.get().hotelNameEditText.getText().toString();

                searchLogViewModel.insertSearchLog(new SearchLog(
                        cityId,
                        cityName,
                        hotelName,
                        selectedHotelStars,
                        selectedPriceMin,
                        selectedPriceMax,
                        selectedRatings
                ));

                navigationController.navigateToSearchResultListActivity(
                        getActivity(),
                        selectedHotelStars,
                        selectedPriceMin,
                        selectedPriceMax,
                        selectedRatings,
                        cityId,
                        hotelName,
                        filterByInfoType
                );
            }
        });

        // Filters
        binding.get().txtFilters.setOnClickListener(view -> {
            Utils.psLog("Open Filters Page");
            navigationController.navigateToHotelFilterActivity(getActivity(), selectedHotelStars, selectedPriceMin, selectedPriceMax, selectedRatings);
        });

        // Location
        binding.get().findYourLocationTextView.setOnClickListener(view -> {
            Utils.psLog("Open Room Filter Activity.");
            navigationController.navigateToCityListActivity(getActivity());

        });

        binding.get().searchLogLayout.setOnClickListener(view -> {
            if(checkValidation(searchLog.cityId)) {
                Utils.psLog("Open Search Result Activity.");

                if(!connectivity.isConnected()) {
                    Toast.makeText(getContext(), getString(R.string.error_message__no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                navigationController.navigateToSearchResultListActivity(
                        getActivity(),
                        searchLog.stars,
                        searchLog.minPrice,
                        searchLog.maxPrice,
                        searchLog.rating,
                        searchLog.cityId,
                        searchLog.hotelName,
                        filterByInfoType
                );
            }
        });
    }

    private boolean checkValidation(String cityId) {
        if(cityId.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__select_location), Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }
    }

    @Override
    protected void initViewModels() {

        searchLogViewModel = ViewModelProviders.of(this, viewModelFactory).get( SearchLogViewModel.class );
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        loadSearchLog();
    }

    //endregion


    @Override
    public void onResume() {
        super.onResume();

        //loadSearchLog();
    }

    private void loadSearchLog()
    {
        searchLogViewModel.setSearchLogObj("searchLog");
        searchLogViewModel.getSearchLogLiveData().observe(this,resource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (resource != null ) {

                Utils.psLog("Got Data Of search Log." + resource.toString());

                searchLog = resource;

                fadeIn(binding.get().getRoot());

                binding.get().searchLogLayout.setVisibility(View.VISIBLE);

                binding.get().setSearchLog(resource);

                binding.get().executePendingBindings();
            }else {

                //noinspection ConstantConditions
                Utils.psLog("No Data of search log.");

                binding.get().searchLogLayout.setVisibility(View.GONE);
            }
        });
    }
}
