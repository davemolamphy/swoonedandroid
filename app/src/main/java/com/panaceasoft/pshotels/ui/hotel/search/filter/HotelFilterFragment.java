package com.panaceasoft.pshotels.ui.hotel.search.filter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentRoomFilterBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.RangeSeekBar;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotelInfo.HotelFeatureViewModel;
import com.panaceasoft.pshotels.viewmodel.price.PriceViewModel;

import java.util.ArrayList;

/**
 * Created by Panacea-Soft on 4/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

public class HotelFilterFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private PriceViewModel priceViewModel;

    private int userSelectedRating = 0;
    private int userSelectedPriceMin = 0;
    private int userSelectedPriceMax = 0;
    private int defaultPrice = 500;
    private int maxPriceFromServer = 0;
    private String currencySymbol = "";

    private String userSelectedFeatures = "";
    private String userSelectedStars = "";

    private boolean isOnePressed = false;
    private boolean isTwoPressed = false;
    private boolean isThreePressed = false;
    private boolean isFourPressed = false;
    private boolean isFivePressed = false;

    private String selectedStars = "";
    private String selectedOne = "";
    private String selectedTwo = "";
    private String selectedThree = "";
    private String selectedFour = "";
    private String selectedFive = "";


    private Boolean oneStarsSelected = false;
    private Boolean twoStarsSelected = false;
    private Boolean threeStarsSelected = false;
    private Boolean fourStarsSelected = false;
    private Boolean fiveStarsSelected = false;

    private String cityId = "";
    private ArrayList<String> featuresId = new ArrayList<>();
    private ArrayList<String> selectedFeaturesId = new ArrayList<>();

    AutoClearedValue<RangeSeekBar> rangeSeekBarAutoClearedValue;

    AutoClearedValue<ChipCloud> chipCloudAutoClearedValue;

    private int isFound = 0;
    private String[] userSelectedFeaturesArray;
    private int isClickClear = 0;


    @VisibleForTesting
    AutoClearedValue<FragmentRoomFilterBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    private HotelFeatureViewModel hotelFeatureViewModel;
    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentRoomFilterBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_filter, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);


        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        initPriceRangeBar(defaultPrice);


        binding.get().ratingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.get().txtRating.setText(getGuestRatingText(i));
                userSelectedRating = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        binding.get().oneStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("One");

            if (!isOnePressed) {
                clickStars("one");
            } else {
                resetStars("one");
            }

        });


        binding.get().twoStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Two");

            if (!isTwoPressed) {
                clickStars("two");
            } else {
                resetStars("two");
            }

        });


        binding.get().threeStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Three");

            if (!isThreePressed) {
                clickStars("three");
            } else {
                resetStars("three");
            }

        });


        binding.get().fourStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Four");

            if (!isFourPressed) {
                clickStars("four");
            } else {
                resetStars("four");
            }

        });


        binding.get().fiveStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Five");

            if (!isFivePressed) {
                clickStars("five");
            } else {
                resetStars("five");
            }

        });

        // Hotel Feature By City Id

        ChipCloud chipCloud = binding.get().chipCloud;

        chipCloudAutoClearedValue = new AutoClearedValue<>(this, chipCloud);
        chipCloudAutoClearedValue.get().setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                Utils.psLog("Selected Tag " + i);
                Utils.psLog("Selected FeatureId : " + featuresId.get(i));
                Utils.psLog("selectedFeaturesId size : " + selectedFeaturesId.size());

                //Duplicate Checking
                if (!selectedFeaturesId.contains(featuresId.get(i))) {
                    //If not duplicate, insert selected array
                    selectedFeaturesId.add(featuresId.get(i));
                }
            }

            @Override
            public void chipDeselected(int i) {
                Utils.psLog("Deselected Tag " + i);
                int removeIndex = 0;
                if (selectedFeaturesId.size() > 0) {
                    for (int p = 0; p < selectedFeaturesId.size(); p++) {
                        if (selectedFeaturesId.get(p).equals(featuresId.get(i))) {
                            removeIndex = p;
                            break;
                        }
                    }
                }

                selectedFeaturesId.remove(removeIndex);

            }
        });


        //Tag End

        binding.get().okButton.setOnClickListener(view -> {
            Utils.psLog("Click Ok Button");

            selectedStars = selectedOne + "," + selectedTwo + "," + selectedThree + "," + selectedFour + "," + selectedFive;

            String[] split = selectedStars.split(",");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < split.length; i++) {


                if (!split[i].isEmpty()) {

                    sb.append(split[i]);

                    if (i != split.length - 1) {
                        sb.append("-");
                    }
                }

            }

            userSelectedStars = sb.toString();


            Utils.psLog("Selected  Stars : " + userSelectedStars);

            Utils.psLog("Min Price : " + userSelectedPriceMin + "| Max Price : " + userSelectedPriceMax);

            Utils.psLog("Rating : " + userSelectedRating);


            if (userSelectedFeaturesArray.length > 0) {
                userSelectedFeatures = "";
            }

            for (int k = 0; k < selectedFeaturesId.size(); k++) {
                Utils.psLog("Selected Feature Id  : " + selectedFeaturesId.get(k));
                if (k == selectedFeaturesId.size() - 1) {
                    userSelectedFeatures += selectedFeaturesId.get(k);
                } else {
                    userSelectedFeatures = userSelectedFeatures + selectedFeaturesId.get(k) + "-";
                }
            }

            Utils.psLog("userSelectedFeatures  : " + userSelectedFeatures);

            //Passing Data Back to another activity
            passBackData(userSelectedStars, userSelectedPriceMin, userSelectedPriceMax, userSelectedRating, userSelectedFeatures);

        });

        binding.get().clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Utils.psLog("Click Clear Button");

                //Reset Price Range
                if (rangeSeekBarAutoClearedValue.get() != null) {

                    if (maxPriceFromServer == 0) {
                        rangeSeekBarAutoClearedValue.get().setSelectedMaxValue(defaultPrice);
                        userSelectedPriceMax = defaultPrice;
                    } else {
                        rangeSeekBarAutoClearedValue.get().setSelectedMaxValue(maxPriceFromServer);
                        userSelectedPriceMax = maxPriceFromServer;
                    }
                    rangeSeekBarAutoClearedValue.get().setSelectedMinValue(0);
                }
                userSelectedPriceMin = 0;

                binding.get().txtPriceRange.setText(getPriceRangeText());


                //Reset User Rating
                binding.get().ratingSeekBar.setProgress(0);
                binding.get().txtRating.setText(getString(R.string.select_rating));

                //Reset Stars
                resetStars("one");
                resetStars("two");
                resetStars("three");
                resetStars("four");
                resetStars("five");

                //Reset ChipCloud
                chipCloud.removeAllViews();
                isClickClear = 1;
                hotelFeatureViewModel.setHotelFeaturesByCityIdObj(cityId);

                //Reset Select Features
                selectedFeaturesId.clear();


            }

        });

        binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);

    }

    @Override
    protected void initViewModels() {

        hotelFeatureViewModel = ViewModelProviders.of(this, viewModelFactory).get(HotelFeatureViewModel.class);
        priceViewModel = ViewModelProviders.of(this, viewModelFactory).get(PriceViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {
        if (getActivity() != null) {
            try {

                userSelectedFeatures = getActivity().getIntent().getStringExtra("userSelectedFeatures");
                userSelectedPriceMin = Integer.parseInt(getActivity().getIntent().getStringExtra("userSelectedPriceMin"));
                userSelectedPriceMax = Integer.parseInt(getActivity().getIntent().getStringExtra("userSelectedPriceMax"));
                userSelectedRating = Integer.parseInt(getActivity().getIntent().getStringExtra("userSelectedRating"));
                cityId = String.valueOf(getActivity().getIntent().getStringExtra("cityId"));
                userSelectedStars = getActivity().getIntent().getStringExtra("selectedHotelStars");

                String[] split = userSelectedStars.split("-");


                for (String aSplit : split) {

                    if (!aSplit.isEmpty()) {

                        switch (aSplit) {
                            case "1":
                                oneStarsSelected = true;
                                break;
                            case "2":
                                twoStarsSelected = true;
                                break;
                            case "3":
                                threeStarsSelected = true;
                                break;
                            case "4":
                                fourStarsSelected = true;
                                break;
                            case "5":
                                fiveStarsSelected = true;
                                break;
                        }

                    }

                }


            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : userSelectedStars.", e);

            }

        }

        // Max Price

        priceViewModel.setPriceObj("max price");
        priceViewModel.getPriceData().observe(this, resource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (resource != null) {

                if (resource.data != null) {
                    Utils.psLog("Max Price : " + resource.data.max_price);


                    if (rangeSeekBarAutoClearedValue.get() != null) {

                        maxPriceFromServer = Integer.parseInt(resource.data.max_price);
                        currencySymbol = resource.data.currency_symbol;
                        initPriceRangeBar(maxPriceFromServer);

                    }

                }

            } else {
                //noinspection ConstantConditions
                Utils.psLog("No Data of About Us.");
            }
        });

        userSelectedFeaturesArray = userSelectedFeatures.split("-");

        hotelFeatureViewModel.setHotelFeaturesByCityIdObj(cityId);
        hotelFeatureViewModel.getHotelFeaturesByCityIdData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got From API Data RoomFilterFragment");

                if (listResource.data.size() > 0) {
                    chipCloudAutoClearedValue.get().removeAllViews();
                    int l = 0;
                    for (int i = 0; i < listResource.data.size(); i++) {
                        if (listResource.data.get(i).hinf_types_arr.size() > 0) {
                            for (int j = 0; j < listResource.data.get(i).hinf_types_arr.size(); j++) {

                                chipCloudAutoClearedValue.get().addChip(listResource.data.get(i).hinf_types_arr.get(j).hinfo_typ_name);
                                featuresId.add(listResource.data.get(i).hinf_types_arr.get(j).hinfo_typ_id);

                                //Checking For Previous Selected Tags
                                if (isClickClear != 1) {
                                    if (userSelectedFeaturesArray.length > 0) {

                                        //Passing Selected Tags Array and Current Tag for checking
                                        selectedChipChecking(userSelectedFeaturesArray, listResource.data.get(i).hinf_types_arr.get(j).hinfo_typ_id);

                                        if (isFound == 1) {
                                            //Select Previous Selected tags
                                            chipCloudAutoClearedValue.get().setSelectedChip(l);
                                            isFound = 0;
                                        }

                                        l++;
                                    }
                                    isClickClear = 0;
                                }

                            }
                        }
                    }


                }

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });


        // Guest Rating Data Set
        binding.get().ratingSeekBar.setProgress(userSelectedRating);

        if (userSelectedRating != 0) {
            binding.get().txtRating.setText(getGuestRatingText(userSelectedRating));
        }


        // Update Hotel Stars
        if (oneStarsSelected) {
            clickStars("one");
        }

        if (twoStarsSelected) {
            clickStars("two");
        }

        if (threeStarsSelected) {
            clickStars("three");
        }

        if (fourStarsSelected) {
            clickStars("four");
        }

        if (fiveStarsSelected) {
            clickStars("five");
        }

    }


    //endregion

    //region private functions

    private void initPriceRangeBar(int maxValue) {

        binding.get().priceRangeBarContainer.removeAllViews();

        RangeSeekBar<Integer> seekBar = new RangeSeekBar<>(0, maxValue, this.getContext());

        seekBar.setSelectedMinValue(userSelectedPriceMin);

        if (userSelectedPriceMax > 0) {
            seekBar.setSelectedMaxValue(userSelectedPriceMax);
        } else {
            seekBar.setSelectedMaxValue(maxValue);
        }

        binding.get().txtPriceRange.setText(getPriceRangeText());

        seekBar.setOnRangeSeekBarChangeListener((bar, minValue, maxValue1) -> {

            binding.get().txtPriceRange.setText(getPriceRangeText());

            userSelectedPriceMin = minValue;
            userSelectedPriceMax = maxValue1;
        });

        rangeSeekBarAutoClearedValue = new AutoClearedValue<>(this, seekBar);

        binding.get().priceRangeBarContainer.addView(seekBar);
    }

    private String getPriceRangeText() {

        if (userSelectedPriceMax > 0) {
            return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (userSelectedPriceMax) + " " + getString(R.string.select_price_range);
        } else {

            if (maxPriceFromServer == 0) {
                return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (defaultPrice) + " " + getString(R.string.select_price_range);
            } else {
                return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (maxPriceFromServer) + " " + getString(R.string.select_price_range);
            }
        }
    }

    private String getGuestRatingText(int rating) {
        return getString(R.string.filter__guest_rating)+ " " + rating + " " + getString(R.string.filter__higher);
    }

    private void resetStars(String stars) {

        switch (stars) {
            case "one":

                binding.get().oneStar.setBackgroundResource(R.drawable.button_border);
                binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isOnePressed = false;
                selectedOne = "";

                break;
            case "two":

                binding.get().twoStar.setBackgroundResource(R.drawable.button_border);
                binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isTwoPressed = false;
                selectedTwo = "";

                break;
            case "three":

                binding.get().threeStar.setBackgroundResource(R.drawable.button_border);
                binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isThreePressed = false;
                selectedThree = "";

                break;
            case "four":

                binding.get().fourStar.setBackgroundResource(R.drawable.button_border);
                binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isFourPressed = false;
                selectedFour = "";

                break;
            case "five":

                binding.get().fiveStar.setBackgroundResource(R.drawable.button_border);
                binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isFivePressed = false;
                selectedFive = "";

                break;
        }

    }

    private void clickStars(String stars) {
        switch (stars) {
            case "one":

                binding.get().oneStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isOnePressed = true;
                selectedOne = "1";

                break;
            case "two":

                binding.get().twoStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isTwoPressed = true;
                selectedTwo = "2";

                break;
            case "three":

                binding.get().threeStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isThreePressed = true;
                selectedThree = "3";

                break;
            case "four":

                binding.get().fourStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isFourPressed = true;
                selectedFour = "4";

                break;
            case "five":

                binding.get().fiveStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isFivePressed = true;
                selectedFive = "5";

                break;
        }
    }

    private void passBackData(String hotelStars, Integer priceMin, Integer priceMax, Integer guestRatings, String hotelFeatures) {
        Intent intent = new Intent();
        intent.putExtra("hotelStars", hotelStars);
        intent.putExtra("priceMin", priceMin);
        intent.putExtra("priceMax", priceMax);
        intent.putExtra("guestRatings", guestRatings);
        intent.putExtra("hotelFeatures", hotelFeatures);

        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    //Checking for previous selected tags and current tag
    private void selectedChipChecking(String[] selectedChips, String chipId) {

        for (String selectedChip : selectedChips) {

            if (!selectedChip.isEmpty()) {
                Utils.psLog("Chip : " + selectedChip);

                if (selectedChip.equals(chipId)) {
                    isFound = 1;
                    break;
                }

            }

        }

    }
    //endregion


}