package com.panaceasoft.pshotels.ui.user;


import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentPasswordChangeBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;


/**
 * PasswordChangeFragment
 */
public class PasswordChangeFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentPasswordChangeBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentPasswordChangeBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_password_change, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().saveButton.setOnClickListener(view -> passwordUpdate());


    }

    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

    }

    //endregion


    //region Private Methods

    private void updateForgotBtnStatus() {

        if (userViewModel.isLoading) {
            binding.get().saveButton.setText(getResources().getString(R.string.message__loading));
        } else {
            binding.get().saveButton.setText(getResources().getString(R.string.save));
        }

    }

    private void passwordUpdate() {

        Utils.hideKeyboard(getActivity());

        if (!connectivity.isConnected()) {

            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        String password = binding.get().passwordEditText.getText().toString().trim();
        String confirmPassword = binding.get().confirmPasswordEditText.getText().toString().trim();
        if (password.equals("") || confirmPassword.equals("")) {
            Toast.makeText(getContext(), getString(R.string.error_message__blank_password), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!(password.equals(confirmPassword))) {
            Toast.makeText(getContext(), getString(R.string.error_message__password_not_equal), Toast.LENGTH_SHORT).show();
            return;
        }


        userViewModel.isLoading = true;
        prgDialog.get().show();
        updateForgotBtnStatus();

        userViewModel.passwordUpdate(loginUserId, password).observe(this, listResource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data" + listResource.message + listResource.toString());


                if (listResource.message != null && !listResource.message.equals("")) {
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                } else {
                    // Update the data
                    Toast.makeText(getContext(), listResource.data.message, Toast.LENGTH_SHORT).show();
                    userViewModel.isLoading = false;
                    prgDialog.get().cancel();
                }

            } else if (listResource != null && listResource.message != null) {
                Utils.psLog("Message from server.");
                Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                userViewModel.isLoading = false;
                prgDialog.get().cancel();
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }

            updateForgotBtnStatus();

        });
    }

    //endregion

}