package com.panaceasoft.pshotels.ui.user;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentProfileEditBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;

/**
 * ProfileEditFragment
 */
public class ProfileEditFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentProfileEditBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        FragmentProfileEditBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_edit, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        userViewModel.getLoginUser().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.size() > 0) {
                Utils.psLog("Got Data");

                //fadeIn Animation
                fadeIn(binding.get().getRoot());

                binding.get().setUser(listResource.get(0).user);
                userViewModel.user = listResource.get(0).user;
                Utils.psLog("Photo : " + listResource.get(0).user.user_profile_photo);
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
                //Toast.makeText(getContext(), "Empty Data", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        binding.get().profileImageview.setOnClickListener(view -> {

            if (connectivity.isConnected()) {
                try {

                    if (Utils.isStoragePermissionGranted(getActivity())) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, Utils.RESULT_LOAD_IMAGE);
                    }
                } catch (Exception e) {
                    Utils.psErrorLog("Error in Image Gallery.", e);
                }
            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }

        });

        binding.get().saveButton.setOnClickListener(view -> editProfileData());

        binding.get().passwordChangeButton.setOnClickListener(view -> navigationController.navigateToPasswordChangeActivity(getActivity()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            if (requestCode == Utils.RESULT_LOAD_IMAGE && resultCode == Utils.RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                if (getActivity() != null && selectedImage != null) {
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);

                    if (cursor != null) {
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        userViewModel.profileImagePath = cursor.getString(columnIndex);
                        cursor.close();

                        uploadImage();
                    }
                }

            }

        } catch (Exception e) {
            Utils.psErrorLog("Error in load image.", e);
        }


    }


    //endregion


    //region Private Methods


    private void editProfileData() {

        if (!connectivity.isConnected()) {
            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        String userName = binding.get().nameEditText.getText().toString();
        if (userName.equals("")) {
            Toast.makeText(getContext(), R.string.error_message__blank_name, Toast.LENGTH_SHORT).show();
            return;
        }

        String userEmail = binding.get().emailEditText.getText().toString();
        if (userEmail.equals("")) {
            Toast.makeText(getContext(), R.string.error_message__blank_email, Toast.LENGTH_SHORT).show();
            return;
        }

        String userPhone = binding.get().phoneEditText.getText().toString();
        String userAboutMe = binding.get().aboutMeEditText.getText().toString();

        if (userViewModel.user != null) {
            prgDialog.get().show();
            userViewModel.user.user_name = userName;
            userViewModel.user.user_email = userEmail;
            userViewModel.user.user_phone = userPhone;
            userViewModel.user.user_about_me = userAboutMe;

            userViewModel.updateUser(userViewModel.user).observe(this, listResource -> {

                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null) {
                    Utils.psLog("Got Data" + listResource.message + listResource.toString());


                    if (listResource.message != null && !listResource.message.equals("")) {
                        Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                        prgDialog.get().cancel();
                    } else {
                        // Update the data
                        Toast.makeText(getContext(), listResource.data.message, Toast.LENGTH_SHORT).show();
                        prgDialog.get().cancel();
                    }

                } else if (listResource != null && listResource.message != null) {
                    Utils.psLog("Message from server.");
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                    prgDialog.get().cancel();
                } else {
                    //noinspection ConstantConditions
                    Utils.psLog("Empty Data");

                }

            });
        }
    }

    private void uploadImage() {

        prgDialog.get().show();
        Utils.psLog("Uploading Image.");

        userViewModel.uploadImage(userViewModel.profileImagePath, loginUserId).observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data" + listResource.message + listResource.toString());


                if (listResource.message != null && !listResource.message.equals("")) {
                    Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();
                    prgDialog.get().cancel();
                } else {
                    // Update the data
                    Toast.makeText(getContext(), listResource.data.user_profile_photo, Toast.LENGTH_SHORT).show();
                    prgDialog.get().cancel();
                }

            } else if (listResource != null && listResource.message != null) {
                Utils.psLog("Message from server.");
                Toast.makeText(getContext(), listResource.message, Toast.LENGTH_SHORT).show();

                prgDialog.get().cancel();
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");

            }

        });
    }


    //endregion
}
