package com.panaceasoft.pshotels.ui.aboutus;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityAboutUsBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class AboutUsActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityAboutUsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityAboutUsBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.about_app));

        // setup Fragment
        setupFragment(new AboutUsFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}