package com.panaceasoft.pshotels.ui.hotel.search.filter;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityFilterBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;


/**
 * Created by Panacea-Soft on 3/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

public class FilterActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityFilterBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_filter);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityFilterBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.title_activity_filter));

        // setup Fragment
        setupFragment(new FilterFragment());


    }

    //endregion


}
