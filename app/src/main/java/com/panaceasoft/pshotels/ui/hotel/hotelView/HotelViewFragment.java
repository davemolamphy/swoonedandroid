package com.panaceasoft.pshotels.ui.hotel.hotelView;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentHotelViewBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.hotel.hotelView.adapter.HotelViewAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotel.HotelViewModel;
import com.panaceasoft.pshotels.viewmodel.room.RoomByHotelIdViewModel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelViewFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private RoomByHotelIdViewModel roomByHotelIdViewModel;

    private HotelViewModel hotelViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentHotelViewBinding> binding;
    AutoClearedValue<HotelViewAdapter> adapter;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentHotelViewBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_hotel_view, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }
        binding.get().setLoadingMore(connectivity.isConnected());

        Utils.psLog(" HotelViewFragment ");

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {
        binding.get().recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if(!binding.get().getLoadingMore() && !roomByHotelIdViewModel.forceEndLoading) {

                        if(connectivity.isConnected()) {

                            int limit = Config.ROOM_BY_HOTEL_COUNT;
                            roomByHotelIdViewModel.offset = roomByHotelIdViewModel.offset + limit;

                            RoomByHotelIdLoadingHolder holder = new RoomByHotelIdLoadingHolder(
                                    String.valueOf(limit),
                                    String.valueOf(roomByHotelIdViewModel.offset),
                                    String.valueOf(roomByHotelIdViewModel.loginUserId),
                                    String.valueOf(roomByHotelIdViewModel.hotelId),
                                    Config.API_KEY
                            );

                            roomByHotelIdViewModel.loadNextPage(holder);
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset roomByHotelIdViewModel.offset
            roomByHotelIdViewModel.offset = 0;

            // reset roomByHotelIdViewModel.forceEndLoading
            roomByHotelIdViewModel.forceEndLoading = false;

            roomByHotelIdViewModel.setRoomByHotelIdList(
                    String.valueOf(Config.ROOM_BY_HOTEL_COUNT),
                    String.valueOf(roomByHotelIdViewModel.offset),
                    loginUserId,
                    String.valueOf(roomByHotelIdViewModel.hotelId)
            );
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.psLog("On Resume");

        loadLoginUserId();

        // update live data
        hotelViewModel.setHotelData(
                loginUserId,
                hotelViewModel.hotelId
        );

    }

    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        roomByHotelIdViewModel = ViewModelProviders.of(this, viewModelFactory).get(RoomByHotelIdViewModel.class);

        hotelViewModel = ViewModelProviders.of( this, viewModelFactory ).get( HotelViewModel.class );
    }

    @Override
    protected void initAdapters() {
        HotelViewAdapter nvAdapter = new HotelViewAdapter(dataBindingComponent,
                hotel -> navigationController.navigateToHotelDetailActivity(getActivity(), hotel),
                room -> navigationController.navigateToRoomDetailActivity(getActivity(), room.room_id, room.room_name),
                hotel ->  navigationController.navigateToInquiry(getActivity(), hotel.hotel_id, ""),
                this::favClicked,
                hotel -> navigationController.navigateToHotelReviewListActivity(getActivity(), hotel),
                hotel -> navigationController.navigateToGalleryActivity(getActivity(), hotel.hotel_id),
                this::bookRoomClicked
        );
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }



    @Override
    protected void initData() {

        if(getActivity() != null) {

            try {

                String hotelId = getActivity().getIntent().getStringExtra("hotel_id");

                roomByHotelIdViewModel.hotelId = hotelId;

                hotelViewModel.hotelId = hotelId;
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : category id.", e);
            }
        }

        loadHotel();

        loadRoom();
    }

    //endregion

    //region Private Methods
    private void bookRoomClicked(Room room) {

        if(connectivity.isConnected()) {

            if (!hotelViewModel.isLoading) {
                if (loginUserId.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__login_first), Toast.LENGTH_SHORT).show();
                    navigationController.navigateToUserLoginActivity(getActivity());
                } else {

                    navigationController.navigateToBooking(getActivity(), room.hotel_id, room.room_id);
                }
            }
        }else {
            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    private void favClicked(Hotel hotel) {
        if(connectivity.isConnected()) {

            if (!hotelViewModel.isLoading) {
                if (loginUserId.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__login_first), Toast.LENGTH_SHORT).show();
                    navigationController.navigateToUserLoginActivity(getActivity());
                } else {
                    hotelViewModel.doFavourite(loginUserId, hotel.hotel_id);
                }
            }
        }else {
            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
        }
    }


    private void loadHotel() {

        hotelViewModel.setHotelData( loginUserId, hotelViewModel.hotelId );

        hotelViewModel.getHotelData().observe( this, listResource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Hotel Data");

                //fadeIn Animation
                fadeIn(binding.get().getRoot());

                // Update the data
                adapter.get().replaceHotel( listResource.data );
                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });
    }

    private void loadRoom() {

        roomByHotelIdViewModel.setRoomByHotelIdList(
                String.valueOf( Config.ROOM_BY_HOTEL_COUNT ),
                String.valueOf( roomByHotelIdViewModel.offset ),
                String.valueOf( roomByHotelIdViewModel.loginUserId ),
                String.valueOf( roomByHotelIdViewModel.hotelId )
        );

        LiveData<Resource<List<Room>>> hotelList  = roomByHotelIdViewModel.getRoomByHotelIdList();

        roomByHotelIdViewModel.isLoading = connectivity.isConnected();
        binding.get().setLoadingMore(roomByHotelIdViewModel.isLoading);
        binding.get().executePendingBindings();

        if(hotelList != null) {
            hotelList.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null) {
                    Utils.psLog("Got Room Data");

                    if (listResource.data.size() > 0) {
                        roomByHotelIdViewModel.isLoading = false;
                    }

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());


                    binding.get().setLoadingMore(roomByHotelIdViewModel.isLoading);

                    // Update the data
                    //adapter.get().replace(listResource.data);
                    adapter.get().replaceRoomList(listResource.data);
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if(listResource != null && listResource.message != null){
                    Toast.makeText(getContext(), getString(R.string.error_message__no_result), Toast.LENGTH_SHORT).show();
                    binding.get().setLoadingMore(false);
                    if(getActivity() != null) {
                        getActivity().finish();
                    }
                }else {
                    //noinspection ConstantConditions
                    if (roomByHotelIdViewModel.offset > 1) {
                        roomByHotelIdViewModel.forceEndLoading = true;
                    }
                }
            });
        }

        roomByHotelIdViewModel.getLoadMoreStatus().observe(this, loadingMore -> {
            if (loadingMore == null && !roomByHotelIdViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if(loadingMore!=null && !roomByHotelIdViewModel.isLoading) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if(loadingMore !=null ) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {

                        roomByHotelIdViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

        // update touch count
        setTouchCount();

    }

    private void setTouchCount() {

        Utils.psLog("Hotel Id : " + hotelViewModel.hotelId + "  Login User Id : " + loginUserId);
        if(connectivity.isConnected()) {
            if (loginUserId.equals("")) {
                hotelViewModel.doTouch("0", hotelViewModel.hotelId);
            } else {
                hotelViewModel.doTouch(loginUserId, hotelViewModel.hotelId);
            }
        }

    }

    //endregion


}
