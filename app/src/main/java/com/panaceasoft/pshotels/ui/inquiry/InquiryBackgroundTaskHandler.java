package com.panaceasoft.pshotels.ui.inquiry;

import com.panaceasoft.pshotels.repository.inquiry.InquiryRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;

public class InquiryBackgroundTaskHandler extends BackgroundTaskHandler {

    private final InquiryRepository repository;

    public InquiryBackgroundTaskHandler(InquiryRepository repository) {
        super();
        this.repository = repository;
    }


    public void postInquiry(String apiKey, String hotelId, String roomId, String userId, String inqName, String inqDesc, String contactName, String contactEmail, String contactPhone) {

        unregister();

        holdLiveData = repository.postInquiry(
                apiKey,
                hotelId,
                roomId,
                userId,
                inqName,
                inqDesc,
                contactName,
                contactEmail,
                contactPhone
        );

        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }
}