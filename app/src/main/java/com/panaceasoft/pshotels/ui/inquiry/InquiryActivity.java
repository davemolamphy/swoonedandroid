package com.panaceasoft.pshotels.ui.inquiry;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;

import com.panaceasoft.pshotels.databinding.ActivityInquiryBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;


/**
 * Created by Panacea-Soft on 11/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

public class InquiryActivity extends PSAppCompactActivity {

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityInquiryBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_inquiry);


        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityInquiryBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.inquiry));

        // setup Fragment
        setupFragment(new InquiryFragment());
    }

    //endregion

}
