package com.panaceasoft.pshotels.ui.review.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ReviewDetailViewBinding;
import com.panaceasoft.pshotels.databinding.ReviewSummaryViewBinding;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.ReviewSummaryWithCategoryData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final android.databinding.DataBindingComponent dataBindingComponent;

    private final int HOTEL_REVIEW_SUMMARY_VIEW = 1;
    private final int HOTEL_REVIEW_DETAIL_VIEW = HOTEL_REVIEW_SUMMARY_VIEW + 1;

    private List<ReviewDetail> reviewDetailList = new ArrayList<>();
    private ReviewSummaryWithCategoryData reviewSummary;

    public ReviewAdapter(android.databinding.DataBindingComponent dataBindingComponent) {

        this.dataBindingComponent = dataBindingComponent;

    }

    //region Override Methods

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;

        switch (viewType) {
            case HOTEL_REVIEW_SUMMARY_VIEW:

                ReviewSummaryViewBinding binding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.review_summary_view, parent, false,
                                dataBindingComponent);


                holder = new ReviewSummaryViewHolder(binding);

                break;
            case HOTEL_REVIEW_DETAIL_VIEW:
                ReviewDetailViewBinding binding2 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.review_detail_view, parent, false,
                                dataBindingComponent);

                holder = new ReviewDetailViewHolder(binding2);

                break;

            default:
                ReviewSummaryViewBinding binding3 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.review_summary_view, parent, false,
                                dataBindingComponent);

                binding3.getRoot().setVisibility(View.GONE);
                holder = new ReviewSummaryViewHolder(binding3);

                break;

        }

        return holder;
    }

    @Override
    public int getItemCount() {
        return reviewDetailList.size() + 1; // 1 is for reviewSummary summary view
    }

    @Override
    public int getItemViewType(int position) {

        switch (position) {
            case 0:
                return HOTEL_REVIEW_SUMMARY_VIEW;
            case 1:
                return HOTEL_REVIEW_DETAIL_VIEW;
            default:
                return HOTEL_REVIEW_DETAIL_VIEW;

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case HOTEL_REVIEW_SUMMARY_VIEW:
                if (holder instanceof ReviewSummaryViewHolder) {
                    setupHotelSummaryView((ReviewSummaryViewHolder) holder, position);
                }
                break;
            case HOTEL_REVIEW_DETAIL_VIEW:
                if (holder instanceof ReviewDetailViewHolder) {
                    setupRoomView((ReviewDetailViewHolder) holder, position);
                }
                break;
            default:
                // nothing
                break;

        }

    }

    private void setupHotelSummaryView(ReviewSummaryViewHolder holder, int position) {

        if (this.reviewSummary != null) {

            holder.binding.setReview(this.reviewSummary);

            Utils.psLog("Review Count : " + this.reviewSummary.review.rating.review_count);

            holder.binding.getRoot().setVisibility(View.VISIBLE);

            if (this.reviewSummary.review_categories.size() > 0) {
                holder.binding.progressBar.setProgress((int) Double.parseDouble(this.reviewSummary.review_categories.get(0).rating.final_rating));
                holder.binding.review1TextView.setText(this.reviewSummary.review_categories.get(0).rvcat_name);
                holder.binding.review1CountTextView.setText(this.reviewSummary.review_categories.get(0).rating.final_rating);
            }

            if (this.reviewSummary.review_categories.size() > 1) {
                holder.binding.progressBar2.setProgress((int) Double.parseDouble(this.reviewSummary.review_categories.get(1).rating.final_rating));
                holder.binding.review2TextView.setText(this.reviewSummary.review_categories.get(1).rvcat_name);
                holder.binding.review2CountTextView.setText(this.reviewSummary.review_categories.get(1).rating.final_rating);
            }

            if (this.reviewSummary.review_categories.size() > 2) {
                holder.binding.progressBar3.setProgress((int) Double.parseDouble(this.reviewSummary.review_categories.get(2).rating.final_rating));
                holder.binding.review3TextView.setText(this.reviewSummary.review_categories.get(2).rvcat_name);
                holder.binding.review3CountTextView.setText(this.reviewSummary.review_categories.get(2).rating.final_rating);
            }

            if (this.reviewSummary.review_categories.size() > 3) {
                holder.binding.progressBar4.setProgress((int) Double.parseDouble(this.reviewSummary.review_categories.get(3).rating.final_rating));
                holder.binding.review4TextView.setText(this.reviewSummary.review_categories.get(3).rvcat_name);
                holder.binding.review4CountTextView.setText(this.reviewSummary.review_categories.get(3).rating.final_rating);
            }

            if (this.reviewSummary.review_categories.size() > 4) {
                holder.binding.progressBar5.setProgress((int) Double.parseDouble(this.reviewSummary.review_categories.get(4).rating.final_rating));
                holder.binding.review5TextView.setText(this.reviewSummary.review_categories.get(4).rvcat_name);
                holder.binding.review5CountTextView.setText(this.reviewSummary.review_categories.get(4).rating.final_rating);
            }


        } else {
            holder.binding.getRoot().setVisibility(View.INVISIBLE);
        }

    }

    private void setupRoomView(ReviewDetailViewHolder holder, int position) {

        if (this.reviewDetailList != null) {
            if (this.reviewDetailList.size() > position - 1) {
                holder.binding.setReviewDetail(this.reviewDetailList.get(position - 1));
            }
        }

    }


    public class ReviewSummaryViewHolder extends RecyclerView.ViewHolder {
        ReviewSummaryViewBinding binding;

        ReviewSummaryViewHolder(ReviewSummaryViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class ReviewDetailViewHolder extends RecyclerView.ViewHolder {
        ReviewDetailViewBinding binding;

        ReviewDetailViewHolder(ReviewDetailViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void replaceReviewSummary(ReviewSummaryWithCategoryData reviewSummary) {
        this.reviewSummary = reviewSummary;
        notifyDataSetChanged();
    }

    public void replaceReviewDetailList(List<ReviewDetail> reviewDetailList) {
        this.reviewDetailList = reviewDetailList;
        notifyDataSetChanged();
    }
}