package com.panaceasoft.pshotels.ui.review.room;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentRoomReviewListBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.review.adapter.ReviewAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.review.ReviewViewModel;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.common.Status;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomReviewListFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    @VisibleForTesting
    AutoClearedValue<FragmentRoomReviewListBinding> binding;
    AutoClearedValue<ReviewAdapter> adapter;

    private ReviewViewModel reviewViewModel;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentRoomReviewListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_review_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }
        binding.get().setLoadingMore(connectivity.isConnected());

        Utils.psLog(" Room Review List ");

        return binding.get().getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.psLog("On Resume");

        loadLoginUserId();

        // reset reviewViewModel.offset
        reviewViewModel.offset = 0;

        // reset reviewViewModel.forceEndLoading
        reviewViewModel.forceEndLoading = false;

        // update live data
        reviewViewModel.setRoomReviewSummaryObj(
                reviewViewModel.roomId
        );

        reviewViewModel.setRoomReviewDetailObj(
                reviewViewModel.roomId,
                Utils.START_OF_OFFSET
        );
    }

    @Override
    protected void initUIAndActions() {
        binding.get().recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if(!binding.get().getLoadingMore() && !reviewViewModel.forceEndLoading) {

                        if(connectivity.isConnected()) {

                            int limit = Config.REVIEW_COUNT;
                            reviewViewModel.offset = reviewViewModel.offset + limit;

                            ReviewDetailLoadingHolder holder = new ReviewDetailLoadingHolder(
                                    String.valueOf( Config.REVIEW_COUNT ),
                                    String.valueOf( reviewViewModel.offset ),
                                    Config.API_KEY,
                                    loginUserId,
                                    reviewViewModel.roomId,
                                    ""
                            );

                            reviewViewModel.loadNextPageRoomReviews(holder);
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset reviewViewModel.offset
            reviewViewModel.offset = 0;

            // reset reviewViewModel.forceEndLoading
            reviewViewModel.forceEndLoading = false;

            // update live data
            reviewViewModel.setRoomReviewSummaryObj(
                    reviewViewModel.roomId
            );

            reviewViewModel.setRoomReviewDetailObj(
                    reviewViewModel.roomId,
                    Utils.START_OF_OFFSET
            );
        });

        binding.get().fab.setOnClickListener( view -> {
            if(connectivity.isConnected()) {

                if (loginUserId.equals("")) {

                    Toast.makeText(getContext(), getString(R.string.error_message__login_first), Toast.LENGTH_SHORT).show();
                    navigationController.navigateToUserLoginActivity(getActivity());
                } else {

                    navigationController.navigateToSubmitReview( getActivity(), reviewViewModel.roomId, "");
                }
            }else {

                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        reviewViewModel = ViewModelProviders.of(this , viewModelFactory).get(ReviewViewModel.class);
    }

    @Override
    protected void initAdapters() {
        ReviewAdapter nvAdapter = new ReviewAdapter(dataBindingComponent);
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        if(getActivity() != null) {

            try {

                reviewViewModel.roomId = getActivity().getIntent().getStringExtra("roomId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : category id.", e);
            }
        }

        loadRoomReview();
    }

    //endregion


    //region Private Methods

    private void loadRoomReview() {

        Utils.psLog("loading room review ");

        // set data
        reviewViewModel.setRoomReviewSummaryObj(
                reviewViewModel.roomId
        );

        reviewViewModel.setRoomReviewDetailObj(
                reviewViewModel.roomId,
                Utils.START_OF_OFFSET
        );

        // get data
        LiveData<Resource<List<ReviewDetail>>> detailList = reviewViewModel.getRoomReviewDetailData();

        // set loading more status
        reviewViewModel.isLoading = connectivity.isConnected();
        binding.get().setLoadingMore(reviewViewModel.isLoading);
        binding.get().executePendingBindings();

        // observe data
        if(detailList != null) {

            detailList.observe(this, listResource -> {

                if ( listResource != null && listResource.data != null ) {

                    Utils.psLog( "Got Room Review Detail" );

                    if( listResource.data.size() > 0 || listResource.status == Status.ERROR ) {

                        reviewViewModel.isLoading = false;
                    }

                    fadeIn( binding.get().getRoot());

                    binding.get().setLoadingMore( reviewViewModel.isLoading );
                    adapter.get().replaceReviewDetailList(listResource.data);
                    binding.get().executePendingBindings();
                    binding.get().swipeRefresh.setRefreshing(false);

                } else if ( listResource != null && listResource.message != null ) {

                    Toast.makeText(getContext(), getString(R.string.error_message__no_result), Toast.LENGTH_SHORT).show();
                    binding.get().setLoadingMore(false);

                }else {

                    binding.get().setLoadingMore(false);

                    //noinspection ConstantConditions
                    if (reviewViewModel.offset > 1) {

                        reviewViewModel.forceEndLoading = true;
                    }
                }
            });
        }

        reviewViewModel.getLoadMoreStatusOfRoomReviews().observe(this, loadingMore -> {
            if (loadingMore == null && !reviewViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if(loadingMore!=null && !reviewViewModel.isLoading) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if(loadingMore !=null ) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {
                        //Snackbar.make(binding.get().loadMoreBar, error, Snackbar.LENGTH_LONG).show();
                        reviewViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

        reviewViewModel.getRoomReviewSummaryData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");

                // update feature news

                adapter.get().replaceReviewSummary(listResource.data);
                binding.get().executePendingBindings();


            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });
    }

    //endregion
}

