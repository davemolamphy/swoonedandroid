package com.panaceasoft.pshotels.ui.review.entry;

import android.app.Activity;
import android.app.SearchManager;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentRoomListFilterBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.review.adapter.RoomListFilterAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.review.RoomListFilterViewModel;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.ArrayList;
import java.util.List;

public class RoomListFilterFragment extends PSFragment {

    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private RoomListFilterViewModel roomListFilterViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentRoomListFilterBinding> binding;
    AutoClearedValue<RoomListFilterAdapter> adapter;
    AutoClearedValue<MenuInflater> inflater;
    AutoClearedValue<Menu> menu;


    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentRoomListFilterBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_list_filter, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        setHasOptionsMenu(true);

        return binding.get().getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate the menu; this adds items to the action bar if it is present.
        this.inflater = new AutoClearedValue<>(this, inflater);
        this.menu = new AutoClearedValue<>(this, menu);

        this.inflater.get().inflate(R.menu.search_view_menu, this.menu.get());
        super.onCreateOptionsMenu(this.menu.get(), this.inflater.get());

        if (getActivity() != null) {
            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

            if (searchManager != null) {
                SearchView searchView = (SearchView) this.menu.get().findItem(R.id.action_search).getActionView();
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                searchView.setMaxWidth(Integer.MAX_VALUE);

                // listening to search query text change
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted

                        Utils.psLog("Submit : " + query);

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {

                        // filter recycler view when text is changed
                        //mAdapter.getFilter().filter(query);
                        Utils.psLog("Text Change : " + query);

                        if (query.equals("")) {

                            roomListFilterViewModel.isFilter = false;
                            roomListFilterViewModel.query = query;

                            List<Room> roomList = roomListFilterViewModel.getRoomList();
                            adapter.get().replace(roomList);
                            binding.get().executePendingBindings();
                        } else {

                            roomListFilterViewModel.isFilter = true;
                            roomListFilterViewModel.query = query;

                            List<Room> roomList = new ArrayList<>();

                            roomList.addAll(roomListFilterViewModel.getRoomList());
                            adapter.get().replace(roomList);
                            binding.get().executePendingBindings();
                        }

                        return false;
                    }

                });
            }
        }
    }

    @Override
    protected void initUIAndActions() {

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // update live data
            roomListFilterViewModel.setRoomByHotelIdList(roomListFilterViewModel.hotelId);
        });
    }

    @Override
    protected void initViewModels() {

        // ViewModel need to get from ViewModelProviders
        roomListFilterViewModel = ViewModelProviders.of(this, viewModelFactory).get(RoomListFilterViewModel.class);
    }

    //
    @Override
    protected void initAdapters() {

        RoomListFilterAdapter nvAdapter = new RoomListFilterAdapter(dataBindingComponent, this::onClickRoom);
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        if (getActivity() != null) {
            try {

                roomListFilterViewModel.hotelId = getActivity().getIntent().getStringExtra("hotelId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : category id.", e);
            }

        }

        loadRooms();
    }

    //endregion


    //region Private Methods

    private void onClickRoom(Room room) {
        Utils.psLog("Clicked : " + room.room_name);

        Intent intent = new Intent();
        intent.putExtra("roomId", room.room_id);
        intent.putExtra("roomName", room.room_name);

        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    private void loadRooms() {

        roomListFilterViewModel.setRoomByHotelIdList(roomListFilterViewModel.hotelId);

        LiveData<Resource<List<Room>>> roomList = roomListFilterViewModel.getRoomListData();

        if (roomList != null) {
            roomList.observe(this, listResource -> {

                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null) {

                    Utils.psLog("Got Room List Data");

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());

                    // Update the data
                    if (roomListFilterViewModel.isFilter) {

                        adapter.get().replace(roomListFilterViewModel.getRoomList());
                    } else {

                        adapter.get().replace(listResource.data);
                    }
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if (listResource != null && listResource.message != null) {

                    Toast.makeText(getContext(), getString(R.string.error_message__no_result), Toast.LENGTH_SHORT).show();
                    binding.get().setLoadingMore(false);
                    if (getActivity() != null) {

                        getActivity().finish();
                    }
                }
            });
        }

    }

    //endregion
}