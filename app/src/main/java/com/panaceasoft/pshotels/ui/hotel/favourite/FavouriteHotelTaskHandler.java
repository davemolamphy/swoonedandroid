package com.panaceasoft.pshotels.ui.hotel.favourite;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.FavouriteHotelLoadingHolder;

public class FavouriteHotelTaskHandler extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public FavouriteHotelTaskHandler(HotelRepository repository) {
        super();
        this.repository = repository;
    }

    public void getNextPage(FavouriteHotelLoadingHolder nextPageHolder) {

        if(nextPageHolder == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageFavouriteHotels( nextPageHolder );
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}
