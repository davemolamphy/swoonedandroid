package com.panaceasoft.pshotels.ui.hotel.search;

import com.panaceasoft.pshotels.repository.searchlog.SearchLogRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.SearchLog;

public class InsertSearchLogTaskHandler  extends BackgroundTaskHandler {

    private final SearchLogRepository repository;

    public InsertSearchLogTaskHandler(SearchLogRepository repository) {

        super();

        this.repository = repository;
    }

    public void insertSearchLog(SearchLog searchLog) {

        if(searchLog == null) {
            return;
        }

        unregister();

        holdLiveData = repository.insertSearchLog(searchLog);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }
}