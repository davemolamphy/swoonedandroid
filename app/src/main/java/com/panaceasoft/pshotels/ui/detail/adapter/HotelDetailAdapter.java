package com.panaceasoft.pshotels.ui.detail.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.HotelDetailFeatureHeaderViewBinding;
import com.panaceasoft.pshotels.databinding.HotelDetailFeatureViewBinding;
import com.panaceasoft.pshotels.databinding.HotelDetailViewBinding;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.HotelFeatureDetail;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/2/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelDetailAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final HotelImageClickCallback hotelImageClickCallback;
    private final ReviewListClickCallback reviewListClickCallback;
    private final FavClickCallback favClickCallback;
    private final ShareClickCallback shareClickCallback;
    private final InquiryClickCallback inquiryClickCallback;
    private final MapClickCallBack mapClickCallBack;

    private final int HOTEL_DETAIL_VIEW = 1;
    private final int HOTEL_FEATURE_HEADER_VIEW = HOTEL_DETAIL_VIEW + 1;
    private final int HOTEL_FEATURE_VIEW = HOTEL_FEATURE_HEADER_VIEW + 1;

    private List<HotelFeatures> hotelFeaturesList = new ArrayList<>();

    private Hotel hotel;

    public HotelDetailAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                              FavClickCallback favClickCallback,
                              ReviewListClickCallback reviewListClickCallback,
                              HotelImageClickCallback hotelImageClickCallback,
                              ShareClickCallback shareClickCallback,
                              InquiryClickCallback inquiryClickCallback,
                              MapClickCallBack mapClickCallBack) {

        this.dataBindingComponent = dataBindingComponent;
        this.reviewListClickCallback = reviewListClickCallback;
        this.favClickCallback = favClickCallback;
        this.hotelImageClickCallback = hotelImageClickCallback;
        this.shareClickCallback = shareClickCallback;
        this.inquiryClickCallback = inquiryClickCallback;
        this.mapClickCallBack = mapClickCallBack;

    }


    //region Override Methods

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;

        switch (viewType) {
            case HOTEL_DETAIL_VIEW:

                HotelDetailViewBinding binding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotel_detail_view, parent, false,
                                dataBindingComponent);

                binding.shareButton.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && shareClickCallback != null) {
                        shareClickCallback.onClick(hotel);
                    }

                });

                binding.inquiryButton.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && inquiryClickCallback != null) {
                        inquiryClickCallback.onClick(hotel);
                    }
                });

                binding.mapButton.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && mapClickCallBack != null) {
                        mapClickCallBack.onClick(hotel);
                    }
                });

                binding.reviewCountTextView.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && reviewListClickCallback != null) {
                        reviewListClickCallback.onClick(hotel);
                    }
                });

                binding.favBg.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && favClickCallback != null) {
                        favClickCallback.onClick(hotel);
                    }
                });

                binding.hotelImageView.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && hotelImageClickCallback != null) {
                        hotelImageClickCallback.onClick(hotel);
                    }
                });

                holder = new HotelDetailViewHolder(binding);

                break;
            case HOTEL_FEATURE_HEADER_VIEW:

                HotelDetailFeatureHeaderViewBinding binding2 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotel_detail_feature_header_view, parent, false,
                                dataBindingComponent);
                holder = new HotelFeatureHeaderViewHolder(binding2);

                break;

            case HOTEL_FEATURE_VIEW:

                HotelDetailFeatureViewBinding binding3 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotel_detail_feature_view, parent, false,
                                dataBindingComponent);
                holder = new HotelFeatureViewHolder(binding3);

                break;

            default:


                HotelDetailViewBinding binding4 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotelview_hotel_summary, parent, false,
                                dataBindingComponent);

                binding4.getRoot().setVisibility(View.GONE);
                holder = new HotelDetailViewHolder(binding4);

                break;

        }

        return holder;
    }

    @Override
    public int getItemCount() {
        return getTotalItemCount();
    }

    @Override
    public int getItemViewType(int position) {

        if(position == 0 ) {
            return HOTEL_DETAIL_VIEW;
        }else {
            if(isHeader(position) ) {
                return HOTEL_FEATURE_HEADER_VIEW;
            }else {
                return HOTEL_FEATURE_VIEW;
            }
        }

    }

    private boolean isHeader(int position) {
        if (position == 0) {
            return false;
        }

        if (position == 1) {
            return true;
        }

        int itemCount = 0;
        int header = 1;
        for(int i=0; i<hotelFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            if(itemCount == position) {
                return true;
            }

            itemCount += hotelFeaturesList.get(i).hinf_types_arr.size();

            if(itemCount > position) {
                break;
            }
        }

        return false;
    }

    private int getTotalItemCount() {

        int hotelDetailCount = 1;
        int hotelFeatureHeaderCount = hotelFeaturesList.size();
        int hotelFeatureCount  = 0;
        for(int i=0; i<hotelFeatureHeaderCount; i++ ) {
            hotelFeatureCount += hotelFeaturesList.get(i).hinf_types_arr.size();
        }

        return hotelDetailCount + hotelFeatureHeaderCount + hotelFeatureCount;
    }

    private HotelFeatures getFeatureType(int position) {

        if(position == 1) {
            return this.hotelFeaturesList.get(position - 1);
        }

        int itemCount = 0;
        int header = 1;
        for(int i=0; i<hotelFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            if(itemCount == position) {
                return this.hotelFeaturesList.get(i);
            }

            itemCount += hotelFeaturesList.get(i).hinf_types_arr.size();

        }

        return null;

    }

    private HotelFeatureDetail getFeatureDetail(int position) {
        int itemCount = 0;
        int header = 1;
        for(int i=0; i<hotelFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            int iItemSize = hotelFeaturesList.get(i).hinf_types_arr.size();
            if( position > itemCount + iItemSize) {
                itemCount += hotelFeaturesList.get(i).hinf_types_arr.size();
            }else {
                int convertedPosition = position - itemCount - 1;
                return this.hotelFeaturesList.get(i).hinf_types_arr.get(convertedPosition);
            }

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case HOTEL_DETAIL_VIEW:
                if(holder instanceof HotelDetailViewHolder) {
                    setupHotelSummaryView((HotelDetailViewHolder) holder, position);
                }
                break;
            case HOTEL_FEATURE_HEADER_VIEW:
                if(holder instanceof HotelFeatureHeaderViewHolder) {
                    setupFeatureHeaderView((HotelFeatureHeaderViewHolder) holder, position);
                }
                break;
            case HOTEL_FEATURE_VIEW:
                if(holder instanceof HotelFeatureViewHolder) {
                    setupFeatureView((HotelFeatureViewHolder) holder, position);
                }
                break;
            default:
                // nothing
                break;

        }

    }

    private void setupHotelSummaryView(HotelDetailViewHolder holder, int position) {

        if ( this.hotel != null ) {

            holder.binding.setHotel(this.hotel);

            if ( this.hotel.hotel_star_rating != null ) {

                int rating = Integer.parseInt( this.hotel.hotel_star_rating );

                if ( rating < 5 ) holder.binding.star5ImageView.setVisibility(View.GONE);

                if ( rating < 4 ) holder.binding.star4ImageView.setVisibility(View.GONE);

                if ( rating < 3 ) holder.binding.star3ImageView.setVisibility(View.GONE);

                if ( rating < 2 ) holder.binding.star2ImageView.setVisibility(View.GONE);

                if ( rating < 1 ) holder.binding.star1ImageView.setVisibility(View.GONE);

            } else {

                holder.binding.star1ImageView.setVisibility(View.GONE);
                holder.binding.star2ImageView.setVisibility(View.GONE);
                holder.binding.star3ImageView.setVisibility(View.GONE);
                holder.binding.star4ImageView.setVisibility(View.GONE);
                holder.binding.star5ImageView.setVisibility(View.GONE);
            }
        }
    }

    private void setupFeatureHeaderView(HotelFeatureHeaderViewHolder holder, int position) {

        if(this.hotelFeaturesList != null && this.hotelFeaturesList.size() > 0) {
            HotelFeatures featureType = getFeatureType(position);
            holder.binding.setFeatureType(featureType);
        }

    }

    private void setupFeatureView(HotelFeatureViewHolder holder, int position) {

        if(this.hotelFeaturesList != null  && this.hotelFeaturesList.size() > 0) {
            HotelFeatureDetail hotelFeatureDetail = getFeatureDetail(position);
            holder.binding.setFeature(hotelFeatureDetail);
        }

    }


    public class HotelDetailViewHolder extends RecyclerView.ViewHolder {
        HotelDetailViewBinding binding;

        HotelDetailViewHolder(HotelDetailViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class HotelFeatureHeaderViewHolder extends RecyclerView.ViewHolder {
        HotelDetailFeatureHeaderViewBinding binding;

        HotelFeatureHeaderViewHolder(HotelDetailFeatureHeaderViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class HotelFeatureViewHolder extends RecyclerView.ViewHolder {
        HotelDetailFeatureViewBinding binding;

        HotelFeatureViewHolder(HotelDetailFeatureViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ReviewListClickCallback {
        void onClick(Hotel hotel);
    }

    public interface FavClickCallback {
        void onClick(Hotel hotel);
    }

    public interface HotelImageClickCallback {
        void onClick(Hotel hotel);
    }

    public interface ShareClickCallback {
        void onClick(Hotel hotel);
    }

    public interface InquiryClickCallback {
        void onClick(Hotel hotel);
    }

    public interface MapClickCallBack {
        void onClick(Hotel hotel);
    }

    public void replaceHotel(Hotel hotel) {
        this.hotel = hotel;
        notifyDataSetChanged();
    }

    public void replaceFeatureList(List<HotelFeatures> hotelFeaturesList) {
        this.hotelFeaturesList = hotelFeaturesList;
        notifyDataSetChanged();
    }
}