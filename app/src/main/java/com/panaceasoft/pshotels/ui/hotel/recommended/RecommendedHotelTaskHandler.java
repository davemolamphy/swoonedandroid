package com.panaceasoft.pshotels.ui.hotel.recommended;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.RecommendedHotelLoadingHolder;

public class RecommendedHotelTaskHandler extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public RecommendedHotelTaskHandler(HotelRepository repository) {
        super();
        this.repository = repository;
    }

    public void getNextPage(RecommendedHotelLoadingHolder nextPageHolder) {

        if(nextPageHolder == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageRecommendedHotels(nextPageHolder);
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}
