package com.panaceasoft.pshotels.ui.review.hotel;

import com.panaceasoft.pshotels.repository.review.ReviewRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;

/**
 * Created by Panacea-Soft on 4/25/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelReviewTaskHandler extends BackgroundTaskHandler {

    private final ReviewRepository repository;

    public HotelReviewTaskHandler(ReviewRepository repository) {
        super();

        this.repository = repository;
    }

    public void getNextPage(ReviewDetailLoadingHolder nextPageHolder) {

        if(nextPageHolder == null) {
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageHotelReviews(nextPageHolder);
        loadingState.setValue(new BackgroundTaskHandler.LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }


}