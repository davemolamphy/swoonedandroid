package com.panaceasoft.pshotels.ui.hotel.search.destinationfilter;


import android.app.Activity;
import android.app.SearchManager;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.SearchView;
import android.widget.Toast;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentCityListBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.hotel.search.destinationfilter.adapter.CityListAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.city.CityViewModel;
import com.panaceasoft.pshotels.viewobject.City;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityListFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    //private SearchNewsViewModel searchNewsViewModel;
    private CityViewModel cityViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentCityListBinding> binding;
    AutoClearedValue<CityListAdapter> adapter;
    AutoClearedValue<MenuInflater> inflater;
    AutoClearedValue<Menu> menu;
    private SearchView searchView;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentCityListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        setHasOptionsMenu(true);

        return binding.get().getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.inflater = new AutoClearedValue<>(this, inflater);
        this.menu = new AutoClearedValue<>(this, menu);

        this.inflater.get().inflate(R.menu.search_view_menu, this.menu.get());
        super.onCreateOptionsMenu(this.menu.get(), this.inflater.get());

        // Associate searchable configuration with the SearchView

        if(getActivity() != null) {
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) this.menu.get().findItem(R.id.action_search).getActionView();

            if(searchManager != null) {

                searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                searchView.setMaxWidth(Integer.MAX_VALUE);

                // listening to search query text change
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        Utils.psLog("Submit : " + query);

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        Utils.psLog("Text Change : " + query);

                        if (query.equals("")) {
                            cityViewModel.isFilter = false;
                            cityViewModel.query = query;

                            List<City> cityList = cityViewModel.getFilteredCityListData();
                            adapter.get().replace(cityList);
                            binding.get().executePendingBindings();
                        } else {
                            cityViewModel.isFilter = true;
                            cityViewModel.query = query;

                            List<City> cityList = new ArrayList<>();

                            cityList.addAll(cityViewModel.getFilteredCityListData());
                            adapter.get().replace(cityList);
                            binding.get().executePendingBindings();
                        }

                        return false;
                    }

                });
            }

        }

    }

    @Override
    protected void initUIAndActions() {


        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // update live data
            cityViewModel.setCityListData();

        });

    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        cityViewModel = ViewModelProviders.of(this, viewModelFactory).get(CityViewModel.class);

    }

    @Override
    protected void initAdapters() {
        CityListAdapter nvAdapter = new CityListAdapter(dataBindingComponent,
                this::onClickCity);
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        if(getActivity() != null) {
            try {

                cityViewModel.cityId = getActivity().getIntent().getStringExtra("cityId");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : category id.", e);
            }

        }

        loadCities();
    }

    //endregion


    //region Private Methods

    private void onClickCity(City city) {
        Utils.psLog("Clicked : " + city.city_name);

        Intent intent = new Intent();
        intent.putExtra("cityId", city.city_id);
        intent.putExtra("cityName", city.city_name);

        if(getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    private void loadCities() {

        cityViewModel.setCityListData();

        //( String.valueOf(Config.SEARCH_COUNT), Utils.START_OF_OFFSET, cityViewModel.keywords, cityViewModel.catId, loginUserId);
        LiveData<Resource<List<City>>> cityList  = cityViewModel.getCityListData();

        if(cityList != null) {
            cityList.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null) {
                    Utils.psLog("Got Hotel Data");

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());

                    // Update the data
                    if(cityViewModel.isFilter) {
                        adapter.get().replace(cityViewModel.getFilteredCityListData());
                    }else {
                        adapter.get().replace(listResource.data);
                    }
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if(listResource != null && listResource.message != null){
                    Toast.makeText(getContext(), getString(R.string.error_message__no_result), Toast.LENGTH_SHORT).show();
                    binding.get().setLoadingMore(false);
                    if(getActivity() != null) {
                        getActivity().finish();
                    }
                }
            });
        }

    }

    //endregion


}
