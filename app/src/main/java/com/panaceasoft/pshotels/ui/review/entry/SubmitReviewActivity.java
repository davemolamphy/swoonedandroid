package com.panaceasoft.pshotels.ui.review.entry;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivitySubmitReviewBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;
import com.panaceasoft.pshotels.utils.Utils;

public class SubmitReviewActivity extends PSAppCompactActivity {

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySubmitReviewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_submit_review);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivitySubmitReviewBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.submit_review));

        // setup Fragment
        setupFragment(new SubmitReviewFragment());
    }

    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Utils.psLog("Inside Result MainActivity");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
