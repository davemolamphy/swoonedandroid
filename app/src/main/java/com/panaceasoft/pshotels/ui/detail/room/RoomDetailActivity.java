package com.panaceasoft.pshotels.ui.detail.room;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityRoomDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class RoomDetailActivity extends PSAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityRoomDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_room_detail);

        // init all UI
        initUI(binding);
    }

    private void initUI(ActivityRoomDetailBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getRoomName());

        // Setup Fragment
        setupFragment(new RoomDetailFragment());
    }

    private String getRoomName() {
        return this.getIntent().getStringExtra("roomName");
    }
}
