package com.panaceasoft.pshotels.ui.booking.entry;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityBookingBinding;
import com.panaceasoft.pshotels.databinding.ActivityInquiryBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;
import com.panaceasoft.pshotels.ui.inquiry.InquiryFragment;

public class BookingActivity extends PSAppCompactActivity {

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityBookingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_booking);


        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityBookingBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.booking__hotel));

        // setup Fragment
        setupFragment(new BookingFragment());
    }

    //endregion

}
