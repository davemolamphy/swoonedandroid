package com.panaceasoft.pshotels.ui.detail.hotel;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityHotelDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class HotelDetailActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityHotelDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_hotel_detail);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityHotelDetailBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getHotelName());

        // setup Fragment
        setupFragment(new HotelDetailFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    private String getHotelName() {
        return this.getIntent().getStringExtra("hotel_name");
    }

    //endregion


}