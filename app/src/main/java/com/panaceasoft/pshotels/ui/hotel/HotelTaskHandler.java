package com.panaceasoft.pshotels.ui.hotel;

import android.support.annotation.NonNull;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.Utils;

/**
 * Created by Panacea-Soft on 4/25/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelTaskHandler  extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public HotelTaskHandler(HotelRepository repository) {
        super();

        this.repository = repository;
    }

    public void doFavourite(String loginUserId, String newsId) {

        if(loginUserId == null || newsId == null) return;

        if(loginUserId.equals("") || newsId.equals("")) return;

        Utils.psLog("Do Favourite : HotelTaskHandler");
        holdLiveData = repository.doFavourite(loginUserId, newsId);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);


    }

    public void doHotelTouch(@NonNull String loginUserId, @NonNull String hotelId) {

        unregister();

        holdLiveData =repository.doPostTouchCount(loginUserId, hotelId);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }


}