package com.panaceasoft.pshotels.ui.hotel.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivitySearchResultListBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;
import com.panaceasoft.pshotels.utils.Utils;

public class SearchResultListActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySearchResultListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_search_result_list);

        // Init all UI
        initUI(binding);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Utils.psLog("Inside Result SearchResultListActivity");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }


    //endregion


    //region Private Methods

    private void initUI(ActivitySearchResultListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.search_result));

        // setup Fragment
        setupFragment(new SearchResultListFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }



    //endregion

}



