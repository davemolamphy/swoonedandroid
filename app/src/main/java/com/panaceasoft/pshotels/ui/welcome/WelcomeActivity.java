package com.panaceasoft.pshotels.ui.welcome;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityWelcomeBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class WelcomeActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityWelcomeBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityWelcomeBinding binding) {

        // Toolbar
        //initToolbar(binding.toolbar, getResources().getString(R.string.about_welcome));

        // setup Fragment
        setupFragment(new WelcomeFragment());

    }

    //endregion


}
