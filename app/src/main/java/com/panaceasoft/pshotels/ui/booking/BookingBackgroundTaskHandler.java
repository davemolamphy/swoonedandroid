package com.panaceasoft.pshotels.ui.booking;

import com.panaceasoft.pshotels.repository.booking.BookingRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class BookingBackgroundTaskHandler extends BackgroundTaskHandler {

    private final BookingRepository repository;

    public BookingBackgroundTaskHandler(BookingRepository repository) {
        super();
        this.repository = repository;
    }


    public void postBooking(String apiKey,
                            String loginUserId,
                            String userName,
                            String userEmail,
                            String userPhone,
                            String hotelId,
                            String roomId,
                            String adultCount,
                            String kidCount,
                            String startDate,
                            String endDate,
                            String extraBed,
                            String remark) {

        unregister();

        holdLiveData = repository.postBooking(
                apiKey,
                loginUserId,
                userName,
                userEmail,
                userPhone,
                hotelId,
                roomId,
                adultCount,
                kidCount,
                startDate,
                endDate,
                extraBed,
                remark
        );

        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }

    public void getNextPage(String apiKey, String loginUserId, String limit, String offset) {

        if(apiKey == null || loginUserId == null || offset == null || limit == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPagePopularBookings(apiKey, loginUserId, limit, offset);
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}