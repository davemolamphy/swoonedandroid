package com.panaceasoft.pshotels.ui.hotel.favourite;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentNewsListBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.hotel.adapter.HotelListAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotel.FavouriteHotelViewModel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.FavouriteHotelLoadingHolder;

import java.util.List;

public class FavouriteHotelListFragment extends PSFragment {

    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private FavouriteHotelViewModel favouriteHotelViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentNewsListBinding> binding;
    AutoClearedValue<HotelListAdapter> adapter;

    //endregion

    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentNewsListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if (Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        } else {
            binding.get().adView.setVisibility(View.GONE);
        }


        binding.get().setLoadingMore(connectivity.isConnected());


        Utils.psLog(" Favourite Hotel ");

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {
        binding.get().recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if (!binding.get().getLoadingMore() && !favouriteHotelViewModel.forceEndLoading) {

                        if (connectivity.isConnected()) {

                            int limit = Config.FAVOURITE_HOTEL_COUNT;
                            favouriteHotelViewModel.offset = favouriteHotelViewModel.offset + limit;

                            FavouriteHotelLoadingHolder holder = new FavouriteHotelLoadingHolder(
                                    loginUserId,
                                    String.valueOf(limit),
                                    String.valueOf(favouriteHotelViewModel.offset)
                            );

                            favouriteHotelViewModel.loadNextPage(holder);
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset favouriteHotelViewModel.offset
            favouriteHotelViewModel.offset = 0;

            // reset favouriteHotelViewModel.forceEndLoading
            favouriteHotelViewModel.forceEndLoading = false;

            // update live data
            favouriteHotelViewModel.setFavouriteHotelList(
                    loginUserId,
                    String.valueOf(Config.FAVOURITE_HOTEL_COUNT),
                    String.valueOf(favouriteHotelViewModel.offset)
            );

        });
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        favouriteHotelViewModel = ViewModelProviders.of(this, viewModelFactory).get(FavouriteHotelViewModel.class);

    }

    @Override
    protected void initAdapters() {

        HotelListAdapter nvAdapter = new HotelListAdapter(dataBindingComponent, hotel -> navigationController.navigateToHotelViewActivity(getActivity(), hotel.hotel_id, hotel.hotel_name));
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        loadFavHotel();
    }

    //endregion


    //region Private Methods

    private void loadFavHotel() {

        /* Boolean isConnected = Connectivity.isConnected(getContext()); */

        favouriteHotelViewModel.setFavouriteHotelList(
                loginUserId,
                String.valueOf(Config.FAVOURITE_HOTEL_COUNT),
                String.valueOf(favouriteHotelViewModel.offset)
        );

        LiveData<Resource<List<Hotel>>> hotel = favouriteHotelViewModel.getFavouriteHotelList();

        favouriteHotelViewModel.isLoading = true;
        binding.get().setLoadingMore(favouriteHotelViewModel.isLoading);
        binding.get().executePendingBindings();

        if (hotel != null) {
            hotel.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null && listResource.message == null) {
                    Utils.psLog("Got News Data");

                    if (listResource.data.size() > 0) {
                        favouriteHotelViewModel.isLoading = false;
                    }

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());

                    binding.get().setLoadingMore(favouriteHotelViewModel.isLoading);

                    // Update the data
                    adapter.get().replace(listResource.data);
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if (listResource != null && listResource.message != null) {
                    //Snackbar.make(binding.get().loadMoreBar, listResource.message, Snackbar.LENGTH_LONG).show();
                    binding.get().swipeRefresh.setRefreshing(false);
                    binding.get().setLoadingMore(false);
                } else {
                    //noinspection ConstantConditions
                    if (favouriteHotelViewModel.offset > 1) {
                        favouriteHotelViewModel.forceEndLoading = true;
                    }
                }
            });
        }

        favouriteHotelViewModel.getLoadMoreStatus().observe(this, loadingMore -> {
            if (loadingMore == null && !favouriteHotelViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if (!favouriteHotelViewModel.isLoading && loadingMore != null) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if (loadingMore != null) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {
                        favouriteHotelViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

    }

    //endregion


}
