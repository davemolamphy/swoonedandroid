package com.panaceasoft.pshotels.ui.gallery;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityGalleryDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class GalleryDetailActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityGalleryDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery_detail);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityGalleryDetailBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.gallery));

        // setup Fragment
        setupFragment(new GalleryDetailFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}