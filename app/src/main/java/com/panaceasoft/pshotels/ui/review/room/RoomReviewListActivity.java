package com.panaceasoft.pshotels.ui.review.room;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityRoomReviewListBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class RoomReviewListActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityRoomReviewListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_room_review_list);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityRoomReviewListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.review_list));

        // setup Fragment
        setupFragment(new RoomReviewListFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}