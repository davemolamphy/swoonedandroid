package com.panaceasoft.pshotels.ui.hotel.search.destinationfilter;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityCityListBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class CityListActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCityListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_city_list);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityCityListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.destination));

        // setup Fragment
        setupFragment(new CityListFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}