package com.panaceasoft.pshotels.ui.booking.detail;


import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentBookingDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.booking.BookingViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookingDetailFragment extends PSFragment {

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private BookingViewModel bookingViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentBookingDetailBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentBookingDetailBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_detail, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);
        return binding.get().getRoot();
    }


    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

    }

    @Override
    protected void initViewModels() {
        bookingViewModel = ViewModelProviders.of(this, viewModelFactory).get(BookingViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        if (getActivity() != null) {
            try {

                bookingViewModel.bookingId = getActivity().getIntent().getStringExtra("BOOKING_ID");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }
        }

        bookingViewModel.setBookingByIdObj(bookingViewModel.bookingId);
        bookingViewModel.getBookingById().observe(this, booking -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (booking != null ) {
                Utils.psLog("Got Booking By Id data");

                //fadeIn Animation
                fadeIn(binding.get().getRoot());

                binding.get().setBooking(booking);

                if(booking.booking_status.equals(getString(R.string.booking__cancelled))) {
                    binding.get().statusTextView.setTextColor(getResources().getColor(R.color.colorCancel));
                }else if(booking.booking_status.equals(getString(R.string.booking__progress))) {
                    binding.get().statusTextView.setTextColor(getResources().getColor(R.color.colorText));
                }else if(booking.booking_status.equals(getString(R.string.booking__comfirmed))) {
                    binding.get().statusTextView.setTextColor(getResources().getColor(R.color.colorConfirm));
                }

                if(!booking.booking_start_date.equals("")) {

                    String[] startDateArr = booking.booking_start_date.split(" ");

                    if(startDateArr.length > 0) {
                        String[] startDate = startDateArr[0].split("-");

                        if(startDate.length > 0) {
                            binding.get().startYearTextView.setText(startDate[0]);
                        }

                        if(startDate.length > 1) {
                            binding.get().startMonthTextView.setText(startDate[1]);
                        }

                        if(startDate.length > 2) {
                            binding.get().startDayTextView.setText(startDate[2]);
                        }
                    }

                }else {
                    binding.get().startDayTextView.setVisibility(View.GONE);
                    binding.get().startMonthTextView.setVisibility(View.GONE);
                    binding.get().startYearTextView.setVisibility(View.GONE);
                }


                if(!booking.booking_end_date.equals("")) {

                    String[] endDateArr = booking.booking_end_date.split(" ");

                    if(endDateArr.length > 0) {
                        String[] endDate = endDateArr[0].split("-");

                        if(endDate.length > 0) {
                            binding.get().endYearTextView.setText(endDate[0]);
                        }

                        if(endDate.length > 1) {
                            binding.get().endMonthTextView.setText(endDate[1]);
                        }

                        if(endDate.length > 2) {
                            binding.get().endDayTextView.setText(endDate[2]);
                        }
                    }

                }else {
                    binding.get().endDayTextView.setVisibility(View.GONE);
                    binding.get().endMonthTextView.setVisibility(View.GONE);
                    binding.get().endYearTextView.setVisibility(View.GONE);
                }



            } else {
                binding.get().getRoot().setVisibility(View.GONE);
            }
        });


    }

}