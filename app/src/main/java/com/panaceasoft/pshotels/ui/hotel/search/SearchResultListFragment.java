package com.panaceasoft.pshotels.ui.hotel.search;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentNewsListBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.hotel.adapter.HotelListAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotel.SearchHotelViewModel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.SearchHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

/**
 * NewsListFragment
 *
 * This fragment will use in difference activities to show
 * news list.
 *
 */
public class SearchResultListFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private SearchHotelViewModel searchHotelViewModel;
    private String selectedHotelStars = "";
    private Integer selectedPriceMin = 0;
    private Integer selectedPriceMax = 0;
    private Integer selectedRatings = 0;
    private String selectedFeatures = "";

    @VisibleForTesting
    AutoClearedValue<FragmentNewsListBinding> binding;
    AutoClearedValue<HotelListAdapter> adapter;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentNewsListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }
        binding.get().setLoadingMore(connectivity.isConnected());

        Utils.psLog(" Search News ");

        setHasOptionsMenu(true);

        return binding.get().getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if(getActivity() != null) {
            inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.filter_menu, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.filter_menu:
                navigationController.navigateToRoomFilterActivity(getActivity(),
                        searchHotelViewModel.cityId,
                        searchHotelViewModel.hotelMinPrice,
                        searchHotelViewModel.hotelMaxPrice,
                        searchHotelViewModel.hotelStarRating,
                        searchHotelViewModel.minUserRating,
                        searchHotelViewModel.filterByInfoType);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {

            if (requestCode == 1) {

                selectedHotelStars = data.getStringExtra("hotelStars");
                selectedPriceMin = data.getIntExtra("priceMin",0);
                selectedPriceMax = data.getIntExtra("priceMax", 0);
                selectedRatings = data.getIntExtra("guestRatings", 0);
                selectedFeatures = data.getStringExtra("hotelFeatures");

                Utils.psLog("selectedHotelStars >>>>>>  " + selectedHotelStars);
                Utils.psLog("selectedPriceMin >>>>>>  " + selectedPriceMin);
                Utils.psLog("selectedPriceMax >>>>>>  " + selectedPriceMax);
                Utils.psLog("selectedRatings >>>>>>  " + selectedRatings);
                Utils.psLog("selectedFeatures >>>>>>  " + selectedFeatures);

                searchHotelViewModel.hotelStarRating = selectedHotelStars;
                searchHotelViewModel.hotelMinPrice = Integer.toString(selectedPriceMin);
                searchHotelViewModel.hotelMaxPrice = Integer.toString(selectedPriceMax);
                searchHotelViewModel.filterByInfoType = selectedFeatures;
                searchHotelViewModel.minUserRating = Integer.toString(selectedRatings);
                searchHotelViewModel.filterByInfoType = selectedFeatures;


                //navigationController.navigateToSearchResultListActivity(getActivity(), selectedHotelStars, selectedPriceMin, selectedPriceMax, selectedRatings, searchHotelViewModel.cityId, searchHotelViewModel.hotelName, selectedFeatures);


//                List<Hotel> blankHotelList = new ArrayList<>();
//                adapter.get().replace(blankHotelList);
//                binding.get().executePendingBindings();
                // reset searchHotelViewModel.offset
                searchHotelViewModel.offset = 0;

                // reset searchHotelViewModel.forceEndLoading
                searchHotelViewModel.forceEndLoading = false;

                // update live data
                searchHotelViewModel.setSearchHotelList( loginUserId,
                        Utils.START_OF_OFFSET,
                        searchHotelViewModel.cityId,
                        searchHotelViewModel.hotelName,
                        searchHotelViewModel.hotelStarRating,
                        searchHotelViewModel.hotelMinPrice,
                        searchHotelViewModel.hotelMaxPrice,
                        searchHotelViewModel.filterByInfoType,
                        searchHotelViewModel.minUserRating);

            }

        } catch (Exception e) {
            Utils.psErrorLog("Error In onActivityResult From SearchResultListFragment",e);
        }
    }

    @Override
    protected void initUIAndActions() {
        binding.get().recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if(!binding.get().getLoadingMore() && !searchHotelViewModel.forceEndLoading) {

                        if(connectivity.isConnected()) {

                            int limit = Config.SEARCH_COUNT;
                            searchHotelViewModel.offset = searchHotelViewModel.offset + limit;

                            SearchHotelLoadingHolder holder = new SearchHotelLoadingHolder(String.valueOf(limit),
                                    searchHotelViewModel.offset+"",
                                    Config.API_KEY,
                                    loginUserId,
                                    searchHotelViewModel.cityId,
                                    searchHotelViewModel.hotelName,
                                    searchHotelViewModel.hotelStarRating,
                                    searchHotelViewModel.hotelMinPrice,
                                    searchHotelViewModel.hotelMaxPrice,
                                    searchHotelViewModel.filterByInfoType,
                                    searchHotelViewModel.minUserRating
                            );

                            searchHotelViewModel.loadNextPage(holder);
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset searchHotelViewModel.offset
            searchHotelViewModel.offset = 0;

            // reset searchHotelViewModel.forceEndLoading
            searchHotelViewModel.forceEndLoading = false;

            // update live data
            searchHotelViewModel.setSearchHotelList( loginUserId,
                    Utils.START_OF_OFFSET,
                    searchHotelViewModel.cityId,
                    searchHotelViewModel.hotelName,
                    searchHotelViewModel.hotelStarRating,
                    searchHotelViewModel.hotelMinPrice,
                    searchHotelViewModel.hotelMaxPrice,
                    searchHotelViewModel.filterByInfoType,
                    searchHotelViewModel.minUserRating);

        });
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        searchHotelViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchHotelViewModel.class);

    }

    @Override
    protected void initAdapters() {
        HotelListAdapter nvAdapter = new HotelListAdapter(dataBindingComponent,
                hotel -> navigationController.navigateToHotelViewActivity(getActivity(), hotel.hotel_id, hotel.hotel_name ));
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        if(getActivity() != null) {
            try {

                searchHotelViewModel.cityId = getActivity().getIntent().getStringExtra("cityId");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : city id.", e);
            }

            try {
                searchHotelViewModel.hotelName = getActivity().getIntent().getStringExtra("hotelName");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : hotel name.", e);
            }

            try {
                searchHotelViewModel.hotelStarRating = getActivity().getIntent().getStringExtra("hotelStars");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : hotel stars rating.", e);
            }

            try {
                searchHotelViewModel.hotelMinPrice = String.valueOf(getActivity().getIntent().getIntExtra("hotelMinPrice",0));
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : min price.", e);
            }

            try {
                searchHotelViewModel.hotelMaxPrice = String.valueOf(getActivity().getIntent().getIntExtra("hotelMaxPrice",0));
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : max price.", e);
            }

            try {
                searchHotelViewModel.filterByInfoType = getActivity().getIntent().getStringExtra("filterByInfoType");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : features.", e);
            }

            try {
                searchHotelViewModel.minUserRating = String.valueOf(getActivity().getIntent().getIntExtra("minUserRating",0));
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : user ratings.", e);
            }
        }

        loadNews();
    }

    //endregion


    //region Private Methods

    private void loadNews() {

        searchHotelViewModel.setSearchHotelList( loginUserId,
                                                Utils.START_OF_OFFSET,
                                                searchHotelViewModel.cityId,
                                                searchHotelViewModel.hotelName,
                                                searchHotelViewModel.hotelStarRating,
                                                searchHotelViewModel.hotelMinPrice,
                                                searchHotelViewModel.hotelMaxPrice,
                                                searchHotelViewModel.filterByInfoType,
                                                searchHotelViewModel.minUserRating);

        //( String.valueOf(Config.SEARCH_COUNT), Utils.START_OF_OFFSET, searchHotelViewModel.keywords, searchHotelViewModel.catId, loginUserId);
        LiveData<Resource<List<Hotel>>> hotelList  = searchHotelViewModel.getSearchHotelList();

        searchHotelViewModel.isLoading = connectivity.isConnected();
        binding.get().setLoadingMore(searchHotelViewModel.isLoading);
        binding.get().executePendingBindings();

        if(hotelList != null) {
            hotelList.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null) {
                    Utils.psLog("Got Hotel Data");

                    if (listResource.data.size() > 0) {
                        searchHotelViewModel.isLoading = false;
                    }

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());


                    binding.get().setLoadingMore(searchHotelViewModel.isLoading);

                    // Update the data
                    adapter.get().replace(listResource.data);
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if(listResource != null && listResource.message != null){

                    if(connectivity.isConnected()) {
                        Toast.makeText(getContext(), getString(R.string.error_message__no_result), Toast.LENGTH_SHORT).show();
                        adapter.get().replace(listResource.data);
                        binding.get().executePendingBindings();
                    }else {
                        Toast.makeText(getContext(), getString(R.string.error_message__no_internet), Toast.LENGTH_SHORT).show();
                    }
                    binding.get().setLoadingMore(false);

                }else {
                    //noinspection ConstantConditions
                    if (searchHotelViewModel.offset > 1) {
                        searchHotelViewModel.forceEndLoading = true;
                    }
                }
            });
        }

        searchHotelViewModel.getLoadMoreStatus().observe(this, loadingMore -> {
            if (loadingMore == null && !searchHotelViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if(loadingMore!=null && !searchHotelViewModel.isLoading) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if(loadingMore !=null ) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {
                        //Snackbar.make(binding.get().loadMoreBar, error, Snackbar.LENGTH_LONG).show();
                        searchHotelViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

    }

    //endregion


}
