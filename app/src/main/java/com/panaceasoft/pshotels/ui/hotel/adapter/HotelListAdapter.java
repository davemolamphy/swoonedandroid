package com.panaceasoft.pshotels.ui.hotel.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.HomeHotelItemBinding;
import com.panaceasoft.pshotels.ui.common.DataBoundListAdapter;
import com.panaceasoft.pshotels.ui.common.DataBoundViewHolder;
import com.panaceasoft.pshotels.utils.Objects;
import com.panaceasoft.pshotels.viewobject.Hotel;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelListAdapter extends DataBoundListAdapter<Hotel, HomeHotelItemBinding> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final HotelClickCallback callback;
    private int lastPosition = -1;


    public HotelListAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                            HotelClickCallback callback) {

        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
    }

    @Override
    protected HomeHotelItemBinding createBinding(ViewGroup parent) {
        HomeHotelItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.home_hotel_item, parent, false,
                        dataBindingComponent);

        binding.getRoot().setOnClickListener(v -> {
            Hotel hotel = binding.getHotel();
            if (hotel != null && callback != null) {
                callback.onClick(hotel);
            }
        });

        return binding;
    }

    // For general animation
    @Override
    public void bindView(DataBoundViewHolder<HomeHotelItemBinding> holder, int position) {
        super.bindView(holder, position);

        setAnimation(holder.itemView, position);
    }

    @Override
    protected void bind(HomeHotelItemBinding binding, Hotel item) {
        binding.setHotel(item);

        if (item.promotion != null && !item.promotion.promo_percent.equals("")) {

            int promo_percent = Integer.parseInt(item.promotion.promo_percent);

            if (promo_percent <= 0) {

                binding.promotionLayout.setVisibility(View.GONE);
            } else {

                binding.promotionLayout.setVisibility(View.VISIBLE);
            }
        } else {

            binding.promotionLayout.setVisibility(View.GONE);
        }

        if (item.hotel_star_rating != null) {

            int rating = Integer.parseInt(item.hotel_star_rating);

            if (rating < 5) binding.imageView12.setVisibility(View.GONE);

            if (rating < 4) binding.imageView15.setVisibility(View.GONE);

            if (rating < 3) binding.imageView14.setVisibility(View.GONE);

            if (rating < 2) binding.imageView7.setVisibility(View.GONE);

            if (rating < 1) binding.imageView10.setVisibility(View.GONE);

        } else {

            binding.imageView10.setVisibility(View.GONE);
            binding.imageView7.setVisibility(View.GONE);
            binding.imageView14.setVisibility(View.GONE);
            binding.imageView15.setVisibility(View.GONE);
            binding.imageView12.setVisibility(View.GONE);
        }
    }

    @Override
    protected boolean areItemsTheSame(Hotel oldItem, Hotel newItem) {
        return Objects.equals(oldItem.hotel_id, newItem.hotel_id) && Objects.equals(oldItem.rating.review_count, newItem.rating.review_count);
    }

    @Override
    protected boolean areContentsTheSame(Hotel oldItem, Hotel newItem) {
        return Objects.equals(oldItem.hotel_id, newItem.hotel_id) && Objects.equals(oldItem.rating.review_count, newItem.rating.review_count);
    }

    public interface HotelClickCallback {
        void onClick(Hotel hotel);
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else {
            lastPosition = position;
        }
    }

}
