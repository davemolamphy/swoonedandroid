package com.panaceasoft.pshotels.ui.hotel.hotelView;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityHotelViewBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class HotelViewActivity extends PSAppCompactActivity {

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityHotelViewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_hotel_view);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityHotelViewBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getHotelName());

        // setup Fragment
        setupFragment(new HotelViewFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    private String getHotelName() {
        return this.getIntent().getStringExtra("hotel_name");
    }

    //endregion


}