package com.panaceasoft.pshotels.ui.welcome;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentWelcomeBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.welcome.adapter.WelcomeViewPagerAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 16/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */
public class WelcomeFragment extends PSFragment {


    //region Variables
    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    WelcomeViewPagerAdapter welcomeViewPagerAdapter;
    TextView[] pages;
    int[] welcomeScreen;

    @Inject
    SharedPreferences pref;
    SharedPreferences.Editor editor;



    @VisibleForTesting
    AutoClearedValue<FragmentWelcomeBinding> binding;

    //endregion

    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentWelcomeBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        welcomeScreen = new int[]{
                R.layout.welcome_screen_1,
                R.layout.welcome_screen_2,
                R.layout.welcome_screen_3,
                R.layout.welcome_screen_4,
                R.layout.welcome_screen_5
        };

        addBottomDots(0);



        binding.get().btnSkip.setOnClickListener(v -> launchHomeScreen());

        binding.get().btnNext.setOnClickListener(v -> {
            int current = getItem(+1);
            if (current < welcomeScreen.length) {
                binding.get().viewPager.setCurrentItem(current);
            } else {
                launchHomeScreen();
            }
        });

    }

    @Override
    protected void initViewModels() {

    }

    @Override
    protected void initAdapters() {

        ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);


                if (position == welcomeScreen.length - 1) {
                    binding.get().btnNext.setText(getString(R.string.start));
                    binding.get().btnSkip.setVisibility(View.GONE);
                } else {
                    binding.get().btnNext.setText(getString(R.string.next));
                    binding.get().btnSkip.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        };

        welcomeViewPagerAdapter = new WelcomeViewPagerAdapter(welcomeScreen, getContext());
        binding.get().viewPager.setAdapter(welcomeViewPagerAdapter);
        binding.get().viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    @Override
    protected void initData() {


    }

    //endregion

    private int getItem(int i) {
        return binding.get().viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {

        editor = pref.edit();
        editor.putBoolean(Utils.IS_FIRST_TIME_LAUNCH, false);
        editor.apply();

        if(getActivity() != null) {
            getActivity().finish();
        }
    }

    private void addBottomDots(int currentPage) {
        pages = new TextView[welcomeScreen.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);


        binding.get().layoutDots.removeAllViews();

        for (int i = 0; i < pages.length; i++) {
            pages[i] = new TextView(getContext());
            pages[i].setText(Html.fromHtml("&#8226;"));
            pages[i].setTextSize(33);
            pages[i].setTextColor(colorsInactive[currentPage]);

            binding.get().layoutDots.addView(pages[i]);
        }

        if (pages.length > 0)
            pages[currentPage].setTextColor(colorsActive[currentPage]);
    }


}
