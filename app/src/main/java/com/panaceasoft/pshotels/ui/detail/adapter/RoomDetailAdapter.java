package com.panaceasoft.pshotels.ui.detail.adapter;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.RoomDetailFeatureHeaderViewBinding;
import com.panaceasoft.pshotels.databinding.RoomDetailFeatureViewBinding;
import com.panaceasoft.pshotels.databinding.RoomDetailViewBinding;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.RoomFeatureDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/2/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class RoomDetailAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final InquiryClickCallback inquiryClickCallback;
    private final RoomImageClickCallback roomImageClickCallback;
    private final ReviewListClickCallback reviewListClickCallback;
    private final BookRoomClickCallback bookRoomClickCallback;

    private final int ROOM_DETAIL_VIEW = 1;
    private final int ROOM_FEATURE_HEADER_VIEW = ROOM_DETAIL_VIEW + 1;
    private final int ROOM_FEATURE_VIEW = ROOM_FEATURE_HEADER_VIEW + 1;

    private List<RoomFeatures> roomFeaturesList = new ArrayList<>();

    private Room room;

    public RoomDetailAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                             InquiryClickCallback callback,
                             ReviewListClickCallback reviewListClickCallback,
                             RoomImageClickCallback roomImageClickCallback,
                             BookRoomClickCallback bookRoomClickCallback) {

        this.dataBindingComponent = dataBindingComponent;
        this.inquiryClickCallback = callback;
        this.reviewListClickCallback = reviewListClickCallback;
        this.roomImageClickCallback = roomImageClickCallback;
        this.bookRoomClickCallback = bookRoomClickCallback;

    }

    //region Override Methods

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;

        switch (viewType) {
            case ROOM_DETAIL_VIEW:

                RoomDetailViewBinding binding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.room_detail_view, parent, false,
                                dataBindingComponent);

                binding.inquiryButton.setOnClickListener(v -> {
                    Room room = binding.getRoom();
                    if (room != null && inquiryClickCallback != null) {
                        inquiryClickCallback.onClick(room);
                    }
                });

                binding.reviewCountTextView.setOnClickListener(v -> {
                    Room room = binding.getRoom();
                    if (room != null && reviewListClickCallback != null) {
                        reviewListClickCallback.onClick(room);
                    }
                });

                binding.roomImageView.setOnClickListener(v -> {
                    Room room = binding.getRoom();
                    if (room != null && roomImageClickCallback != null) {
                        roomImageClickCallback.onClick(room);
                    }
                });

                binding.bookingButton.setOnClickListener(v -> {
                    Room room = binding.getRoom();
                    if ( room != null && bookRoomClickCallback != null ) {
                        bookRoomClickCallback.onClick(room);
                    }
                });

                holder = new RoomDetailViewHolder(binding);

                break;
            case ROOM_FEATURE_HEADER_VIEW:

                RoomDetailFeatureHeaderViewBinding binding2 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.room_detail_feature_header_view, parent, false,
                                dataBindingComponent);

                holder = new RoomFeatureHeaderViewHolder(binding2);

                break;

            case ROOM_FEATURE_VIEW:

                RoomDetailFeatureViewBinding binding3 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.room_detail_feature_view, parent, false,
                                dataBindingComponent);

                holder = new RoomFeatureViewHolder(binding3);

                break;

            default:


                RoomDetailViewBinding binding4 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.room_detail_view, parent, false,
                                dataBindingComponent);
                binding4.getRoot().setOnClickListener(v -> {
                });
                binding4.getRoot().setVisibility(View.GONE);
                holder = new RoomDetailViewHolder(binding4);

                break;

        }

        return holder;
    }

    @Override
    public int getItemCount() {
        return getTotalItemCount();
    }

    @Override
    public int getItemViewType(int position) {

        if(position == 0 ) {
            return ROOM_DETAIL_VIEW;
        }else {
            if(isHeader(position) ) {
                return ROOM_FEATURE_HEADER_VIEW;
            }else {
                return ROOM_FEATURE_VIEW;
            }
        }

    }

    private boolean isHeader(int position) {
        if (position == 0) {
            return false;
        }

        if (position == 1) {
            return true;
        }

        int itemCount = 0;
        int header = 1;
        for(int i=0; i<roomFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            if(itemCount == position) {
                return true;
            }

            itemCount += roomFeaturesList.get(i).rinf_types_arr.size();

            if(itemCount > position) {
                break;
            }
        }

        return false;
    }

    private int getTotalItemCount() {

        int roomDetailCount = 1;
        int roomFeatureHeaderCount = roomFeaturesList.size();
        int roomFeatureCount  = 0;
        for(int i=0; i<roomFeatureHeaderCount; i++ ) {
            roomFeatureCount += roomFeaturesList.get(i).rinf_types_arr.size();
        }

        return roomDetailCount + roomFeatureHeaderCount + roomFeatureCount;
    }

    private RoomFeatures getFeatureType(int position) {

        if(position == 1) {
            return this.roomFeaturesList.get(position - 1);
        }

        int itemCount = 0;
        int header = 1;
        for(int i=0; i<roomFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            if(itemCount == position) {
                return this.roomFeaturesList.get(i);
            }

            itemCount += roomFeaturesList.get(i).rinf_types_arr.size();

        }

        return null;

    }

    private RoomFeatureDetail getFeatureDetail(int position) {
        int itemCount = 0;
        int header = 1;
        for(int i=0; i<roomFeaturesList.size(); i++) {

            // Added Header
            itemCount += header;

            int iItemSize = roomFeaturesList.get(i).rinf_types_arr.size();
            if( position > itemCount + iItemSize) {
                itemCount += roomFeaturesList.get(i).rinf_types_arr.size();
            }else {
                int convertedPosition = position - itemCount - 1;
                return this.roomFeaturesList.get(i).rinf_types_arr.get(convertedPosition);
            }

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case ROOM_DETAIL_VIEW:
                if(holder instanceof RoomDetailViewHolder) {
                    setupRoomSummaryView((RoomDetailViewHolder) holder, position);
                }
                break;
            case ROOM_FEATURE_HEADER_VIEW:
                if(holder instanceof RoomFeatureHeaderViewHolder) {
                    setupFeatureHeaderView((RoomFeatureHeaderViewHolder) holder, position);
                }
                break;
            case ROOM_FEATURE_VIEW:
                if(holder instanceof RoomFeatureViewHolder) {
                    setupFeatureView((RoomFeatureViewHolder) holder, position);
                }
                break;
            default:
                // nothing
                break;

        }

    }

    private void setupRoomSummaryView(RoomDetailViewHolder holder, int position) {

        if(this.room != null) {

            try {

                holder.binding.setRoom(this.room);

                if (this.room.is_available.equals("0")) {
                    holder.binding.bookingButton.setText(holder.binding.getRoot().getResources().getString(R.string.booking_not_available));
                    holder.binding.bookingButton.setEnabled(false);
                } else {
                    holder.binding.bookingButton.setText(holder.binding.getRoot().getResources().getString(R.string.book_this_room));
                    holder.binding.bookingButton.setEnabled(true);
                }



            } catch (Exception e) {
                Utils.psErrorLog("setupRoomSummaryView", e);
            }


            if (this.room.promotion != null && !this.room.promotion.promo_percent.equals("")) {

                holder.binding.discountPriceTextView.setVisibility(View.VISIBLE);
                holder.binding.priceRangeTextView.setPaintFlags(holder.binding.priceRangeTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                float discountPrice = (Float.parseFloat(this.room.promotion.promo_percent) / 100) * Float.parseFloat(this.room.room_price);
                String discountPriceStr = this.room.currency_symbol + (int) discountPrice;
                holder.binding.discountPriceTextView.setText(discountPriceStr);

                String priceStr = room.currency_symbol + room.room_price;
                holder.binding.priceRangeTextView.setText(priceStr);
                holder.binding.priceRangeTextView.setTextColor(holder.binding.getRoot().getResources().getColor(R.color.colorText));

            } else {

                holder.binding.discountPriceTextView.setVisibility(View.GONE);
                holder.binding.priceRangeTextView.setTextColor(holder.binding.getRoot().getResources().getColor(R.color.colorPromotion));
                String priceStr = room.currency_symbol + room.room_price;
                holder.binding.priceRangeTextView.setText(priceStr);
                holder.binding.priceRangeTextView.setPaintFlags(holder.binding.priceRangeTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));

            }

        }



    }

    private void setupFeatureHeaderView(RoomFeatureHeaderViewHolder holder, int position) {

        if(this.roomFeaturesList != null && this.roomFeaturesList.size() > 0) {
            RoomFeatures featureType = getFeatureType(position);
            holder.binding.setFeatureType(featureType);
        }

    }

    private void setupFeatureView(RoomFeatureViewHolder holder, int position) {

        if(this.roomFeaturesList != null  && this.roomFeaturesList.size() > 0) {
            RoomFeatureDetail roomFeatureDetail = getFeatureDetail(position);
            holder.binding.setFeature(roomFeatureDetail);
        }

    }


    public class RoomDetailViewHolder extends RecyclerView.ViewHolder {
        RoomDetailViewBinding binding;

        RoomDetailViewHolder(RoomDetailViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class RoomFeatureHeaderViewHolder extends RecyclerView.ViewHolder {
        RoomDetailFeatureHeaderViewBinding binding;

        RoomFeatureHeaderViewHolder(RoomDetailFeatureHeaderViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class RoomFeatureViewHolder extends RecyclerView.ViewHolder {
        RoomDetailFeatureViewBinding binding;

        RoomFeatureViewHolder(RoomDetailFeatureViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ReviewListClickCallback {
        void onClick(Room room);
    }

    public interface InquiryClickCallback {
        void onClick(Room room);
    }

    public interface RoomImageClickCallback {
        void onClick(Room room);
    }

    public interface BookRoomClickCallback {
        void onClick(Room room);
    }

    public void replaceRoom(Room room) {
        this.room = room;
        notifyDataSetChanged();
    }

    public void replaceFeatureList(List<RoomFeatures> roomFeaturesList) {
        this.roomFeaturesList = roomFeaturesList;
        notifyDataSetChanged();
    }
}