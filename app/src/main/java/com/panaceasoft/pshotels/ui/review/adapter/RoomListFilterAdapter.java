package com.panaceasoft.pshotels.ui.review.adapter;


import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.RoomListFilterBinding;
import com.panaceasoft.pshotels.ui.common.DataBoundListAdapter;
import com.panaceasoft.pshotels.ui.common.DataBoundViewHolder;
import com.panaceasoft.pshotels.utils.Objects;
import com.panaceasoft.pshotels.viewobject.Room;

public class RoomListFilterAdapter extends DataBoundListAdapter<Room, RoomListFilterBinding> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final RoomListFilterAdapter.RoomClickCallback callback;
    private int lastPosition = -1;



    public RoomListFilterAdapter( android.databinding.DataBindingComponent dataBindingComponent,
                                  RoomListFilterAdapter.RoomClickCallback callback) {

        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;

    }

    @Override
    protected RoomListFilterBinding createBinding(ViewGroup parent) {

        RoomListFilterBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.room_list_filter, parent, false,
                        dataBindingComponent);
        binding.getRoot().setOnClickListener(v -> {
            Room city = binding.getRoom();
            if (city != null && callback != null) {
                callback.onClick(city);
            }
        });
        return binding;
    }

    // For general animation
    @Override
    public void bindView(DataBoundViewHolder<RoomListFilterBinding> holder, int position) {
        super.bindView(holder, position);

        setAnimation(holder.itemView, position);
    }

    @Override
    protected void bind(RoomListFilterBinding binding, Room item) {
        binding.setRoom(item);

    }

    @Override
    protected boolean areItemsTheSame(Room oldItem, Room newItem) {
        return Objects.equals(oldItem.room_id, newItem.room_id);
    }

    @Override
    protected boolean areContentsTheSame(Room oldItem, Room newItem) {
        return Objects.equals(oldItem.room_id, newItem.room_id);
    }

    public interface RoomClickCallback {
        void onClick(Room city);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }else{
            lastPosition = position;
        }
    }

}

