package com.panaceasoft.pshotels.ui.hotel.promotion;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentNewsListBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.hotel.adapter.HotelListAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotel.PromotionHotelViewModel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.PromotionHotelLoadingHolder;

import java.util.List;

public class PromotionHotelListFragment extends PSFragment {

    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private PromotionHotelViewModel promotionHotelViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentNewsListBinding> binding;
    AutoClearedValue<HotelListAdapter> adapter;

    //endregion

    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentNewsListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }


        binding.get().setLoadingMore(connectivity.isConnected());


        Utils.psLog(" Promotion Hotel ");

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {
        binding.get().recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if(!binding.get().getLoadingMore() && !promotionHotelViewModel.forceEndLoading) {

                        if(connectivity.isConnected()) {

                            int limit = Config.PROMOTION_HOTEL_COUNT;
                            promotionHotelViewModel.offset = promotionHotelViewModel.offset + limit;

                            PromotionHotelLoadingHolder holder = new PromotionHotelLoadingHolder(
                                    String.valueOf(limit),
                                    String.valueOf(promotionHotelViewModel.offset),
                                    loginUserId
                            );

                            promotionHotelViewModel.loadNextPage(holder);
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset promotionHotelViewModel.offset
            promotionHotelViewModel.offset = 0;

            // reset promotionHotelViewModel.forceEndLoading
            promotionHotelViewModel.forceEndLoading = false;

            // update live data
            promotionHotelViewModel.setPromotionHotelList(
                    String.valueOf(Config.PROMOTION_HOTEL_COUNT),
                    String.valueOf(promotionHotelViewModel.offset),
                    loginUserId
            );

        });
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        promotionHotelViewModel = ViewModelProviders.of(this, viewModelFactory).get(PromotionHotelViewModel.class);

    }

    @Override
    protected void initAdapters() {
        HotelListAdapter nvAdapter = new HotelListAdapter(dataBindingComponent,hotel -> navigationController.navigateToHotelViewActivity(getActivity(), hotel.hotel_id, hotel.hotel_name ));
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        loadHotel();
    }

    //endregion


    //region Private Methods

    private void loadHotel() {

        promotionHotelViewModel.setPromotionHotelList(
                String.valueOf(Config.PROMOTION_HOTEL_COUNT),
                String.valueOf(promotionHotelViewModel.offset),
                loginUserId
        );

        LiveData<Resource<List<Hotel>>> hotel = promotionHotelViewModel.getPromotionHotelList();

        promotionHotelViewModel.isLoading = true;
        binding.get().setLoadingMore(promotionHotelViewModel.isLoading);
        binding.get().executePendingBindings();

        if(hotel != null) {
            hotel.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null && listResource.message == null) {
                    Utils.psLog("Got Promotions Data");

                    if (listResource.data.size() > 0) {
                        promotionHotelViewModel.isLoading = false;
                    }

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());

                    binding.get().setLoadingMore(promotionHotelViewModel.isLoading);

                    // Update the data
                    adapter.get().replace(listResource.data);
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if(listResource != null && listResource.message != null) {
                    binding.get().swipeRefresh.setRefreshing(false);
                    binding.get().setLoadingMore(false);
                }else {
                    //noinspection ConstantConditions
                    if (promotionHotelViewModel.offset > 1) {
                        promotionHotelViewModel.forceEndLoading = true;
                    }
                }
            });
        }

        promotionHotelViewModel.getLoadMoreStatus().observe(this, loadingMore -> {
            if (loadingMore == null && !promotionHotelViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if(!promotionHotelViewModel.isLoading && loadingMore != null) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if(loadingMore != null) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {
                        promotionHotelViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

    }

    //endregion


}
