package com.panaceasoft.pshotels.ui.gallery;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentGalleryDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.TouchImageView;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.image.ImageViewModel;
import com.panaceasoft.pshotels.viewobject.Image;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Gallery Detail Fragment
 */
public class GalleryDetailFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    @VisibleForTesting
    AutoClearedValue<FragmentGalleryDetailBinding> binding;

    private ImageViewModel imageViewModel;

    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentGalleryDetailBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery_detail, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        binding.get().viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageScrollStateChanged(int arg0) {

            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            public void onPageSelected(int currentPage) {

                if(imageViewModel.newsImageList != null) {
                    if (currentPage >= imageViewModel.newsImageList.size()) {
                        currentPage = currentPage % imageViewModel.newsImageList.size();
                    }

                    binding.get().imgDesc.setText(imageViewModel.newsImageList.get(currentPage).img_desc);
                }

            }

        });
    }

    @Override
    protected void initViewModels() {
        imageViewModel = ViewModelProviders.of(this, viewModelFactory).get(ImageViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {
        try {

            if(getActivity() != null) {
                imageViewModel.newsId = getActivity().getIntent().getStringExtra("news_id");
                imageViewModel.imgId = getActivity().getIntent().getStringExtra("img_id");
            }

        }catch (Exception e){
            Utils.psErrorLog("Error in getting intent.", e);
        }



        LiveData<Resource<List<Image>>> imageListLiveData = imageViewModel.getImageListLiveData();
        imageViewModel.setImageParentId(imageViewModel.newsId);
        imageListLiveData.observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");

                // Update the data
                imageViewModel.newsImageList = listResource.data;

                int selectedItemPosition = 0;

                for (int i=0; i<imageViewModel.newsImageList.size(); i++) {
                    if(imageViewModel.newsImageList.get(i).img_id.equals(imageViewModel.imgId)) {
                        selectedItemPosition = i;
                        break;
                    }
                }

                binding.get().viewPager.setAdapter(new TouchImageAdapter());
                binding.get().viewPager.setCurrentItem(selectedItemPosition);

                binding.get().imgDesc.setText(imageViewModel.newsImageList.get(selectedItemPosition).img_desc);

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });
    }

    //endregion

    
    //region Pager Adapter Class

    class TouchImageAdapter extends PagerAdapter {

        private TouchImageAdapter(){
        }

        @Override
        public int getCount() {
            if(imageViewModel.newsImageList != null) {
                return imageViewModel.newsImageList.size();
            }

            return 0;

        }

        @Override
        @NonNull
        public View instantiateItem(@NonNull ViewGroup container, int position) {

            TouchImageView imgView = new TouchImageView(container.getContext());
            if(imageViewModel.newsImageList != null) {
                if (position >= imageViewModel.newsImageList.size()) {
                    position = position % imageViewModel.newsImageList.size();
                }

                if(getActivity() != null) {
                    Glide.with(getActivity()).load(Config.APP_IMAGES_URL + imageViewModel.newsImageList.get(position).img_path).apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_image)
                            .centerCrop()
                            .dontAnimate()
                            .dontTransform()).into(imgView);


                    container.addView(imgView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                }
            }
            return imgView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        fixLeakCanary696(getActivity().getApplicationContext());
    }

    static void fixLeakCanary696(Context context) {
//        if (!isEmui()) {
//            Lo "not emui");
//            return;
//        }

        //https://github.com/square/leakcanary/issues/696
        try {
            Class clazz = Class.forName("android.gestureboost.GestureBoostManager");
            Utils.psLog("clazz " + clazz);

            Field _sGestureBoostManager = clazz.getDeclaredField("sGestureBoostManager");
            _sGestureBoostManager.setAccessible(true);
            Field _mContext = clazz.getDeclaredField("mContext");
            _mContext.setAccessible(true);

            Object sGestureBoostManager = _sGestureBoostManager.get(null);
            if (sGestureBoostManager != null) {
                _mContext.set(sGestureBoostManager, context);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    //endregion

}
