package com.panaceasoft.pshotels.ui.booking.list;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentBookingListBinding;
import com.panaceasoft.pshotels.ui.booking.list.adapter.BookingAdapter;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.booking.BookingViewModel;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingListFragment extends PSFragment {

    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private BookingViewModel bookingViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentBookingListBinding> binding;
    AutoClearedValue<BookingAdapter> adapter;

    //endregion

    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentBookingListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_list, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if (Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        } else {
            binding.get().adView.setVisibility(View.GONE);
        }


        binding.get().setLoadingMore(connectivity.isConnected());


        Utils.psLog(" Popular Hotel ");

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {
        binding.get().bookingList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {

                    if (!binding.get().getLoadingMore() && !bookingViewModel.forceEndLoading) {

                        if (connectivity.isConnected()) {

                            int limit = Config.BOOKING_COUNT;
                            bookingViewModel.offset = bookingViewModel.offset + limit;

                            bookingViewModel.loadNextPage(loginUserId, String.valueOf(limit), String.valueOf(bookingViewModel.offset));
                        }

                    }
                }
            }
        });

        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {

            // reset bookingViewModel.offset
            bookingViewModel.offset = 0;

            // reset bookingViewModel.forceEndLoading
            bookingViewModel.forceEndLoading = false;

            // update live data
            bookingViewModel.setBookingObj(
                    String.valueOf(Config.BOOKING_COUNT),
                    String.valueOf(bookingViewModel.offset),
                    loginUserId
            );

        });
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        bookingViewModel = ViewModelProviders.of(this, viewModelFactory).get(BookingViewModel.class);

    }

    @Override
    protected void initAdapters() {
        BookingAdapter nvAdapter = new BookingAdapter(dataBindingComponent, booking -> navigationController.navigateToBookingDetailViewActivity(getActivity(), booking));
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().bookingList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        loadHotel();
    }

    //endregion


    //region Private Methods

    private void loadHotel() {

        bookingViewModel.setBookingObj(
                String.valueOf(Config.BOOKING_COUNT),
                String.valueOf(bookingViewModel.offset),
                loginUserId
        );

        LiveData<Resource<List<Booking>>> bookingList = bookingViewModel.getBookingList();

        bookingViewModel.isLoading = true;
        binding.get().setLoadingMore(bookingViewModel.isLoading);
        binding.get().executePendingBindings();

        if (bookingList != null) {
            bookingList.observe(this, listResource -> {
                // we don't need any null checks here for the adapter since LiveData guarantees that
                // it won't call us if fragment is stopped or not started.
                if (listResource != null && listResource.data != null && listResource.message == null) {
                    Utils.psLog("Got Booking Data");

                    if (listResource.data.size() > 0) {
                        bookingViewModel.isLoading = false;
                    }

                    //fadeIn Animation
                    fadeIn(binding.get().getRoot());

                    binding.get().setLoadingMore(bookingViewModel.isLoading);

                    // Update the data
                    adapter.get().replace(listResource.data);
                    binding.get().executePendingBindings();

                    binding.get().swipeRefresh.setRefreshing(false);

                } else if (listResource != null && listResource.message != null) {
                    binding.get().swipeRefresh.setRefreshing(false);
                    binding.get().setLoadingMore(false);
                } else {
                    //noinspection ConstantConditions
                    if (bookingViewModel.offset > 1) {
                        bookingViewModel.forceEndLoading = true;
                    }
                }
            });
        }


        bookingViewModel.getLoadingStatus().observe(this, loadingMore -> {
            if (loadingMore == null && !bookingViewModel.isLoading) {
                binding.get().setLoadingMore(false);

            } else {

                if (!bookingViewModel.isLoading && loadingMore != null) {
                    binding.get().setLoadingMore(loadingMore.isRunning());
                }

                if (loadingMore != null) {
                    String error = loadingMore.getErrorMessageIfNotHandled();
                    if (error != null) {
                        bookingViewModel.forceEndLoading = true;
                    }
                }
            }
            binding.get().executePendingBindings();
        });

    }

    //endregion


}
