package com.panaceasoft.pshotels.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityUserForgotPasswordBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class UserForgotPasswordActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUserForgotPasswordBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user_forgot_password);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityUserForgotPasswordBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.forgot_password));

        // setup Fragment
        setupFragment(new UserForgotPasswordFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}