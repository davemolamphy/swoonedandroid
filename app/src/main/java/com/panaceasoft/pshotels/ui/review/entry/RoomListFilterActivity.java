package com.panaceasoft.pshotels.ui.review.entry;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityRoomListFilterBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class RoomListFilterActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityRoomListFilterBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_room_list_filter);

        // Init all UI
        initUI(binding);
    }

    //endregion


    //region Private Methods

    private void initUI(ActivityRoomListFilterBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.filter_room));

        // setup Fragment
        setupFragment(new RoomListFilterFragment());

        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);
    }

    //endregion
}
