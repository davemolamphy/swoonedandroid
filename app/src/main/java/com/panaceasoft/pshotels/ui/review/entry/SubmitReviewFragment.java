package com.panaceasoft.pshotels.ui.review.entry;


import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentSubmitReviewBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.review.SubmitReviewViewModel;
import com.panaceasoft.pshotels.viewobject.PostRating;
import com.panaceasoft.pshotels.viewobject.holder.SyncUserRatingHolder;

import java.util.ArrayList;
import java.util.List;

public class SubmitReviewFragment extends PSFragment {

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private SubmitReviewViewModel submitReviewViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentSubmitReviewBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSubmitReviewBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_submit_review, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);
        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().btnSubmit.setOnClickListener(view -> {

            if(connectivity.isConnected()) {

                String reviewDesc = binding.get().contactDescEditText.getText().toString().trim();

                if (reviewDesc.equals("")) {

                    Toast.makeText(getContext(), getString(R.string.error_message__blank_message), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(submitReviewViewModel.roomId.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__select_room), Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( !submitReviewViewModel.isLoading ) {

                    submitReviewViewModel.reviewDesc = reviewDesc;
                    doSubmit();
                }

            }else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }
        });

        setSeekBarListeners();

        // Location
        binding.get().findYourLocationTextView.setOnClickListener(view -> {

            Utils.psLog("Open Room Filter Activity.");
            navigationController.navigateToRoomListFilterActivity(getActivity(), submitReviewViewModel.hotelId);
        });
    }

    private void setSeekBarListeners()
    {
        // Review Category 1
        binding.get().ratingSeekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.get().txtRating1.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Review Category 2
        binding.get().ratingSeekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.get().txtRating2.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Review Category 3
        binding.get().ratingSeekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.get().txtRating3.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Review Category 4
        binding.get().ratingSeekBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.get().txtRating4.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Review Category 5
        binding.get().ratingSeekBar5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.get().txtRating5.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void initViewModels() {

        submitReviewViewModel = ViewModelProviders.of(this, viewModelFactory).get(SubmitReviewViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        if(getActivity() != null) {
            try {

                submitReviewViewModel.hotelId = getActivity().getIntent().getStringExtra("hotelId");

                submitReviewViewModel.roomId = getActivity().getIntent().getStringExtra("roomId");

                if ( submitReviewViewModel.roomId.equals("")) {

                    binding.get().constraintLayout1.setVisibility(View.VISIBLE);
                } else {

                    binding.get().constraintLayout1.setVisibility(View.GONE);
                }
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }
        }

        loadReviewCategories();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {

            submitReviewViewModel.roomId = data.getStringExtra("roomId");
            submitReviewViewModel.roomName = data.getStringExtra("roomName");

            // set room to ui
            updateRoomName();

        } catch (Exception e) {

            Utils.psErrorLog("Error In onActivityResult From SearchFragment",e);
        }
    }

    private void updateRoomName() {

        this.binding.get().findYourLocationTextView.setText(submitReviewViewModel.roomName);
    }

    private void loadReviewCategories()
    {
        submitReviewViewModel.setReviewCategory();
        submitReviewViewModel.getAllReviewCategory().observe(this, listResource -> {

            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {

                Utils.psLog("Got Data");

                submitReviewViewModel.reviewCategories = listResource.data;

                if ( listResource.data.size() > 0 ) {

                    binding.get().txtName1.setText(listResource.data.get(0).rvcat_name);
                }
                if ( listResource.data.size() > 1 ) {

                    binding.get().txtName2.setText(listResource.data.get(1).rvcat_name);
                }
                if ( listResource.data.size() > 2 ) {

                    binding.get().txtName3.setText(listResource.data.get(2).rvcat_name);
                }
                if ( listResource.data.size() > 3 ) {

                    binding.get().txtName4.setText(listResource.data.get(3).rvcat_name);
                }
                if ( listResource.data.size() > 4 ) {

                    binding.get().txtName5.setText(listResource.data.get(4).rvcat_name);
                }

                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });
    }

    private void doSubmit() {

        prgDialog.get().show();

        SyncUserRatingHolder syncUserRatingHolder = getRatings();

        submitReviewViewModel.postSubmitReview(
                Config.API_KEY,
                submitReviewViewModel.roomId,
                loginUserId,
                submitReviewViewModel.reviewDesc,
                syncUserRatingHolder
        );

        submitReviewViewModel.getLoadingStatus().observe(this, submitStatus -> {

            if ( submitStatus != null ) {

                // error
                String error = submitStatus.getErrorMessageIfNotHandled();

                if (error != null) {

                    Utils.psLog(error);

                    prgDialog.get().cancel();
                } else {

                    if (!submitStatus.isRunning()) {

                        // success
                        binding.get().contactDescEditText.setText("");

                        Toast.makeText(getContext(), getString(R.string.message__message_sent), Toast.LENGTH_SHORT).show();

                        prgDialog.get().cancel();

                        if(getActivity() != null) {
                            getActivity().finish();
                        }
                    }
                }
            }

            binding.get().executePendingBindings();
        });
    }

    private SyncUserRatingHolder getRatings()
    {
        List<PostRating> ratings = new ArrayList<>();

        if ( submitReviewViewModel.reviewCategories.size() > 0 && binding.get().ratingSeekBar1.getProgress() > 0 ) {

            PostRating rating = new PostRating(
                    submitReviewViewModel.reviewCategories.get(0).rvcat_id,
                    String.valueOf(binding.get().ratingSeekBar1.getProgress())
            );
            ratings.add(rating);
        }

        if ( submitReviewViewModel.reviewCategories.size() > 1 && binding.get().ratingSeekBar2.getProgress() > 0 ) {

            PostRating rating = new PostRating(
                    submitReviewViewModel.reviewCategories.get(1).rvcat_id,
                    String.valueOf(binding.get().ratingSeekBar2.getProgress())
            );
            ratings.add(rating);
        }

        if ( submitReviewViewModel.reviewCategories.size() > 2 && binding.get().ratingSeekBar3.getProgress() > 0 ) {

            PostRating rating = new PostRating(
                    submitReviewViewModel.reviewCategories.get(2).rvcat_id,
                    String.valueOf(binding.get().ratingSeekBar3.getProgress())
            );
            ratings.add(rating);
        }

        if ( submitReviewViewModel.reviewCategories.size() > 3 && binding.get().ratingSeekBar4.getProgress() > 0 ) {

            PostRating rating = new PostRating(
                    submitReviewViewModel.reviewCategories.get(3).rvcat_id,
                    String.valueOf(binding.get().ratingSeekBar4.getProgress())
            );
            ratings.add(rating);
        }

        if ( submitReviewViewModel.reviewCategories.size() > 4 && binding.get().ratingSeekBar5.getProgress() > 0 ) {

            PostRating rating = new PostRating(
                    submitReviewViewModel.reviewCategories.get(4).rvcat_id,
                    String.valueOf(binding.get().ratingSeekBar5.getProgress())
            );
            ratings.add(rating);
        }

        return new SyncUserRatingHolder(ratings);
    }
}