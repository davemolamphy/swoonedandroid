package com.panaceasoft.pshotels.ui.detail.room;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentRoomDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.detail.adapter.RoomDetailAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.room.RoomViewModel;
import com.panaceasoft.pshotels.viewmodel.roomInfo.RoomFeatureViewModel;
import com.panaceasoft.pshotels.viewobject.Room;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomDetailFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private RoomViewModel roomViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentRoomDetailBinding> binding;
    AutoClearedValue<RoomDetailAdapter> adapter;
    private RoomFeatureViewModel roomFeatureViewModel;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentRoomDetailBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_detail , container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }
        binding.get().setLoadingMore(connectivity.isConnected());

        Utils.psLog(" RoomDetailFragment ");

        return binding.get().getRoot();

    }

    @Override
    protected void initUIAndActions() {
        binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorLine));
        binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipe));
        binding.get().swipeRefresh.setOnRefreshListener(() -> {});


    }

    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        roomViewModel = ViewModelProviders.of(this, viewModelFactory).get(RoomViewModel.class);
        roomFeatureViewModel = ViewModelProviders.of(this , viewModelFactory).get(RoomFeatureViewModel.class);
    }

    @Override
    protected void initAdapters() {
        RoomDetailAdapter nvAdapter = new RoomDetailAdapter(dataBindingComponent,
                room -> navigationController.navigateToInquiry(getActivity(), "", room.room_id),
                room -> navigationController.navigateToRoomReviewListActivity(getActivity(), room),
                room -> navigationController.navigateToGalleryActivity(getActivity(), room.room_id),
                this::bookRoomClicked
        );
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);


    }


    @Override
    protected void initData() {

        if(getActivity() != null) {
            try {

                roomViewModel.roomId = getActivity().getIntent().getStringExtra("roomId");

            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }
        }

        loadRoom();

        // update touch count
        setTouchCount();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.psLog("On Resume");

        loadLoginUserId();

        // update live data
        roomViewModel.setRoomData(
                loginUserId,
                roomViewModel.roomId
        );

    }
    //endregion


    //region Private Methods

    private void loadRoom() {

        roomViewModel.setRoomData(
                loginUserId,
                roomViewModel.roomId
        );

        roomViewModel.getRoomData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");


                adapter.get().replaceRoom(listResource.data);

                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });

        roomFeatureViewModel.setRoomFeaturesByRoomIdObj(roomViewModel.roomId);
        roomFeatureViewModel.getRoomFeatureByRoomIdData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");

                adapter.get().replaceFeatureList(listResource.data);

                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });

        binding.get().setLoadingMore(false);


    }

    private void setTouchCount() {

        Utils.psLog("Room Id : " + roomViewModel.roomId + "  Login User Id : " + loginUserId);
        if(connectivity.isConnected()) {
            if (loginUserId.equals("")) {
                roomViewModel.doRoomTouch("0", roomViewModel.roomId);
            } else {
                roomViewModel.doRoomTouch(loginUserId, roomViewModel.roomId);
            }
        }

    }

    //region Private Methods
    private void bookRoomClicked(Room room) {

        if(connectivity.isConnected()) {

            if (!roomViewModel.isLoading) {
                if (loginUserId.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__login_first), Toast.LENGTH_SHORT).show();
                    navigationController.navigateToUserLoginActivity(getActivity());
                } else {

                    navigationController.navigateToBooking(getActivity(), room.hotel_id, room.room_id);
                }
            }
        }else {
            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    //endregion

}