package com.panaceasoft.pshotels.ui.booking.entry;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentBookingBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.booking.BookingViewModel;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragment extends PSFragment {

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private BookingViewModel bookingViewModel;
    private UserViewModel userViewModel;

    @VisibleForTesting
    AutoClearedValue<FragmentBookingBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    Calendar myCalendar = Calendar.getInstance();

    private boolean isFirstDateEdit = true;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentBookingBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }


    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);

        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        binding.get().btnSubmit.setOnClickListener(view -> {

            if (connectivity.isConnected()) {

                String userName = binding.get().userNameTextView.getText().toString();
                String userEmail = binding.get().bookingUserEmailTextInput.getText().toString();
                String userPhone = binding.get().bookingUserPhoneTextInput.getText().toString();
                String adultCount = binding.get().bookingAdultCountTextInput.getText().toString();
                String kidCount = binding.get().bookingKidCountTextInput.getText().toString();
                String startDate = binding.get().startDateEditText.getText().toString();
                String endDate = binding.get().endDateEditText.getText().toString();
                String extraBed = binding.get().bookingExtraBedTextInput.getText().toString();
                String remark = binding.get().bookingRemarkEditText.getText().toString();

                if ( userName.equals("")) {
                    Toast.makeText(getContext(), "Please input user name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( userEmail.equals("")) {
                    Toast.makeText(getContext(), "Please input user email address", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( userPhone.equals("")) {
                    Toast.makeText(getContext(), "Please input user phone number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( startDate.equals("")) {
                    Toast.makeText(getContext(), "Please input start date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( endDate.equals("")) {
                    Toast.makeText(getContext(), "Please input end date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( adultCount.equals("")) {
                    adultCount = "0";
                }

                if ( kidCount.equals("")) {
                    kidCount = "0";
                }

                if ( extraBed.equals("")) {
                    extraBed = "0";
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {

                    Date strDate = sdf.parse(startDate);
                    Date enDate = sdf.parse(endDate);

                    if ( strDate.equals(enDate) || strDate.after(enDate)) {

                        Toast.makeText(getContext(), "End Date cannot be equal or before start date", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (ParseException e) {

                    Utils.psLog(e.getMessage());
                }


                if ( !bookingViewModel.isLoading ) {

                    bookingViewModel.userName = userName;
                    bookingViewModel.userEmail = userEmail;
                    bookingViewModel.userPhone = userPhone;
                    bookingViewModel.adultCount = adultCount;
                    bookingViewModel.kidCount = kidCount;
                    bookingViewModel.startDate = startDate;
                    bookingViewModel.endDate = endDate;
                    bookingViewModel.extraBed = extraBed;
                    bookingViewModel.remark = remark;

                    doSubmit();
                }

            } else {
                Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
            }
        });



        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        binding.get().startDateEditText.setOnClickListener( v -> {
            // TODO Auto-generated method stub
            if(getActivity() != null) {
                isFirstDateEdit = true;
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        binding.get().endDateEditText.setOnClickListener( v -> {
            // TODO Auto-generated method stub
            if(getActivity() != null) {
                isFirstDateEdit = false;
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(isFirstDateEdit) {
            binding.get().startDateEditText.setText(sdf.format(myCalendar.getTime()));
        }else {
            binding.get().endDateEditText.setText(sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    protected void initViewModels() {
        bookingViewModel = ViewModelProviders.of(this, viewModelFactory).get(BookingViewModel.class);

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        if (getActivity() != null) {
            try {

                bookingViewModel.hotelId = getActivity().getIntent().getStringExtra("hotelId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }

            try {

                bookingViewModel.roomId = getActivity().getIntent().getStringExtra("roomId");
            } catch (Exception e) {

                Utils.psErrorLog("Error in Getting Intent : room id.", e);
            }
        }

        userViewModel.getUser(loginUserId).observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");

                //fadeIn Animation
                fadeIn(binding.get().getRoot());

                binding.get().userNameTextView.setText(listResource.data.user_name);
                binding.get().bookingUserEmailTextInput.setText(listResource.data.user_email);
                binding.get().bookingUserPhoneTextInput.setText(listResource.data.user_phone);

                Utils.psLog("Photo : " + listResource.data.user_profile_photo);
            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
                //Toast.makeText(getContext(), "Empty Data", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.psLog("On Resume");

        loadLoginUserId();
    }

    private void doSubmit() {

        prgDialog.get().show();

        bookingViewModel.postInquiry(
                Config.API_KEY,
                loginUserId,
                bookingViewModel.userName,
                bookingViewModel.userEmail,
                bookingViewModel.userPhone,
                bookingViewModel.hotelId,
                bookingViewModel.roomId,
                bookingViewModel.adultCount,
                bookingViewModel.kidCount,
                bookingViewModel.startDate,
                bookingViewModel.endDate,
                bookingViewModel.extraBed,
                bookingViewModel.remark
        );

        bookingViewModel.getLoadingStatus().observe(this, submitStatus -> {

            if (submitStatus != null) {

                // error
                String error = submitStatus.getErrorMessageIfNotHandled();

                if (error != null) {

                    Utils.psLog(error);

                    prgDialog.get().cancel();
                } else {

                    if (!submitStatus.isRunning()) {

                        // success
                        binding.get().bookingAdultCountTextInput.setText("");
                        binding.get().bookingKidCountTextInput.setText("");
                        binding.get().startDateEditText.setText("");
                        binding.get().endDateEditText.setText("");
                        binding.get().bookingExtraBedTextInput.setText("");
                        binding.get().bookingRemarkEditText.setText("");

                        Toast.makeText(getContext(), "Booking Request is sent", Toast.LENGTH_SHORT).show();

                        prgDialog.get().cancel();

                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    }
                }
            }

            binding.get().executePendingBindings();
        });
    }
}