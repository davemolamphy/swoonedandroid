package com.panaceasoft.pshotels.ui.review.hotel;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityHotelReviewListBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class HotelReviewListActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityHotelReviewListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_hotel_review_list);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityHotelReviewListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.review_list));

        // setup Fragment
        setupFragment(new HotelReviewListFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}