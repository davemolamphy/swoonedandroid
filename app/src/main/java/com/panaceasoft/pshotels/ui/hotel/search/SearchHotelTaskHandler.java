package com.panaceasoft.pshotels.ui.hotel.search;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;

import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.SearchHotelLoadingHolder;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class SearchHotelTaskHandler extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public SearchHotelTaskHandler(HotelRepository repository) {
        super();

        this.repository = repository;
    }

    public void getNextPage(SearchHotelLoadingHolder nextPageHolder) {

        if(nextPageHolder == null) {
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageSearchHotels(nextPageHolder);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }


}