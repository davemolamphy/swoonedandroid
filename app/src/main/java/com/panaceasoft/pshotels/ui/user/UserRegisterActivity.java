package com.panaceasoft.pshotels.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityUserRegisterBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class UserRegisterActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUserRegisterBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user_register);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityUserRegisterBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.register));

        // setup Fragment
        setupFragment(new UserRegisterFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}