package com.panaceasoft.pshotels.ui.common;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.viewobject.common.Resource;

/**
 * Created by Panacea-Soft on 12/5/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

public class BackgroundTaskHandler implements Observer<Resource<Boolean>> {

    @Nullable
    protected LiveData<Resource<Boolean>> holdLiveData;
    protected final MutableLiveData<LoadingState> loadingState = new MutableLiveData<>();
    protected String limit;
    protected String offset;
    protected PSRepository repository;
    private boolean hasMore;

    protected BackgroundTaskHandler(PSRepository repository) {
        this.repository = repository;
        reset();
    }

    public BackgroundTaskHandler() {
        reset();
    }

    public void save(Object obj) {
        if(obj == null) {
            return;
        }

        unregister();

        holdLiveData = repository.save(obj);
        loadingState.setValue(new LoadingState(true, null));
        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }

    public void delete(Object obj) {
        if(obj == null) {
            return;
        }

        unregister();

        holdLiveData = repository.delete(obj);
        loadingState.setValue(new LoadingState(true, null));
        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }

    @Override
    public void onChanged(@Nullable Resource<Boolean> result) {
        if (result == null) {
            reset();
        } else {
            switch (result.status) {
                case SUCCESS:
                    hasMore = Boolean.TRUE.equals(result.data);
                    unregister();
                    loadingState.setValue(new LoadingState(false, null));
                    break;
                case ERROR:
                    hasMore = true;
                    unregister();
                    loadingState.setValue(new LoadingState(false,
                            result.message));
                    break;
            }
        }
    }

    public void unregister() {
        if (holdLiveData != null) {
            holdLiveData.removeObserver(this);
            holdLiveData = null;
            if (hasMore) {
                limit = null;
                offset = null;
            }
        }
    }

    public void reset() {
        unregister();
        hasMore = true;
        loadingState.setValue(new LoadingState(false, null));
    }

    public MutableLiveData<LoadingState> getLoadingState() {
        return loadingState;
    }

    public static class LoadingState {
        private final boolean running;
        private final String errorMessage;
        private boolean handledError = false;

        public LoadingState(boolean running, String errorMessage) {
            this.running = running;
            this.errorMessage = errorMessage;
        }


        public boolean isRunning() {
            return running;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public String getErrorMessageIfNotHandled() {
            if (handledError) {
                return null;
            }
            handledError = true;
            return errorMessage;
        }
    }
}