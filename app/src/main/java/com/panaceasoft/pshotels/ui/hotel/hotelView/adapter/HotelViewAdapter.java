package com.panaceasoft.pshotels.ui.hotel.hotelView.adapter;

import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.HotelviewHotelSummaryBinding;
import com.panaceasoft.pshotels.databinding.HotelviewRoomItemBinding;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.Room;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/1/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final HotelClickCallback callback;
    private final RoomClickCallback roomClickCallback;
    private final InquiryClickCallback inquiryClickCallback;
    private final ReviewListClickCallback reviewListClickCallback;
    private final FavClickCallback favClickCallback;
    private final HotelImageClickCallback hotelImageClickCallback;
    private final BookRoomClickCallback bookRoomClickCallback;

    private final int HOTEL_SUMMARY_VIEW = 1;
    private final int ROOM_VIEW = HOTEL_SUMMARY_VIEW + 1;

    private List<Room> roomList = new ArrayList<>();
    private Hotel hotel;

    public HotelViewAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                            HotelClickCallback callback,
                            RoomClickCallback roomClickCallback,
                            InquiryClickCallback inquiryClickCallback,
                            FavClickCallback favClickCallback,
                            ReviewListClickCallback reviewListClickCallback,
                            HotelImageClickCallback hotelImageClickCallback,
                            BookRoomClickCallback bookRoomClickCallback
    ) {

        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
        this.roomClickCallback = roomClickCallback;
        this.inquiryClickCallback = inquiryClickCallback;
        this.reviewListClickCallback = reviewListClickCallback;
        this.favClickCallback = favClickCallback;
        this.hotelImageClickCallback = hotelImageClickCallback;
        this.bookRoomClickCallback = bookRoomClickCallback;
    }

    //region Override Methods

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        RecyclerView.ViewHolder holder;

        switch (viewType) {
            case HOTEL_SUMMARY_VIEW:

                HotelviewHotelSummaryBinding binding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotelview_hotel_summary, parent, false,
                                dataBindingComponent);
                binding.viewDetailButton.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && callback != null) {
                        callback.onClick(hotel);
                    }
                });

                binding.inquiryButton.setOnClickListener(v -> {
                    Utils.psLog("click inquiry");
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && inquiryClickCallback != null) {
                        inquiryClickCallback.onClick(hotel);
                    }
                });

                binding.reviewCountTextView.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && reviewListClickCallback != null) {
                        reviewListClickCallback.onClick(hotel);
                    }
                });

                binding.favBg.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && favClickCallback != null) {
                        favClickCallback.onClick(hotel);
                    }
                });

                binding.hotelImageView.setOnClickListener(v -> {
                    Hotel hotel = binding.getHotel();
                    if (hotel != null && hotelImageClickCallback != null) {
                        hotelImageClickCallback.onClick(hotel);
                    }
                });

                holder = new HotelSummaryViewHolder(binding);

                break;
            case ROOM_VIEW:
                HotelviewRoomItemBinding binding2 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotelview_room_item, parent, false,
                                dataBindingComponent);
                binding2.getRoot().setOnClickListener(v -> {
                    Room room = binding2.getRoom();
                    if (room != null && roomClickCallback != null) {
                        roomClickCallback.onClick(room);
                        Utils.psLog("Clicked Room " + room.room_name);
                    }
                });

                binding2.bookingButton.setOnClickListener(v -> {
                    Room room = binding2.getRoom();
                    if (room != null && bookRoomClickCallback != null) {

                        bookRoomClickCallback.onClick(room);
                        Utils.psLog("Clicked bookRoomClickCallback " + room.room_name);
                    }
                });


                holder = new RoomViewHolder(binding2);

                break;

            default:
                view = inflater.inflate(R.layout.hotelview_room_item, parent, false);
                view.setVisibility(View.GONE);
                HotelviewHotelSummaryBinding binding3 = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()),
                                R.layout.hotelview_hotel_summary, parent, false,
                                dataBindingComponent);
                binding3.getRoot().setOnClickListener(v -> {
                    /*Hotel hotel = binding.getHotel();
                    if (hotel != null && callback != null) {
                        callback.onClick(hotel);
                    }*/
                });

                holder = new HotelSummaryViewHolder(binding3);

                break;

        }

        return holder;
    }

    @Override
    public int getItemCount() {
        return roomList.size() + 1; // 1 is for hotel summary view
    }

    @Override
    public int getItemViewType(int position) {

        switch (position) {
            case 0:
                return HOTEL_SUMMARY_VIEW;
            case 1:
                return ROOM_VIEW;
            default:
                return ROOM_VIEW;

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case HOTEL_SUMMARY_VIEW:
                if(holder instanceof HotelSummaryViewHolder) {
                    setupHotelSummaryView((HotelSummaryViewHolder) holder, position);
                }
                break;
            case ROOM_VIEW:
                if(holder instanceof RoomViewHolder) {
                    setupRoomView((RoomViewHolder) holder, position);
                }
                break;
            default:
                // nothing
                break;

        }

    }

    private void setupHotelSummaryView(HotelSummaryViewHolder holder, int position) {

        try {
            if (this.hotel != null) {

                holder.binding.setHotel(this.hotel);

                if (this.hotel.hotel_star_rating != null) {

                    int rating = Integer.parseInt(this.hotel.hotel_star_rating);

                    if (rating < 5) holder.binding.star5ImageView.setVisibility(View.GONE);

                    if (rating < 4) holder.binding.star4ImageView.setVisibility(View.GONE);

                    if (rating < 3) holder.binding.star3ImageView.setVisibility(View.GONE);

                    if (rating < 2) holder.binding.star2ImageView.setVisibility(View.GONE);

                    if (rating < 1) holder.binding.star1ImageView.setVisibility(View.GONE);

                } else {

                    holder.binding.star1ImageView.setVisibility(View.GONE);
                    holder.binding.star2ImageView.setVisibility(View.GONE);
                    holder.binding.star3ImageView.setVisibility(View.GONE);
                    holder.binding.star4ImageView.setVisibility(View.GONE);
                    holder.binding.star5ImageView.setVisibility(View.GONE);
                }
            }
        }catch (Exception e) {
            Utils.psErrorLog("setupHotelSummaryView", e);
        }
    }


    private void setupRoomView(RoomViewHolder holder, int position) {

        try {
            if (this.roomList != null && this.roomList.size() > 0) {

                Room room = this.roomList.get(position - 1);
                holder.binding.setRoom(room);

                if (room.promotion != null && !room.promotion.promo_percent.equals("")) {

                    if (Integer.parseInt(room.promotion.promo_percent) > 0) {

                        holder.binding.promotionLayout.setVisibility(View.VISIBLE);
                    } else {

                        holder.binding.promotionLayout.setVisibility(View.GONE);
                    }
                } else {

                    holder.binding.promotionLayout.setVisibility(View.GONE);
                }


                if (room.promotion != null && !room.promotion.promo_percent.equals("")) {
                    //room got discount
                    holder.binding.discountPriceTextView.setVisibility(View.VISIBLE);
                    holder.binding.priceTextview.setPaintFlags(holder.binding.priceTextview.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discountPrice = (Float.parseFloat(room.promotion.promo_percent) / 100) * Float.parseFloat(room.room_price);

                    String discountPriceStr = room.currency_symbol + (int) discountPrice;
                    holder.binding.discountPriceTextView.setText(discountPriceStr);
                    String priceStr = room.currency_symbol + room.room_price;
                    holder.binding.priceTextview.setText(priceStr);
                    holder.binding.priceTextview.setTextColor(holder.binding.getRoot().getResources().getColor(R.color.colorText));

                } else {
                    holder.binding.discountPriceTextView.setVisibility(View.GONE);
                    holder.binding.priceTextview.setTextColor(holder.binding.getRoot().getResources().getColor(R.color.colorPromotion));
                    String priceStr = room.currency_symbol + room.room_price;
                    holder.binding.priceTextview.setText(priceStr);
                    holder.binding.priceTextview.setPaintFlags(holder.binding.priceTextview.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                }

                if (room.is_available.equals("0")) {
                    holder.binding.bookingButton.setText(holder.binding.getRoot().getResources().getString(R.string.booking_not_available));
                    holder.binding.bookingButton.setEnabled(false);
                } else {
                    holder.binding.bookingButton.setText(holder.binding.getRoot().getResources().getString(R.string.book_this_room));
                    holder.binding.bookingButton.setEnabled(true);
                }

            }
        }catch (Exception e) {
            Utils.psErrorLog("setupRoomView", e);
        }
    }


    public class HotelSummaryViewHolder extends RecyclerView.ViewHolder {
        HotelviewHotelSummaryBinding binding;

        HotelSummaryViewHolder(HotelviewHotelSummaryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {
        HotelviewRoomItemBinding binding;

        RoomViewHolder(HotelviewRoomItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface HotelClickCallback {
        void onClick(Hotel hotel);
    }

    public interface RoomClickCallback {
        void onClick(Room room);
    }

    public interface InquiryClickCallback {
        void onClick(Hotel hotel);
    }

    public interface ReviewListClickCallback {
        void onClick(Hotel hotel);
    }

    public interface FavClickCallback {
        void onClick(Hotel hotel);
    }

    public interface HotelImageClickCallback {
        void onClick(Hotel hotel);
    }

    public interface BookRoomClickCallback {
        void onClick(Room room);
    }

    public void replaceHotel(Hotel hotel) {
        this.hotel = hotel;

        notifyDataSetChanged();
    }

    public void replaceRoomList(List<Room> roomList) {
        this.roomList = roomList;
        notifyDataSetChanged();
    }
}