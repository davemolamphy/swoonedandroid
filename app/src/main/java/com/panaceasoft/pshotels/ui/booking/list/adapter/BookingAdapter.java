package com.panaceasoft.pshotels.ui.booking.list.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.panaceasoft.pshotels.R;

import com.panaceasoft.pshotels.databinding.BookingItemBinding;
import com.panaceasoft.pshotels.ui.common.DataBoundListAdapter;
import com.panaceasoft.pshotels.ui.common.DataBoundViewHolder;
import com.panaceasoft.pshotels.utils.Objects;
import com.panaceasoft.pshotels.viewobject.Booking;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class BookingAdapter extends DataBoundListAdapter<Booking, BookingItemBinding> {

    private final android.databinding.DataBindingComponent dataBindingComponent;
    private final BookingClickCallback callback;
    private int lastPosition = -1;


    public BookingAdapter(android.databinding.DataBindingComponent dataBindingComponent,
                            BookingClickCallback callback) {

        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
    }

    @Override
    protected BookingItemBinding createBinding(ViewGroup parent) {
        BookingItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.booking_item, parent, false,
                        dataBindingComponent);

        binding.getRoot().setOnClickListener(v -> {
            Booking booking = binding.getBooking();
            if (booking != null && callback != null) {
                callback.onClick(booking);
            }
        });

        return binding;
    }

    // For general animation
    @Override
    public void bindView(DataBoundViewHolder<BookingItemBinding> holder, int position) {
        super.bindView(holder, position);

        setAnimation(holder.itemView, position);
    }

    @Override
    protected void bind(BookingItemBinding binding, Booking booking) {
        binding.setBooking(booking);

        if(booking.booking_status.equals(binding.getRoot().getContext().getString(R.string.booking__cancelled))) {
            binding.statusTextView.setTextColor(binding.getRoot().getContext().getResources().getColor(R.color.colorCancel));
        }else if(booking.booking_status.equals(binding.getRoot().getContext().getString(R.string.booking__progress))) {
            binding.statusTextView.setTextColor(binding.getRoot().getContext().getResources().getColor(R.color.colorText));
        }else if(booking.booking_status.equals(binding.getRoot().getContext().getString(R.string.booking__comfirmed))) {
            binding.statusTextView.setTextColor(binding.getRoot().getContext().getResources().getColor(R.color.colorConfirm));
        }


        if(!booking.booking_start_date.equals("")) {

            String[] startDateArr = booking.booking_start_date.split(" ");

            if(startDateArr.length > 0) {
                String[] startDate = startDateArr[0].split("-");

                if(startDate.length > 0) {
                    binding.startYearTextView.setText(startDate[0]);
                }

                if(startDate.length > 1) {
                    binding.startMonthTextView.setText(startDate[1]);
                }

                if(startDate.length > 2) {
                    binding.startDayTextView.setText(startDate[2]);
                }
            }

        }else {
            binding.startDayTextView.setVisibility(View.GONE);
            binding.startMonthTextView.setVisibility(View.GONE);
            binding.startYearTextView.setVisibility(View.GONE);
        }


        if(!booking.booking_end_date.equals("")) {

            String[] endDateArr = booking.booking_end_date.split(" ");

            if(endDateArr.length > 0) {
                String[] endDate = endDateArr[0].split("-");

                if(endDate.length > 0) {
                    binding.endYearTextView.setText(endDate[0]);
                }

                if(endDate.length > 1) {
                    binding.endMonthTextView.setText(endDate[1]);
                }

                if(endDate.length > 2) {
                    binding.endDayTextView.setText(endDate[2]);
                }
            }

        }else {
            binding.endDayTextView.setVisibility(View.GONE);
            binding.endMonthTextView.setVisibility(View.GONE);
            binding.endYearTextView.setVisibility(View.GONE);
        }
    }

    @Override
    protected boolean areItemsTheSame(Booking oldItem, Booking newItem) {
        return Objects.equals(oldItem.hotel_id, newItem.hotel_id) && Objects.equals(oldItem.booking_status, newItem.booking_status);
    }

    @Override
    protected boolean areContentsTheSame(Booking oldItem, Booking newItem) {
        return Objects.equals(oldItem.hotel_id, newItem.hotel_id) && Objects.equals(oldItem.booking_status, newItem.booking_status);
    }

    public interface BookingClickCallback {
        void onClick(Booking booking);
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else {
            lastPosition = position;
        }
    }

}
