package com.panaceasoft.pshotels.ui.booking.list;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityBookingListBinding;
import com.panaceasoft.pshotels.ui.booking.entry.BookingFragment;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class BookingListActivity extends PSAppCompactActivity {

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityBookingListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_booking_list);


        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityBookingListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.booking__hotel));

        // setup Fragment
        setupFragment(new BookingListFragment());
    }

    //endregion

}

