package com.panaceasoft.pshotels.ui.hotel.search.filter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentFilterBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.RangeSeekBar;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.price.PriceViewModel;

/**
 * Created by Panacea-Soft on 3/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

public class FilterFragment extends PSFragment {

    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private PriceViewModel priceViewModel;

    private String userSelectedStars = "";
    private int userSelectedRating = 0;
    private int userSelectedPriceMin = 0;
    private int userSelectedPriceMax = 0;
    private int defaultPrice = 1000;
    private int maxPriceFromServer = 0;
    private String currencySymbol = "";

    private boolean isOnePressed = false;
    private boolean isTwoPressed = false;
    private boolean isThreePressed = false;
    private boolean isFourPressed = false;
    private boolean isFivePressed = false;

    private String selectedStars = "";
    private String selectedOne = "";
    private String selectedTwo = "";
    private String selectedThree = "";
    private String selectedFour = "";
    private String selectedFive = "";

    private Boolean oneStarsSelected = false;
    private Boolean twoStarsSelected = false;
    private Boolean threeStarsSelected = false;
    private Boolean fourStarsSelected = false;
    private Boolean fiveStarsSelected = false;


    @VisibleForTesting
    AutoClearedValue<FragmentFilterBinding> binding;

    AutoClearedValue<ProgressDialog> prgDialog;

    AutoClearedValue<RangeSeekBar> rangeSeekBarAutoClearedValue;


    //endregion


    //region Override Methods

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentFilterBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        prgDialog.get().setMessage(getString(R.string.message__please_wait));
        prgDialog.get().setCancelable(false);


        //fadeIn Animation
        fadeIn(binding.get().getRoot());

        initPriceRangeBar(defaultPrice);

        binding.get().ratingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.get().txtRating.setText(getGuestRatingText(i));
                userSelectedRating = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.get().oneStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("One");

            if (!isOnePressed) {
                clickStars("one");
            } else {
                resetStars("one");
            }

        });

        binding.get().twoStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Two");

            if (!isTwoPressed) {
                clickStars("two");
            } else {
                resetStars("two");
            }

        });

        binding.get().threeStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Three");

            if (!isThreePressed) {
                clickStars("three");
            } else {
                resetStars("three");
            }

        });

        binding.get().fourStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Four");

            if (!isFourPressed) {
                clickStars("four");
            } else {
                resetStars("four");
            }

        });

        binding.get().fiveStar.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Utils.psLog("Five");

            if (!isFivePressed) {
                clickStars("five");
            } else {
                resetStars("five");
            }

        });

        binding.get().okButton.setOnClickListener(view -> {
            Utils.psLog("Click Ok Button");

            selectedStars = selectedOne + "," + selectedTwo + "," + selectedThree + "," + selectedFour + "," + selectedFive;

            String[] split = selectedStars.split(",");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < split.length; i++) {

                if (!split[i].isEmpty()) {

                    sb.append(split[i]);

                    if (i != split.length - 1) {
                        sb.append("-");
                    }
                }

            }

            userSelectedStars = sb.toString();

            Utils.psLog("Selected  Stars : " + userSelectedStars);

            Utils.psLog("Min Price : " + userSelectedPriceMin + "| Max Price : " + userSelectedPriceMax);

            Utils.psLog("Rating : " + userSelectedRating);

            passBackData(userSelectedStars, userSelectedPriceMin, userSelectedPriceMax, userSelectedRating);

        });

        binding.get().clearButton.setOnClickListener(view -> {
            Utils.psLog("Click Clear Button");

            //Reset Price Range
            if (rangeSeekBarAutoClearedValue.get() != null) {

                if (maxPriceFromServer == 0) {
                    rangeSeekBarAutoClearedValue.get().setSelectedMaxValue(defaultPrice);
                    userSelectedPriceMax = defaultPrice;
                } else {
                    rangeSeekBarAutoClearedValue.get().setSelectedMaxValue(maxPriceFromServer);
                    userSelectedPriceMax = maxPriceFromServer;
                }
                rangeSeekBarAutoClearedValue.get().setSelectedMinValue(0);
            }
            userSelectedPriceMin = 0;

            binding.get().txtPriceRange.setText(getPriceRangeText());


            //Reset User Rating
            binding.get().ratingSeekBar.setProgress(0);
            binding.get().txtRating.setText(getString(R.string.select_rating));

            //Reset Stars
            resetStars("one");
            resetStars("two");
            resetStars("three");
            resetStars("four");
            resetStars("five");


        });

        binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
        binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);


    }

    @Override
    protected void initViewModels() {
        priceViewModel = ViewModelProviders.of(this, viewModelFactory).get(PriceViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {
        if (getActivity() != null) {
            try {

                userSelectedPriceMin = getActivity().getIntent().getIntExtra("selectedPriceMin", 0);
                userSelectedPriceMax = getActivity().getIntent().getIntExtra("selectedPriceMax", 0);
                userSelectedRating = getActivity().getIntent().getIntExtra("selectedRatings", 0);
                userSelectedStars = getActivity().getIntent().getStringExtra("selectedHotelStars");

                String[] split = userSelectedStars.split("-");


                for (String aSplit : split) {

                    if (!aSplit.isEmpty()) {

                        switch (aSplit) {
                            case "1":
                                oneStarsSelected = true;
                                break;
                            case "2":
                                twoStarsSelected = true;
                                break;
                            case "3":
                                threeStarsSelected = true;
                                break;
                            case "4":
                                fourStarsSelected = true;
                                break;
                            case "5":
                                fiveStarsSelected = true;
                                break;
                        }

                    }

                }

            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : userSelectedStars.", e);

            }


        }

        priceViewModel.setPriceObj("max price");
        priceViewModel.getPriceData().observe(this, resource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (resource != null) {

                if (resource.data != null) {
                    Utils.psLog("Max Price : " + resource.data.max_price);


                    if (rangeSeekBarAutoClearedValue.get() != null) {

                        maxPriceFromServer = Integer.parseInt(resource.data.max_price);
                        currencySymbol = resource.data.currency_symbol;
                        initPriceRangeBar(maxPriceFromServer);

                    }

                }

            } else {
                //noinspection ConstantConditions
                Utils.psLog("No Data of About Us.");
            }
        });


        // Guest Rating Data Set
        binding.get().ratingSeekBar.setProgress(userSelectedRating);

        if (userSelectedRating != 0) {
            binding.get().txtRating.setText(getGuestRatingText(userSelectedRating));
        }


        // Update Hotel Stars
        if (oneStarsSelected) {
            clickStars("one");
        }

        if (twoStarsSelected) {
            clickStars("two");
        }

        if (threeStarsSelected) {
            clickStars("three");
        }

        if (fourStarsSelected) {
            clickStars("four");
        }

        if (fiveStarsSelected) {
            clickStars("five");
        }
    }


    //endregion

    //region private functions

    private void initPriceRangeBar(int maxValue) {

        binding.get().priceRangeBarContainer.removeAllViews();

        RangeSeekBar<Integer> seekBar = new RangeSeekBar<>(0, maxValue, this.getContext());

        seekBar.setSelectedMinValue(userSelectedPriceMin);

        if (userSelectedPriceMax > 0) {
            seekBar.setSelectedMaxValue(userSelectedPriceMax);
        } else {
            seekBar.setSelectedMaxValue(maxValue);
        }

        binding.get().txtPriceRange.setText(getPriceRangeText());

        seekBar.setOnRangeSeekBarChangeListener((bar, minValue, maxValue1) -> {

            binding.get().txtPriceRange.setText(getPriceRangeText());

            userSelectedPriceMin = minValue;
            userSelectedPriceMax = maxValue1;
        });

        rangeSeekBarAutoClearedValue = new AutoClearedValue<>(this, seekBar);

        binding.get().priceRangeBarContainer.addView(seekBar);
    }

    private String getPriceRangeText() {

        if (userSelectedPriceMax > 0) {
            return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (userSelectedPriceMax) + " " + getString(R.string.select_price_range);
        } else {

            if (maxPriceFromServer == 0) {
                return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (defaultPrice) + " " + getString(R.string.select_price_range);
            } else {
                return currencySymbol + (userSelectedPriceMin) + " - " + currencySymbol + (maxPriceFromServer) + " " + getString(R.string.select_price_range);
            }
        }
    }

    private String getGuestRatingText(int rating) {
        return getString(R.string.filter__guest_rating)+ " " + rating + " " + getString(R.string.filter__higher);
    }

    private void resetStars(String stars) {

        switch (stars) {
            case "one":

                binding.get().oneStar.setBackgroundResource(R.drawable.button_border);
                binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isOnePressed = false;
                selectedOne = "";

                break;
            case "two":

                binding.get().twoStar.setBackgroundResource(R.drawable.button_border);
                binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isTwoPressed = false;
                selectedTwo = "";

                break;
            case "three":

                binding.get().threeStar.setBackgroundResource(R.drawable.button_border);
                binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isThreePressed = false;
                selectedThree = "";

                break;
            case "four":

                binding.get().fourStar.setBackgroundResource(R.drawable.button_border);
                binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isFourPressed = false;
                selectedFour = "";

                break;
            case "five":

                binding.get().fiveStar.setBackgroundResource(R.drawable.button_border);
                binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_full_gray, 0);
                isFivePressed = false;
                selectedFive = "";

                break;
        }

    }

    private void clickStars(String stars) {
        switch (stars) {
            case "one":

                binding.get().oneStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().oneStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isOnePressed = true;
                selectedOne = "1";

                break;
            case "two":

                binding.get().twoStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().twoStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isTwoPressed = true;
                selectedTwo = "2";

                break;
            case "three":

                binding.get().threeStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().threeStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isThreePressed = true;
                selectedThree = "3";

                break;
            case "four":

                binding.get().fourStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().fourStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isFourPressed = true;
                selectedFour = "4";

                break;
            case "five":

                binding.get().fiveStar.setBackgroundResource(R.drawable.button_border_pressed);
                binding.get().fiveStar.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_white, 0);
                isFivePressed = true;
                selectedFive = "5";

                break;
        }
    }

    private void passBackData(String hotelStars, Integer priceMin, Integer priceMax, Integer guestRatings) {
        Intent intent = new Intent();
        intent.putExtra("hotelStars", hotelStars);
        intent.putExtra("priceMin", priceMin);
        intent.putExtra("priceMax", priceMax);
        intent.putExtra("guestRatings", guestRatings);

        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    //endregion


}

