package com.panaceasoft.pshotels.ui.detail.hotel;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.binding.FragmentDataBindingComponent;
import com.panaceasoft.pshotels.databinding.FragmentHotelDetailBinding;
import com.panaceasoft.pshotels.ui.common.PSFragment;
import com.panaceasoft.pshotels.ui.detail.adapter.HotelDetailAdapter;
import com.panaceasoft.pshotels.utils.AutoClearedValue;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.hotel.HotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotelInfo.HotelFeatureViewModel;
import com.panaceasoft.pshotels.viewobject.Hotel;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelDetailFragment extends PSFragment {


    //region Variables

    private final android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private HotelViewModel hotelViewModel;


    @VisibleForTesting
    AutoClearedValue<FragmentHotelDetailBinding> binding;
    AutoClearedValue<HotelDetailAdapter> adapter;
    private HotelFeatureViewModel hotelFeatureViewModel;

    //endregion


    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentHotelDetailBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_hotel_detail , container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        if(Config.SHOW_ADMOB && connectivity.isConnected()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            binding.get().adView.loadAd(adRequest);
        }else {
            binding.get().adView.setVisibility(View.GONE);
        }


        return binding.get().getRoot();

    }

    @Override
    protected void initUIAndActions() {
    }


    @Override
    protected void initViewModels() {
        // ViewModel need to get from ViewModelProviders
        hotelViewModel = ViewModelProviders.of(this, viewModelFactory).get(HotelViewModel.class);
        hotelFeatureViewModel = ViewModelProviders.of(this , viewModelFactory).get(HotelFeatureViewModel.class);
    }

    @Override
    protected void initAdapters() {
        HotelDetailAdapter nvAdapter = new HotelDetailAdapter(dataBindingComponent,
                hotel -> favClicked(hotel),
                hotel -> navigationController.navigateToHotelReviewListActivity(getActivity(), hotel),
                hotel -> navigationController.navigateToGalleryActivity(getActivity(), hotel.hotel_id),
                hotel -> shareClick(),
                hotel -> navigationController.navigateToInquiry(getActivity(),hotel.hotel_id, ""),
                hotel-> mapClick(hotel));
        this.adapter = new AutoClearedValue<>(this, nvAdapter);
        binding.get().recList.setAdapter(nvAdapter);
    }

    @Override
    protected void initData() {

        if(getActivity() != null) {
            try {

                hotelViewModel.hotelId = getActivity().getIntent().getStringExtra("hotel_id");
            } catch (Exception e) {
                Utils.psErrorLog("Error in Getting Intent : category id.", e);
            }

        }

        loadDetailHotelData();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.psLog("On Resume");

        loadLoginUserId();

        // update live data
        hotelViewModel.setHotelData(
                loginUserId,
                hotelViewModel.hotelId
        );

    }


    //endregion


    //region Private Methods

    private void favClicked(Hotel hotel) {
        if(connectivity.isConnected()) {

            if (!hotelViewModel.isLoading) {
                if (loginUserId.equals("")) {
                    Toast.makeText(getContext(), getString(R.string.error_message__login_first), Toast.LENGTH_SHORT).show();
                    navigationController.navigateToUserLoginActivity(getActivity());
                } else {
                    hotelViewModel.doFavourite(loginUserId, hotel.hotel_id);
                }
            }
        }else {
            Toast.makeText(getContext(), R.string.error_message__no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    private void loadDetailHotelData() {

        hotelViewModel.setHotelData(
                loginUserId,
                hotelViewModel.hotelId
        );

        hotelViewModel.getHotelData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");


                adapter.get().replaceHotel(listResource.data);
                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });

        hotelFeatureViewModel.setHotelFeaturesByHotelIdObj(hotelViewModel.hotelId);
        hotelFeatureViewModel.getHotelFeatureByHotelIdData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                Utils.psLog("Got Data");


                adapter.get().replaceFeatureList(listResource.data);

                binding.get().executePendingBindings();

            } else {
                //noinspection ConstantConditions
                Utils.psLog("Empty Data");
            }
        });

    }


    private void shareClick() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));

        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.message__share_text));

        startActivity(Intent.createChooser(share, "Share About The App!"));
    }

    private void mapClick(Hotel hotel) {
        try {
            String uri = "http://maps.google.com/maps?daddr=" + hotel.hotel_lat + "," + hotel.hotel_lng;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
        }catch (Exception e) {
            Toast.makeText(getContext(), getString(R.string.error_message__error_map), Toast.LENGTH_SHORT).show();
        }
    }

    //endregion


}