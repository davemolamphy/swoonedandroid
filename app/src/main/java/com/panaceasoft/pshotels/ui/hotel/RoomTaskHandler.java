package com.panaceasoft.pshotels.ui.hotel;

import android.support.annotation.NonNull;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.repository.room.RoomRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.Utils;

/**
 * Created by Panacea-Soft on 4/29/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class RoomTaskHandler extends BackgroundTaskHandler {

    private final RoomRepository repository;

    public RoomTaskHandler(RoomRepository repository) {
        super();

        this.repository = repository;
    }


    public void doRoomTouch(@NonNull String loginUserId, @NonNull String roomId) {

        unregister();

        holdLiveData =repository.doPostTouchCount(loginUserId, roomId);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }


}