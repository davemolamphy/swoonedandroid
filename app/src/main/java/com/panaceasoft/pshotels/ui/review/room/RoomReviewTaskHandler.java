package com.panaceasoft.pshotels.ui.review.room;

import com.panaceasoft.pshotels.repository.review.ReviewRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;
import com.panaceasoft.pshotels.viewobject.holder.SyncUserRatingHolder;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class RoomReviewTaskHandler extends BackgroundTaskHandler {

    private final ReviewRepository repository;

    public RoomReviewTaskHandler(ReviewRepository repository) {
        super();

        this.repository = repository;
    }

    public void getNextPage(ReviewDetailLoadingHolder nextPageHolder) {

        if(nextPageHolder == null) {
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageRoomReviews(nextPageHolder);
        loadingState.setValue(new BackgroundTaskHandler.LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }

    public void postReview(String apiKey, String roomId, String userId, String reviewDesc, SyncUserRatingHolder syncUserRatingHolder) {

        unregister();

        holdLiveData = repository.postReview(apiKey, roomId, Utils.checkUserId( userId ), reviewDesc, syncUserRatingHolder);
        loadingState.setValue(new LoadingState(true, null));

        //noinspection ConstantConditions
        holdLiveData.observeForever(this);
    }

}