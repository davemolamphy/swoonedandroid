package com.panaceasoft.pshotels.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.databinding.ActivityProfileEditBinding;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;

public class ProfileEditActivity extends PSAppCompactActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityProfileEditBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_edit);

        // Init all UI
        initUI(binding);

    }

    //endregion


    //region Private Methods

    private void initUI(ActivityProfileEditBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.edit_profile));

        // setup Fragment
        setupFragment(new ProfileEditFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}