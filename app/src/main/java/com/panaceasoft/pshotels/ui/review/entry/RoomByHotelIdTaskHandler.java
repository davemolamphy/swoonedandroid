package com.panaceasoft.pshotels.ui.review.entry;

import com.panaceasoft.pshotels.repository.room.RoomRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;

public class RoomByHotelIdTaskHandler extends BackgroundTaskHandler {

    private final RoomRepository repository;

    public RoomByHotelIdTaskHandler(RoomRepository repository) {
        super();
        this.repository = repository;
    }

    public void getNextPage(RoomByHotelIdLoadingHolder nextPageHolder) {

        if(nextPageHolder == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPageRoomByHotelIds(nextPageHolder);
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}
