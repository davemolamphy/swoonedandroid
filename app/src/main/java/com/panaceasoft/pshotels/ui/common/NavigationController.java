package com.panaceasoft.pshotels.ui.common;

import android.app.Activity;
import android.content.Intent;
import com.panaceasoft.pshotels.MainActivity;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.ui.aboutus.AboutUsFragment;
import com.panaceasoft.pshotels.ui.booking.detail.BookingDetailActivity;
import com.panaceasoft.pshotels.ui.booking.entry.BookingActivity;
import com.panaceasoft.pshotels.ui.booking.list.BookingListFragment;
import com.panaceasoft.pshotels.ui.contactus.ContactUsFragment;
import com.panaceasoft.pshotels.ui.detail.hotel.HotelDetailActivity;
import com.panaceasoft.pshotels.ui.detail.room.RoomDetailActivity;
import com.panaceasoft.pshotels.ui.gallery.GalleryActivity;
import com.panaceasoft.pshotels.ui.gallery.GalleryDetailActivity;
import com.panaceasoft.pshotels.ui.hotel.favourite.FavouriteHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.popular.PopularHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.promotion.PromotionHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.recommended.RecommendedHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.hotelView.HotelViewActivity;
import com.panaceasoft.pshotels.ui.hotel.search.destinationfilter.CityListActivity;
import com.panaceasoft.pshotels.ui.hotel.search.filter.FilterActivity;
import com.panaceasoft.pshotels.ui.hotel.search.filter.HotelFilterActivity;
import com.panaceasoft.pshotels.ui.inquiry.InquiryActivity;
import com.panaceasoft.pshotels.ui.notification.NotificationSettingFragment;
import com.panaceasoft.pshotels.ui.review.entry.RoomListFilterActivity;
import com.panaceasoft.pshotels.ui.review.entry.SubmitReviewActivity;
import com.panaceasoft.pshotels.ui.review.hotel.HotelReviewListActivity;
import com.panaceasoft.pshotels.ui.review.room.RoomReviewListActivity;
import com.panaceasoft.pshotels.ui.user.PasswordChangeActivity;
import com.panaceasoft.pshotels.ui.user.UserLoginActivity;
import com.panaceasoft.pshotels.ui.hotel.search.SearchFragment;
import com.panaceasoft.pshotels.ui.hotel.search.SearchResultListActivity;
import com.panaceasoft.pshotels.ui.user.ProfileEditActivity;
import com.panaceasoft.pshotels.ui.user.ProfileFragment;
import com.panaceasoft.pshotels.ui.user.UserForgotPasswordActivity;
import com.panaceasoft.pshotels.ui.user.UserForgotPasswordFragment;
import com.panaceasoft.pshotels.ui.user.UserLoginFragment;
import com.panaceasoft.pshotels.ui.user.UserRegisterActivity;
import com.panaceasoft.pshotels.ui.user.UserRegisterFragment;
import com.panaceasoft.pshotels.ui.welcome.WelcomeActivity;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.Room;
import javax.inject.Inject;


/**
 * Created by Panacea-Soft on 11/17/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

public class NavigationController {

    //region Variables

    private final int containerId;
    private RegFragments currentFragment;

    //endregion


    //region Constructor
    @Inject
    public NavigationController() {

        // This setup is for MainActivity
        this.containerId = R.id.content_frame;
    }

    //endregion


    //region Navigation Methods


    // PS Hotels


    public void navigateToBookingDetailViewActivity(Activity activity, Booking booking) {

        Intent intent = new Intent(activity, BookingDetailActivity.class);

        if (booking != null && (!booking.booking_id.equals(""))) {

            intent.putExtra("BOOKING_ID", booking.booking_id);

        } else {

            intent.putExtra("BOOKING_ID", "");

        }

        activity.startActivity(intent);
    }


    public void navigateToHotelViewActivity(Activity activity, String hotelId, String hotelName ) {

        Intent intent = new Intent(activity, HotelViewActivity.class);

        if (hotelId != null && (!hotelId.equals(""))) {

            intent.putExtra("hotel_id", hotelId);
            intent.putExtra("hotel_name", hotelName);

        } else {

            intent.putExtra("hotel_id", "");
            intent.putExtra("hotel_name", "");
        }

        activity.startActivity(intent);
    }

    public void navigateToHotelDetailActivity(Activity activity, Hotel hotel) {
        Intent intent = new Intent(activity, HotelDetailActivity.class);

        Utils.psLog("Hotel : " + hotel.hotel_name);

        intent.putExtra("hotel_id", hotel.hotel_id);
        intent.putExtra("hotel_name", hotel.hotel_name);

        activity.startActivity(intent);

    }

    public void navigateToHotelReviewListActivity(Activity activity, Hotel hotel) {
        Intent intent = new Intent(activity, HotelReviewListActivity.class);

        Utils.psLog("Hotel : " + hotel.hotel_name);

        intent.putExtra("hotel_id", hotel.hotel_id);

        activity.startActivity(intent);
    }

    public void navigateToRoomDetailActivity(Activity activity, String roomId, String roomName) {

        Intent intent = new Intent(activity, RoomDetailActivity.class);

        if (roomId != null && (!roomId.equals(""))) {

            intent.putExtra("roomId", roomId);
            intent.putExtra("roomName", roomName);

        } else {

            intent.putExtra("roomId", "");
            intent.putExtra("roomName", "");
        }

        activity.startActivity(intent);
    }

    public void navigateToRoomReviewListActivity(Activity activity, Room room) {

        Intent intent = new Intent(activity, RoomReviewListActivity.class);

        if (room != null && (!room.room_id.equals(""))) {

            intent.putExtra("roomId", room.room_id);
        } else {

            intent.putExtra("roomId", "");
        }

        activity.startActivity(intent);
    }

    // PS News



    public void navigateToRecommendedHotel(MainActivity mainActivity) {

        if (checkFragmentChange(RegFragments.HOME_RECOMMENDED_HOTEL_LIST)) {

            try {

                RecommendedHotelListFragment fragment = new RecommendedHotelListFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();

            } catch (Exception e) {

                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToPopularHotel(MainActivity mainActivity) {

        if (checkFragmentChange(RegFragments.HOME_POPULAR_HOTEL_LIST)) {

            try {

                PopularHotelListFragment fragment = new PopularHotelListFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();

            } catch (Exception e) {

                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToPromotionHotel(MainActivity mainActivity) {

        if (checkFragmentChange(RegFragments.HOME_PROMOTION_HOTEL_LIST)) {

            try {

                PromotionHotelListFragment fragment = new PromotionHotelListFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();

            } catch (Exception e) {

                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToFavouriteHotel(MainActivity mainActivity) {

        if (checkFragmentChange(RegFragments.HOME_FAVOURITE_HOTEL_LIST)) {

            try {

                FavouriteHotelListFragment fragment = new FavouriteHotelListFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();

            } catch (Exception e) {

                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToBookingList(MainActivity mainActivity) {

        if (checkFragmentChange(RegFragments.HOME_BOOKING_LIST)) {

            try {

                BookingListFragment fragment = new BookingListFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();

            } catch (Exception e) {

                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToUserLogin(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_LOGIN)) {
            try {
                UserLoginFragment fragment = new UserLoginFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToUserProfile(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_LOGIN)) {
            try {
                ProfileFragment fragment = new ProfileFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToUserRegister(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_REGISTER)) {
            try {
                UserRegisterFragment fragment = new UserRegisterFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToUserForgotPassword(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_FOGOT_PASSWORD)) {
            try {
                UserForgotPasswordFragment fragment = new UserForgotPasswordFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToFindHotels(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_SEARCH)) {
            try {
                SearchFragment fragment = new SearchFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToNotificationSetting(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_NOTI_SETTING)) {
            try {
                NotificationSettingFragment fragment = new NotificationSettingFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToAboutUs(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_ABOUTUS)) {
            try {
                AboutUsFragment fragment = new AboutUsFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToGalleryActivity(Activity activity, String imgParentId) {
        Intent intent = new Intent(activity, GalleryActivity.class);

        if (!imgParentId.equals("")) {
            intent.putExtra("imgParentId", imgParentId);
        }

        activity.startActivity(intent);

    }

    public void navigateToDetailGalleryActivity(Activity activity, String newsId, String imgId) {
        Intent intent = new Intent(activity, GalleryDetailActivity.class);

        if (!newsId.equals("")) {
            intent.putExtra("news_id", newsId);
        }

        if (!imgId.equals("")) {
            intent.putExtra("img_id", imgId);
        }

        activity.startActivity(intent);

    }



    public void navigateToSearchResultListActivity(Activity activity, String selectedHotelStars, Integer selectedPriceMin, Integer selectedPriceMax, Integer selectedRatings, String cityId, String hotelName, String  filterByInfoType) {
        Intent intent = new Intent(activity, SearchResultListActivity.class);

        if(selectedHotelStars.equals("All")) {
            selectedHotelStars = "";
        }

        intent.putExtra("cityId", cityId);
        intent.putExtra("hotelName", hotelName);
        intent.putExtra("hotelStars", selectedHotelStars);
        intent.putExtra("hotelMinPrice", selectedPriceMin);
        intent.putExtra("hotelMaxPrice", selectedPriceMax);
        intent.putExtra("filterByInfoType", filterByInfoType);
        intent.putExtra("minUserRating", selectedRatings);

        activity.startActivity(intent);

    }


    public void navigateToProfileEditActivity(Activity activity) {
        Intent intent = new Intent(activity, ProfileEditActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToUserLoginActivity(Activity activity) {
        Intent intent = new Intent(activity, UserLoginActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToUserRegisterActivity(Activity activity) {
        Intent intent = new Intent(activity, UserRegisterActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToUserForgotPasswordActivity(Activity activity) {
        Intent intent = new Intent(activity, UserForgotPasswordActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToPasswordChangeActivity(Activity activity) {
        Intent intent = new Intent(activity, PasswordChangeActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToContactUs(MainActivity mainActivity) {
        if (checkFragmentChange(RegFragments.HOME_CONTACTUS)) {
            try {
                ContactUsFragment fragment = new ContactUsFragment();
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .replace(containerId, fragment)
                        .commitAllowingStateLoss();
            } catch (Exception e) {
                Utils.psErrorLog("Error! Can't replace fragment.", e);
            }
        }
    }

    public void navigateToInquiry(Activity activity, String hotelId, String roomId) {
        Intent intent = new Intent(activity, InquiryActivity.class);

        intent.putExtra("hotelId", hotelId);
        intent.putExtra("roomId", roomId);

        activity.startActivity(intent);
    }

    public void navigateToBooking(Activity activity, String hotelId, String roomId) {
        Intent intent = new Intent(activity, BookingActivity.class);

        intent.putExtra("hotelId", hotelId);
        intent.putExtra("roomId", roomId);

        activity.startActivity(intent);


    }

    public void navigateToSubmitReview(Activity activity, String roomId, String hotelId) {
        Intent intent = new Intent(activity, SubmitReviewActivity.class);

        intent.putExtra("roomId", roomId);
        intent.putExtra("hotelId", hotelId);

        activity.startActivity(intent);
    }

    public void navigateToHotelFilterActivity(Activity activity, String selectedHotelStars, Integer selectedPriceMin, Integer selectedPriceMax, Integer selectedRatings) {
        Intent intent = new Intent(activity, FilterActivity.class);


        intent.putExtra("selectedHotelStars", selectedHotelStars);
        intent.putExtra("selectedPriceMin", selectedPriceMin);
        intent.putExtra("selectedPriceMax", selectedPriceMax);

        intent.putExtra("selectedRatings", selectedRatings);

        activity.startActivityForResult(intent, 0);
    }

    public void navigateToRoomFilterActivity(Activity activity,
                                             String cityId,
                                             String hotelMinPrice,
                                             String hotelMaxPrice,
                                             String hotelStarRating,
                                             String minUserRating,
                                             String filterByInfoType) {
        Intent intent = new Intent(activity, HotelFilterActivity.class);

        intent.putExtra("cityId", cityId);
        intent.putExtra("selectedHotelStars", hotelStarRating);
        intent.putExtra("userSelectedPriceMin", hotelMinPrice);
        intent.putExtra("userSelectedPriceMax", hotelMaxPrice);
        intent.putExtra("userSelectedRating", minUserRating);
        intent.putExtra("userSelectedFeatures", filterByInfoType);

        activity.startActivityForResult(intent, 1);
    }

    public void navigateToCityListActivity(Activity activity) {
        Intent intent = new Intent(activity, CityListActivity.class);
        activity.startActivityForResult(intent, 1);
    }

    public void navigateToRoomListFilterActivity(Activity activity, String hotelId) {
        Intent intent = new Intent(activity, RoomListFilterActivity.class);
        intent.putExtra("hotelId", hotelId);
        activity.startActivityForResult(intent, 1);
    }

    public void navigateToWelcomeActivity(Activity activity) {
        Intent intent = new Intent(activity, WelcomeActivity.class);
        activity.startActivity(intent);
    }

    //endregion


    //region Private methods
    private Boolean checkFragmentChange(RegFragments regFragments) {
        if (currentFragment != regFragments) {
            currentFragment = regFragments;
            return true;
        }

        return false;
    }

    /**
     * Remark : This enum is only for MainActivity,
     * For the other fragments, no need to register here
     **/
    private enum RegFragments {
        HOME_FRAGMENT,
        HOME_NEWS_LIST_FRAGMENT,
        HOME_CATEGORY_LIST_SELECTION,
        HOME_EDITOR_PICKED_NEWS_LIST,
        HOME_TRENDING_NEWS_LIST,
        HOME_FAVOURITE_NEWS_LIST,
        HOME_USER_LOGIN,
        HOME_USER_REGISTER,
        HOME_USER_FOGOT_PASSWORD,
        HOME_SEARCH,
        HOME_ABOUTUS,
        HOME_CONTACTUS,
        HOME_NOTI_SETTING,
        HOME_RECOMMENDED_HOTEL_LIST,
        HOME_POPULAR_HOTEL_LIST,
        HOME_PROMOTION_HOTEL_LIST,
        HOME_FAVOURITE_HOTEL_LIST,
        HOME_BOOKING_LIST
    }

    //endregion
}
