package com.panaceasoft.pshotels.ui.hotel.popular;

import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;

public class PopularHotelTaskHandler extends BackgroundTaskHandler {

    private final HotelRepository repository;

    public PopularHotelTaskHandler(HotelRepository repository) {
        super();
        this.repository = repository;
    }

    public void getNextPage(PopularHotelLoadingHolder nextPageHolder) {

        if(nextPageHolder == null){
            return;
        }

        unregister();

        holdLiveData = repository.getNextPagePopularHotels(nextPageHolder);
        loadingState.setValue(new LoadingState(true, null));

        if(holdLiveData != null) {
            holdLiveData.observeForever(this);
        }
    }
}
