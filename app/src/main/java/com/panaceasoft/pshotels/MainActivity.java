package com.panaceasoft.pshotels;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.panaceasoft.pshotels.databinding.ActivityMainBinding;
import com.panaceasoft.pshotels.ui.common.NavigationController;
import com.panaceasoft.pshotels.ui.common.PSAppCompactActivity;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewmodel.common.NotificationViewModel;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;
import com.panaceasoft.pshotels.viewobject.User;
import javax.inject.Inject;

/**
 * MainActivity of PSHotels
 * Contact Email : teamps.is.cool@gmail.com
 *
 * @author Panacea-soft
 * @since 11/15/17.
 * @version 1.0
 *
 */

public class MainActivity extends PSAppCompactActivity {


    //region Variables

    @Inject
    SharedPreferences pref;

    private Boolean notiSetting = false;
    private String token = "";
    private UserViewModel userViewModel;

    private NotificationViewModel notificationViewModel;
    private User user;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;

    public ActivityMainBinding binding;
    private BroadcastReceiver broadcastReceiver = null;
    //endregion


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Base_PSTheme);

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if(pref.getBoolean(Utils.IS_FIRST_TIME_LAUNCH,true)) {
            navigationController.navigateToWelcomeActivity(this);
        }

        initUIAndActions();

        initModels();

        initData();

        FirebaseMessaging.getInstance().subscribeToTopic("psnews");


        if(!notiSetting) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Utils.psLog("On Receive Token");

                    String token = intent.getStringExtra("message");

                    if (!token.equals("")) {
                        registerNotificationToken(token);
                    }

                }
            };
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Utils.psLog("Inside Result MainActivity");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {

        try {
            if(broadcastReceiver != null) {
                unregisterReceiver(broadcastReceiver);
            }
        }catch (Exception e) {
            Utils.psErrorLog("Error in unregister.", e);
        }

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();


        Utils.psLog(">>>> On Resume.");

        try {

            if(!notiSetting && broadcastReceiver != null) {
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter("NOW"));
            }

            if (pref.getBoolean(Utils.IS_NOTI_EXISTS_TO_SHOW, false) ||
                    getIntent().getBooleanExtra( Utils.IS_NOTI_EXISTS_TO_SHOW, false) ) {

                String message = pref.getString(Utils.NOTI_MSG, "");

                if (!message.equals("")) {

                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Utils.IS_NOTI_EXISTS_TO_SHOW, false).apply();

                    //if(!alreadyNotiMsgShow) {
                        showAlertMessage(message );
                    //    alreadyNotiMsgShow = true;
                    //}

                }
            }
        }catch (NullPointerException ne){
            Utils.psErrorLog("Null Pointer Exception.", ne);
        }catch(Exception e){
            Utils.psErrorLog("Error in getting notification flag data.", e);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(R.string.message__want_to_quit)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, (DialogInterface dialog, int whichButton) -> {
                        finish();
                        System.exit(0);
                    }).show();

        }
        return true;
    }

    //endregion


    //region Private Methods

    /**
     * Initialize Models
     */
    private void initModels() {
        //MobileAds.initialize(this, getResources().getString(R.string.banner_home_footer));

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
        notificationViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel.class);
    }


    /**
     * Show alert message to user.
     *
     * @param msg Message to show to user
     */
    private void showAlertMessage(String msg) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        View view = inflater.inflate(R.layout.ps_dialog, null);

        builder.setView(view)
                .setPositiveButton(getString(R.string.ok), null);

        TextView message = view.findViewById(R.id.messageTextView);

        message.setText(msg);

        builder.create();

        builder.show();

    }

    /**
     *  This function will initialize UI and Event Listeners
     */
    private void initUIAndActions(){

        // binding.getRoot().startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));

        initToolbar(binding.toolbar, getResources().getString(R.string.app_name));

        initDrawerLayout();

        initNavigationView();

        //navigationController.navigateToHome(this);
        navigationController.navigateToFindHotels(this);

        setSelectMenu(R.id.nav_find_hotels);
    }


    private void initDrawerLayout() {

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        binding.drawerLayout.addDrawerListener(drawerToggle);
        binding.drawerLayout.post(drawerToggle::syncState);

    }

    private void initNavigationView() {

        if (binding.navView != null) {

            // Updating Custom Fonts
            Menu m = binding.navView.getMenu();
            try {
                if (m != null) {
                    for (int i = 0; i < m.size(); i++) {
                        MenuItem mi = m.getItem(i);

                        //for applying a font to subMenu ...
                        SubMenu subMenu = mi.getSubMenu();
                        if (subMenu != null && subMenu.size() > 0) {
                            for (int j = 0; j < subMenu.size(); j++) {
                                MenuItem subMenuItem = subMenu.getItem(j);

                                // update font
                                subMenuItem.setTitle(Utils.getSpannableString(getBaseContext(), subMenuItem.getTitle().toString(), Utils.Fonts.ROBOTO_LIGHT));
                            }
                        }

                        // update font
                        mi.setTitle(Utils.getSpannableString(getBaseContext(), mi.getTitle().toString(), Utils.Fonts.ROBOTO_LIGHT));
                    }
                }
            }catch (Exception e) {
                Utils.psErrorLog("Error in Setting Custom Font", e);
            }

            binding.navView.setNavigationItemSelectedListener( menuItem -> {
                navigationMenuChanged(menuItem);
                return true;
            });

        }

    }

    private void navigationMenuChanged(MenuItem menuItem) {
        openFragment(menuItem.getItemId());
        menuItem.setChecked(true);
        binding.drawerLayout.closeDrawers();
    }

    public void setSelectMenu(int id) {
        binding.navView.setCheckedItem(id);
    }

    private int menuId = 0;

    /**
     *
     * Open Fragment
     * @param menuId To know which fragment to open.
     *
     */
    private void openFragment(int menuId) {

        this.menuId = menuId;
        switch (menuId) {
            case R.id.nav_find_hotels:
            case R.id.nav_find_hotels_login:

                setToolbarText(binding.toolbar, getString(R.string.home));
                navigationController.navigateToFindHotels(this);
                Utils.psLog("nav_find_hotels");

                break;


            case R.id.nav_recommended_hotels:
            case R.id.nav_recommended_hotels_login:

                setToolbarText(binding.toolbar, getString(R.string.recommended_hotels));
                navigationController.navigateToRecommendedHotel(this);
                Utils.psLog("nav_recommended_hotels");

                break;

            case R.id.nav_popular_hotels:
            case R.id.nav_popular_hotels_login:

                setToolbarText(binding.toolbar, getString(R.string.popular_hotels));
                navigationController.navigateToPopularHotel(this);
                Utils.psLog("nav_popular_hotels");

                break;

            case R.id.nav_promotion_hotels:
            case R.id.nav_promotion_hotels_login:

                setToolbarText(binding.toolbar, getString(R.string.promotion_hotels));
                navigationController.navigateToPromotionHotel(this);
                Utils.psLog("nav_promotion_hotels");

                break;

            case R.id.nav_profile:
            case R.id.nav_profile_login:

                if(user == null) {
                    setToolbarText(binding.toolbar, getString(R.string.login));
                    navigationController.navigateToUserLogin(this);
                }else {
                    setToolbarText(binding.toolbar, getString(R.string.profile));
                    navigationController.navigateToUserProfile(this);
                }

                Utils.psLog("nav_profile");

                break;

            case R.id.nav_favourite_news_login:

                setToolbarText(binding.toolbar, getString(R.string.favourite_news));
                navigationController.navigateToFavouriteHotel(this);
                Utils.psLog("nav_favourite_news");

                break;

            case R.id.nav_booking_list_login:

                setToolbarText(binding.toolbar, getString(R.string.booking__hotel));
                navigationController.navigateToBookingList(this);
                Utils.psLog("nav_booking_list_login");

                break;

            case R.id.nav_logout_login:

                userViewModel.deleteUserLogin(user).observe(this, status -> {
                    if(status != null) {
                        this.menuId = 0;
                        setToolbarText(binding.toolbar, getString(R.string.home));
                        navigationController.navigateToFindHotels(this);
                    }
                });

                Utils.psLog("nav_logout_login");

                break;

            case R.id.nav_about_us:
            case R.id.nav_about_us_login:

                Utils.psLog("nav_about_us");
                setToolbarText(binding.toolbar, getString(R.string.about_app));
                navigationController.navigateToAboutUs(this);

                break;

            case R.id.nav_contact_us:
            case R.id.nav_contact_us_login:

                Utils.psLog("nav_contact_us");

                setToolbarText(binding.toolbar, getString(R.string.contact_us));
                navigationController.navigateToContactUs(this);

                break;

            case R.id.nav_setting:
            case R.id.nav_setting_login:

                setToolbarText(binding.toolbar, getString(R.string.notification_setting));
                navigationController.navigateToNotificationSetting(this);
                Utils.psLog("nav_setting");

                break;

        }

    }

    /**
     *  Initialize Data
     */
    private void initData(){

        try {
            Utils.psLog(">>>> On initData.");
            notiSetting = pref.getBoolean(Utils.NOTI_SETTING, false);
            token = pref.getString(Utils.NOTI_TOKEN, "");

        }catch (NullPointerException ne){
            Utils.psErrorLog("Null Pointer Exception.", ne);
        }catch(Exception e){
            Utils.psErrorLog("Error in getting notification flag data.", e);
        }

        /*
         * Check Notification
         * If there is pending notification to show,
         * Show it to the user.
         */
        /*try {

            if (getIntent().getBooleanExtra( Utils.IS_NOTI_EXISTS_TO_SHOW, false)) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Utils.IS_NOTI_EXISTS_TO_SHOW, false).apply();

                navigationController.navigateToHome(this);

                String msg = getIntent().getStringExtra(Utils.NOTI_MSG);

                Utils.psLog("Message : " + msg);


                if(!alreadyNotiMsgShow) {
                    showAlertMessage(msg);
                    alreadyNotiMsgShow = true;
                }


            } else {

                navigationController.navigateToHome(this);
            }

        }catch(Exception e){
            Utils.psErrorLog("Error in getting notification flag data.", e);
        }*/

        /*
         * Get Login User and Update Menu
         */
        userViewModel.getLoginUser().observe(this, data -> {

            if (data != null ) {

                if(data.size() > 0) {
                    user = data.get(0).user;

                    pref.edit().putString("user_id", user.user_id).apply();
                    pref.edit().putString("user_name", user.user_name).apply();
                    pref.edit().putString("user_email", user.user_email).apply();

                }else {
                    user = null;

                    pref.edit().remove("user_id").apply();
                    pref.edit().remove("user_name").apply();
                    pref.edit().remove("user_email").apply();
                }

            } else {

                user = null;
                pref.edit().remove("user_id").apply();
                pref.edit().remove("user_name").apply();
                pref.edit().remove("user_email").apply();

                Utils.psLog("Empty Data");

            }
            updateMenu();

        });


        registerNotificationToken(""); // Just send "" because don't have token to sent. It will get token itself.
    }

    /**
     * This function will change the menu based on the user is logged in or not.
     */
    private void updateMenu() {

        if(user == null){
            binding.navView.getMenu().setGroupVisible(R.id.group_before_login, true);
            binding.navView.getMenu().setGroupVisible(R.id.group_after_login, false);

            setSelectMenu(R.id.nav_find_hotels);

        } else {
            binding.navView.getMenu().setGroupVisible(R.id.group_after_login, true);
            binding.navView.getMenu().setGroupVisible(R.id.group_before_login, false);

            if(menuId == R.id.nav_profile) {
                setSelectMenu(R.id.nav_profile_login);
            }else{
                setSelectMenu(R.id.nav_find_hotels_login);
            }

        }


    }

    private void registerNotificationToken(String token) {
        /*
         * Register Notification
         */

        // Check already submit or not
        // If haven't, submit to server
        if(!notiSetting) {

            if(this.token.equals("")) {
                Utils.psLog("Registering Notification Token...");
                notificationViewModel.registerNotification(getBaseContext(), Utils.PLATFORM, token);
            }
        }else{
            Utils.psLog("Notification Token is already registered. Notification Setting : true.");
        }
    }

    //endregion


}
