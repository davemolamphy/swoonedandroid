package com.panaceasoft.pshotels.api;

import android.arch.lifecycle.LiveData;

import com.panaceasoft.pshotels.viewobject.AboutUs;
import com.panaceasoft.pshotels.viewobject.ApiStatus;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.City;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.Price;
import com.panaceasoft.pshotels.viewobject.ReviewCategory;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;
import com.panaceasoft.pshotels.viewobject.Review;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;
import com.panaceasoft.pshotels.viewobject.Image;
import com.panaceasoft.pshotels.viewobject.User;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * REST API access points
 */
public interface PSApiService {


    //region Hotel Related

    //region Hotels Search

    @FormUrlEncoded
    @POST("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}")
    LiveData<ApiResponse<List<Hotel>>> postSearchHotel(@Path("API_KEY") String apiKey,
                                                       @Path("login_user_id") String loginUserId,
                                                       @Path("limit") String limit,
                                                       @Path("offset") String offset,
                                                       @Field("city_id") String cityId,
                                                       @Field("hotel_name") String hotelName,
                                                       @Field("hotel_star_rating") String hotelStarRating,
                                                       @Field("hotel_min_price") String hotelMinPrice,
                                                       @Field("hotel_max_price") String hotelMaxPrice,
                                                       @Field("filter_by_info_type") String filterByInfoType,
                                                       @Field("min_user_rating") String minUserRating
    );

    @FormUrlEncoded
    @POST("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}")
    Call<List<Hotel>> rawPostSearchHotel(@Path("API_KEY") String apiKey,
                                         @Path("login_user_id") String loginUserId,
                                         @Path("limit") String limit,
                                         @Path("offset") String offset,
                                         @Field("city_id") String cityId,
                                         @Field("hotel_name") String hotelName,
                                         @Field("hotel_star_rating") String hotelStarRating,
                                         @Field("hotel_min_price") String hotelMinPrice,
                                         @Field("hotel_max_price") String hotelMaxPrice,
                                         @Field("filter_by_info_type") String filterByInfoType,
                                         @Field("min_user_rating") String minUserRating
    );

    //endregion

    //region Get Hotel ById
    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/hotel_id/{hotel_id}")
    LiveData<ApiResponse<List<Hotel>>> getHotelById(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("hotel_id") String hotelId);

    //endregion

    // region recommended hotel

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/is_recommended/{is_recommended}")
    LiveData<ApiResponse<List<Hotel>>> getRecommendedHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("is_recommended") String is_recommended);

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/is_recommended/{is_recommended}")
    Call<List<Hotel>> getRawRecommendedHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("is_recommended") String is_recommended);

    //endregion

    //region popular hotel

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/popular/{popular}")
    LiveData<ApiResponse<List<Hotel>>> getPopularHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("popular") String popular);

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/popular/{popular}")
    Call<List<Hotel>> getRawPopularHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("popular") String popular);

    //endregion

    //region promotion hotel

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/promotion_only/{promotion}")
    LiveData<ApiResponse<List<Hotel>>> getPromotionHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("promotion") String promotion);

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/promotion_only/{promotion}")
    Call<List<Hotel>> getRawPromotionHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("promotion") String promotion);

    //endregion

    //region favourited hotels

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/is_favourited/{favourite}")
    LiveData<ApiResponse<List<Hotel>>> getFavouritedHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("favourite") String favourite);

    @GET("rest/hotels/get/api_key/{API_KEY}/login_user_id/{login_user_id}/limit/{limit}/offset/{offset}/is_favourited/{favourite}")
    Call<List<Hotel>> getRawFavouritedHotel(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("limit") String limit, @Path("offset") String offset, @Path("favourite") String favourite);

    //endregion

    //region POST Hotel Open Count

    @FormUrlEncoded
    @POST("rest/hotel_touches/add/api_key/{API_KEY}")
    LiveData<ApiResponse<ApiStatus>> postTouchCount(@Path("API_KEY") String apiKey, @Field("hotel_id") String hotelId, @Field("login_user_id") String loginUserId);

    @FormUrlEncoded
    @POST("rest/hotel_touches/add/api_key/{API_KEY}")
    Call<ApiStatus> rawPostTouchCount(@Path("API_KEY") String apiKey, @Field("hotel_id") String hotelId, @Field("login_user_id") String loginUserId);

    // ROOM
    @FormUrlEncoded
    @POST("rest/room_touches/add/api_key/{API_KEY}")
    Call<ApiStatus> rawRoomPostTouchCount(@Path("API_KEY") String apiKey, @Field("room_id") String roomId, @Field("login_user_id") String loginUserId);

    //endregion

    //region POST Favourite

    @FormUrlEncoded
    @POST("rest/favourites/press/api_key/{API_KEY}")
    Call<Hotel> rawPostFavHotel(@Path("API_KEY") String apiKey, @Field("hotel_id") String hotelId, @Field("login_user_id") String loginUserId);


    //endregion

    //endregion


    //region Room Related

    @GET("rest/rooms/get/api_key/{API_KEY}/login_user_id/{login_user_id}/hotel_id/{hotel_id}/limit/{limit}/offset/{offset}")
    LiveData<ApiResponse<List<Room>>> getRoomListByHotelId(@Path("API_KEY") String apiKey,
                                                           @Path("login_user_id") String loginUserId,
                                                           @Path("hotel_id") String hotelId,
                                                           @Path("limit") String limit,
                                                           @Path("offset") String offset
    );

    @GET("rest/rooms/get/api_key/{API_KEY}/hotel_id/{hotel_id}")
    LiveData<ApiResponse<List<Room>>> getAllRoomListByHotelId(@Path("API_KEY") String apiKey,
                                                              @Path("hotel_id") String hotelId
    );

    @GET("rest/rooms/get/api_key/{API_KEY}/login_user_id/{login_user_id}/hotel_id/{hotel_id}/limit/{limit}/offset/{offset}")
    Call<List<Room>> getRawRoomListByHotelId(@Path("API_KEY") String apiKey,
                                             @Path("login_user_id") String loginUserId,
                                             @Path("hotel_id") String hotelId,
                                             @Path("limit") String limit,
                                             @Path("offset") String offset
    );

    //endregion


    //region Get City

    @GET("rest/cities/get/api_key/{API_KEY}")
    LiveData<ApiResponse<List<City>>> getCities(@Path("API_KEY") String apiKey);

    //endregion


    //region Features

    //region Hotels Info Related

    @GET("rest/hotels/features/api_key/{API_KEY}/hotel_id/{hotelId}")
    LiveData<ApiResponse<List<HotelFeatures>>> getHotelFeatureByHotelId(@Path("API_KEY") String apiKey, @Path("hotelId") String hotelId);

    @GET("rest/hotels/features/api_key/{API_KEY}/city_id/{city_id}")
    LiveData<ApiResponse<List<HotelFeatures>>> getHotelFeaturesByCityId(@Path("API_KEY") String apiKey, @Path("city_id") String cityId);

    //endregion

    //region Rooms Info Related

    @GET("rest/rooms/get/api_key/{API_KEY}/login_user_id/{login_user_id}/room_id/{room_id}")
    LiveData<ApiResponse<List<Room>>> getRoomById(@Path("API_KEY") String apiKey, @Path("login_user_id") String loginUserId, @Path("room_id") String roomId);

    @GET("rest/rooms/features/api_key/{API_KEY}/room_id/{room_id}")
    LiveData<ApiResponse<List<RoomFeatures>>> getRoomFeatureByRoomId(@Path("API_KEY") String apiKey, @Path("room_id") String room_id);

    //endregion

    //endregion


    // region Review

    @GET("rest/reviews/summary/api_key/{API_KEY}/hotel_id/{hotelId}")
    LiveData<ApiResponse<Review>> getHotelReviewSummary(@Path("API_KEY") String apiKey, @Path("hotelId") String hotelId);

    @GET("rest/reviews/details/api_key/{API_KEY}/hotel_id/{hotelId}/limit/{limit}/offset/{offset}")
    LiveData<ApiResponse<List<ReviewDetail>>> getHotelReviewDetail(@Path("API_KEY") String apiKey, @Path("hotelId") String hotelId, @Path("limit") String limit, @Path("offset") String offset);

    @GET("rest/reviews/details/api_key/{API_KEY}/hotel_id/{hotelId}/limit/{limit}/offset/{offset}")
    Call<List<ReviewDetail>> getRawHotelReviewDetail(@Path("API_KEY") String apiKey, @Path("hotelId") String hotelId, @Path("limit") String limit, @Path("offset") String offset);

    @GET("rest/reviews/summary/api_key/{API_KEY}/room_id/{roomId}")
    LiveData<ApiResponse<Review>> getRoomReviewSummary(@Path("API_KEY") String apiKey, @Path("roomId") String roomId);

    @GET("rest/reviews/summary/api_key/{API_KEY}/room_id/{roomId}")
    Call<Review> getRawRoomReviewSummary(@Path("API_KEY") String apiKey, @Path("roomId") String roomId);

    @GET("rest/reviews/details/api_key/{API_KEY}/room_id/{roomId}/limit/{limit}/offset/{offset}")
    LiveData<ApiResponse<List<ReviewDetail>>> getRoomReviewDetail(@Path("API_KEY") String apiKey, @Path("roomId") String roomId, @Path("limit") String limit, @Path("offset") String offset);

    @GET("rest/reviews/details/api_key/{API_KEY}/room_id/{roomId}/limit/{limit}/offset/{offset}")
    Call<List<ReviewDetail>> getRawRoomReviewDetail(@Path("API_KEY") String apiKey, @Path("roomId") String roomId, @Path("limit") String limit, @Path("offset") String offset);

    @FormUrlEncoded
    @POST("rest/reviews/submit/api_key/{API_KEY}")
    Call<ApiStatus> rawPostReview(
            @Path("API_KEY") String apiKey,
            @Field("room_id") String roomId,
            @Field("user_id") String userId,
            @Field("review_desc") String reviewDesc,
            @Field("ratings") String syncUserRatingHolder
    );


    //region review categories
    @GET("rest/review_categories/get/api_key/{API_KEY}")
    LiveData<ApiResponse<List<ReviewCategory>>> getAllReviewCategory(@Path("API_KEY") String apiKey);
    //endregion

    //endregion


    //region POST Notification

    // Submit Notification Token
    @FormUrlEncoded
    @POST("rest/notis/register/api_key/{API_KEY}")
    Call<ApiStatus> rawRegisterNotiToken(@Path("API_KEY") String apiKey, @Field("platform_name") String platform, @Field("device_id") String deviceId);


    @FormUrlEncoded
    @POST("rest/notis/unregister/api_key/{API_KEY}")
    Call<ApiStatus> rawUnregisterNotiToken(@Path("API_KEY") String apiKey, @Field("platform_name") String platform, @Field("device_id") String deviceId);

    //endregion


    //region User Related

    //region GET User
    @GET("rest/users/get/api_key/{API_KEY}/user_id/{user_id}")
    LiveData<ApiResponse<List<User>>> getUser(@Path("API_KEY") String apiKey, @Path("user_id") String userId);

    //endregion

    //region POST Upload Image

    @Multipart
    @POST("rest/images/upload/api_key/{API_KEY}")
    LiveData<ApiResponse<User>> doUploadImage(@Path("API_KEY") String apiKey, @Part("user_id") RequestBody userId, @Part("file") RequestBody name, @Part MultipartBody.Part file, @Part("platform_name") RequestBody platformName);

    //endregion

    //region POST User for Login

    @FormUrlEncoded
    @POST("rest/users/login/api_key/{API_KEY}")
    LiveData<ApiResponse<User>> postUserLogin(@Path("API_KEY") String apiKey, @Field("user_email") String userEmail, @Field("user_password") String userPassword);

    //endregion

    //region POST User for Register

    @FormUrlEncoded
    @POST("rest/users/add/api_key/{API_KEY}")
    LiveData<ApiResponse<User>> postUser(@Path("API_KEY") String apiKey, @Field("user_name") String userName, @Field("user_email") String userEmail, @Field("user_password") String userPassword);

    //endregion

    //region POST Forgot Password

    @FormUrlEncoded
    @POST("rest/users/reset/api_key/{API_KEY}")
    LiveData<ApiResponse<ApiStatus>> postForgotPassword(@Path("API_KEY") String apiKey, @Field("user_email") String userEmail);

    //endregion

    //region PUT User for User Update

    @FormUrlEncoded
    @PUT("rest/users/profile_update/api_key/{API_KEY}")
    LiveData<ApiResponse<ApiStatus>> putUser(@Path("API_KEY") String apiKey, @Field("login_user_id") String loginUserId, @Field("user_name") String userName, @Field("user_email") String userEmail, @Field("user_phone") String userPhone, @Field("user_about_me") String userAboutMe);

    //endregion

    //region PUT for Password Update

    @FormUrlEncoded
    @PUT("rest/users/password_update/api_key/{API_KEY}")
    LiveData<ApiResponse<ApiStatus>> postPasswordUpdate(@Path("API_KEY") String apiKey, @Field("login_user_id") String loginUserId, @Field("user_password") String password);

    //endregion

    //endregion


    //region About Us

    @GET("rest/abouts/get/api_key/{API_KEY}")
    LiveData<ApiResponse<List<AboutUs>>> getAboutUs(@Path("API_KEY") String apiKey);

    //endregion


    //region Contact Us
    @FormUrlEncoded
    @POST("rest/contacts/add/api_key/{API_KEY}")
    Call<ApiStatus> rawPostContact(@Path("API_KEY") String apiKey, @Field("contact_name") String contactName, @Field("contact_email") String contactEmail, @Field("contact_message") String contactMessage, @Field("contact_phone") String contactPhone);
    //endregion


    //region Inquiry
    @FormUrlEncoded
    @POST("rest/inquirys/add/api_key/{API_KEY}")
    Call<ApiStatus> rawPostInquiry(
            @Path("API_KEY") String apiKey,
            @Field("hotel_id") String hotelId,
            @Field("room_id") String roomId,
            @Field("user_id") String userId,
            @Field("inq_name") String inqName,
            @Field("inq_desc") String inqDesc,
            @Field("inq_user_name") String contactName,
            @Field("inq_user_email") String contactEmail,
            @Field("inq_user_phone") String contactPhone
    );
    //endregion


    //region GET Image List

    @GET("rest/images/get/api_key/{API_KEY}/img_parent_id/{img_parent_id}")
    LiveData<ApiResponse<List<Image>>> getImageList(@Path("API_KEY") String apiKey, @Path("img_parent_id") String imgParentId);

    //endregion


    //region GET Max Price

    @GET("rest/hotels/max_price/api_key/{API_KEY}")
    LiveData<ApiResponse<Price>> getMaxPrice(@Path("API_KEY") String apiKey);

    //endregion


    //region Booking

    @FormUrlEncoded
    @POST("rest/bookings/add/api_key/{API_KEY}")
    Call<Booking> rawPostBooking(
            @Path("API_KEY") String apiKey,
            @Field("login_user_id") String loginUserId,
            @Field("booking_user_name") String userName,
            @Field("booking_user_email") String userEmail,
            @Field("booking_user_phone") String userPhone,
            @Field("hotel_id") String hotelId,
            @Field("room_id") String roomId,
            @Field("booking_adult_count") String adultCount,
            @Field("booking_kid_count") String kidCount,
            @Field("booking_start_date") String startDate,
            @Field("booking_end_date") String endDate,
            @Field("booking_extra_bed") String extraBed,
            @Field("booking_remark") String remark
    );

    @GET("rest/bookings/get/api_key/{API_KEY}/login_user_id/{loginUserId}/limit/{limit}/offset/{offset}")
    LiveData<ApiResponse<List<Booking>>> getBookingList(@Path("API_KEY") String apiKey, @Path("loginUserId") String loginUserId, @Path("limit") String limit, @Path("offset") String offset);

    @GET("rest/bookings/get/api_key/{API_KEY}/login_user_id/{loginUserId}/limit/{limit}/offset/{offset}")
    Call<List<Booking>> getRawBookingList(@Path("API_KEY") String apiKey, @Path("loginUserId") String loginUserId, @Path("limit") String limit, @Path("offset") String offset);

    //endregion

}