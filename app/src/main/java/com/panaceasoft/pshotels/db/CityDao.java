package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.panaceasoft.pshotels.viewobject.City;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Dao
public abstract class CityDao {

    //region Common City

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(City city);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(City city);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<City> cityList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAll(List<City> cityList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertAllNewOnly(List<City> cityList);

    @Query("DELETE FROM City")
    public abstract void deleteAll();

    @Query("SELECT * FROM City WHERE city_id = :hid limit 1")
    public abstract LiveData<City> getById(String hid);

    @Query("SELECT * FROM City ")
    public abstract LiveData<List<City>> getAllCity();

    //endregion

}