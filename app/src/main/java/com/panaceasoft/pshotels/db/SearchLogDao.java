package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.panaceasoft.pshotels.viewobject.SearchLog;

@Dao
public interface SearchLogDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SearchLog searchLog);

    @Query("SELECT * FROM SEARCHLOG LIMIT '1'")
    LiveData<SearchLog> get();

    @Query("DELETE FROM SEARCHLOG")
    void deleteAll();
}