package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.panaceasoft.pshotels.viewobject.Review;
import com.panaceasoft.pshotels.viewobject.ReviewCategoriesData;
import com.panaceasoft.pshotels.viewobject.ReviewCategory;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.ReviewSummaryWithCategoryData;

import java.util.List;

/**
 * Created by Panacea-Soft on 3/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public abstract class ReviewDao {

    //region Review Summary

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(Review review);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(Review review);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllReviewSummary(List<Review> reviewList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllReviewSummary(List<Review> reviewList);

    @Query("DELETE FROM Review WHERE review_parent_id = :reviewParentId ")
    public abstract void deleteRoomReviewSummary(String reviewParentId);

    @Transaction @Query("SELECT * FROM Review")
    public abstract LiveData<List<ReviewSummaryWithCategoryData>> getAllRoomReviewSummary();

    @Transaction @Query("SELECT * FROM Review WHERE review_parent_id = :reviewParentId limit 1")
    public abstract LiveData<ReviewSummaryWithCategoryData> getReviewDataWithParentId(String reviewParentId);

    //endregion

    //region Review Details

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(ReviewDetail reviewDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(ReviewDetail reviewDetail);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllReviewDetail(List<ReviewDetail> reviewDetailList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllReviewDetail(List<ReviewDetail> reviewDetailList);

    @Query("DELETE FROM ReviewDetail WHERE review_parent_id = :reviewParentId ")
    public abstract void deleteReviewDetailWithParentId(String reviewParentId);

    @Query("SELECT * FROM ReviewDetail")
    public abstract LiveData<List<ReviewDetail>> getAllReviewDetail();

    @Query("SELECT * FROM ReviewDetail WHERE review_parent_id = :reviewParentId")
    public abstract LiveData<List<ReviewDetail>> getReviewDetailDataWithParentId(String reviewParentId);

    //endregion

    //region Room Review Categories Data

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(ReviewCategory reviewCategory);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(ReviewCategory reviewCategory);

    @Query("SELECT * FROM REVIEWCATEGORY")
    public abstract LiveData<List<ReviewCategory>> getAllReviewCategory();

    @Query("DELETE FROM REVIEWCATEGORY")
    public abstract void deleteAllReviewCategory();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertReviewCategoryList(List<ReviewCategory> reviewCategoryList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllReviewCategory(List<ReviewCategoriesData> reviewCategoryList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllReviewCategory(List<ReviewCategoriesData> reviewCategoryList);

    @Query("SELECT * FROM ReviewCategoriesData WHERE review_parent_id = :review_parent_id")
    public abstract List<ReviewCategoriesData> getReviewCategoriesDataWithParentId(String review_parent_id);

    //endregion

}
