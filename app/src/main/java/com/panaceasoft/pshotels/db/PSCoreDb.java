package com.panaceasoft.pshotels.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

import com.panaceasoft.pshotels.db.common.Converters;
import com.panaceasoft.pshotels.viewobject.AboutUs;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.City;
import com.panaceasoft.pshotels.viewobject.Country;
import com.panaceasoft.pshotels.viewobject.FavouriteHotel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;
import com.panaceasoft.pshotels.viewobject.HotelFeatureDetail;
import com.panaceasoft.pshotels.viewobject.Image;
import com.panaceasoft.pshotels.viewobject.PopularHotel;
import com.panaceasoft.pshotels.viewobject.Price;
import com.panaceasoft.pshotels.viewobject.PromotionHotel;
import com.panaceasoft.pshotels.viewobject.RecommendedHotel;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.RoomByHotelId;
import com.panaceasoft.pshotels.viewobject.Review;
import com.panaceasoft.pshotels.viewobject.ReviewCategoriesData;
import com.panaceasoft.pshotels.viewobject.ReviewCategory;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatureDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;
import com.panaceasoft.pshotels.viewobject.SearchHotel;
import com.panaceasoft.pshotels.viewobject.SearchLog;
import com.panaceasoft.pshotels.viewobject.User;
import com.panaceasoft.pshotels.viewobject.UserLogin;

/**
 * Created by Panacea-Soft on 11/20/17.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Database(entities = {
        Image.class,
        User.class,
        UserLogin.class,
        AboutUs.class,
        HotelFeatures.class,
        HotelFeatureDetail.class,
        Review.class,
        ReviewCategory.class,
        ReviewCategoriesData.class,
        ReviewDetail.class,
        Hotel.class,
        SearchHotel.class,
        RecommendedHotel.class,
        PopularHotel.class,
        PromotionHotel.class,
        Room.class,
        RoomByHotelId.class,
        FavouriteHotel.class,
        RoomFeatures.class,
        RoomFeatureDetail.class,
        City.class,
        Country.class,
        SearchLog.class,
        Price.class,
        Booking.class
}, version = 4, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class PSCoreDb extends RoomDatabase {

    // PSHotels
    abstract public HotelDao hotelDao();

    abstract public CityDao cityDao();

    abstract public RoomDao roomDao();

    abstract public UserDao userDao();

    abstract public AboutUsDao aboutUsDao();

    abstract public ImageDao imageDao();

    abstract public HotelFeatureDao hotelInfoDao();

    abstract public ReviewDao reviewDao();

    abstract public RoomFeatureDao roomFeatureDao();

    abstract public SearchLogDao searchLogDao();

    abstract public PriceDao priceDao();

    abstract public BookingDao bookingDao();


    /**
     * Migrate from:
     * version 1 - using Room
     * to
     * version 2 - using Room where the {@link } has an extra field: added_date_str
     */
    public static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `Booking` (`booking_id` TEXT NOT NULL, `user_id` TEXT, `booking_user_name` TEXT, `booking_user_email` TEXT, `booking_user_phone` TEXT, `hotel_id` TEXT, `room_id` TEXT, `added_date` TEXT, `booking_adult_count` TEXT, `booking_kid_count` TEXT, `booking_start_date` TEXT, `booking_end_date` TEXT, `booking_extra_bed` TEXT, `booking_remark` TEXT, `booking_status` TEXT, `hotel_hotel_id` TEXT, `hotel_city_id` TEXT, `hotel_hotel_name` TEXT, `hotel_hotel_desc` TEXT, `hotel_hotel_address` TEXT, `hotel_hotel_lat` TEXT, `hotel_hotel_lng` TEXT, `hotel_hotel_phone` TEXT, `hotel_hotel_email` TEXT, `hotel_hotel_min_price` TEXT, `hotel_hotel_max_price` TEXT, `hotel_hotel_star_rating` TEXT, `hotel_hotel_check_in` TEXT, `hotel_hotel_check_out` TEXT, `hotel_is_recommended` TEXT, `hotel_status` TEXT, `hotel_added_date` TEXT, `hotel_added_date_str` TEXT, `hotel_currency_symbol` TEXT, `hotel_currency_short_form` TEXT, `hotel_touch_count` TEXT, `hotel_image_count` TEXT, `hotel_is_user_favourited` TEXT, `hotel_photo_img_id` TEXT, `hotel_photo_img_parent_id` TEXT, `hotel_photo_img_type` TEXT, `hotel_photo_img_path` TEXT, `hotel_photo_Img_width` TEXT, `hotel_photo_img_height` TEXT, `hotel_photo_img_desc` TEXT, `hotel_promotion_promo_id` TEXT, `hotel_promotion_promo_name` TEXT, `hotel_promotion_promo_desc` TEXT, `hotel_promotion_promo_percent` TEXT, `hotel_promotion_promo_start_time` TEXT, `hotel_promotion_promo_end_time` TEXT, `hotel_promotion_status` TEXT, `hotel_promotion_added_date` TEXT, `hotel_rating_final_rating` TEXT, `hotel_rating_rating_text` TEXT, `hotel_rating_review_count` TEXT, `room_room_id` TEXT, `room_hotel_id` TEXT, `room_room_name` TEXT, `room_room_desc` TEXT, `room_room_size` TEXT, `room_room_price` TEXT, `room_room_no_of_beds` TEXT, `room_room_adult_limit` TEXT, `room_room_kid_limit` TEXT, `room_room_extra_bed_price` TEXT, `room_status` TEXT, `room_added_date` TEXT, `room_added_date_str` TEXT, `room_touch_count` TEXT, `room_image_count` TEXT, `room_currency_symbol` TEXT, `room_currency_short_form` TEXT, `room_photo_img_id` TEXT, `room_photo_img_parent_id` TEXT, `room_photo_img_type` TEXT, `room_photo_img_path` TEXT, `room_photo_Img_width` TEXT, `room_photo_img_height` TEXT, `room_photo_img_desc` TEXT, `room_rating_final_rating` TEXT, `room_rating_rating_text` TEXT, `room_rating_review_count` TEXT, `room_promotion_promo_id` TEXT, `room_promotion_promo_name` TEXT, `room_promotion_promo_desc` TEXT, `room_promotion_promo_percent` TEXT, `room_promotion_promo_start_time` TEXT, `room_promotion_promo_end_time` TEXT, `room_promotion_status` TEXT, `room_promotion_added_date` TEXT, PRIMARY KEY(`booking_id`))");
        }
    };



    /**
     * Migrate from:
     * version 3 - using Room
     * to
     * version 4 - an extra field: is_available for room table and room_is_available for booking table
     */

    public static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Room " +
                    " ADD COLUMN is_available TEXT DEFAULT '' ");

            database.execSQL("ALTER TABLE Booking " +
                    " ADD COLUMN room_is_available TEXT DEFAULT '' ");
        }
    };

    /* More migration write here */

}
