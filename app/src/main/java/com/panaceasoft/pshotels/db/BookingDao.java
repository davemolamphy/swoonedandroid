package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.panaceasoft.pshotels.viewobject.Booking;

import java.util.List;

/**
 * Created by Panacea-Soft on 4/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

@Dao
public interface BookingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Booking booking);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Booking> bookingList);

    @Query("SELECT * FROM Booking LIMIT '1'")
    LiveData<Booking> get();

    @Query("SELECT * FROM Booking WHERE booking_id = :bookingId LIMIT '1' ")
    LiveData<Booking> getBookingById(String bookingId);

    @Query("SELECT * FROM Booking order by added_date desc")
    LiveData<List<Booking>> getAll();

    @Query("DELETE FROM Booking WHERE booking_id = :bookingId")
    void deleteById(String bookingId);

    @Query("DELETE FROM Booking")
    void deleteTable();

}
