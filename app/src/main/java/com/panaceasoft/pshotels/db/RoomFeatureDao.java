package com.panaceasoft.pshotels.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.panaceasoft.pshotels.viewobject.RoomFeatureDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;

import java.util.List;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public abstract class RoomFeatureDao {

    //region Room Info Group
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(RoomFeatures roomFeatures);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(RoomFeatures roomFeatures);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllGroup(List<RoomFeatures> roomFeaturesList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllGroup(List<RoomFeatures> roomFeaturesList);

    @Query("DELETE FROM RoomFeatures WHERE rinfo_grp_id = :groupId ")
    public abstract void deleteRoomInfoGroup(String groupId);

    @Query("SELECT * FROM RoomFeatures")
    public abstract List<RoomFeatures> getAll();

    @Query("SELECT * FROM RoomFeatures WHERE rinfo_parent_id =:parentId")
    public abstract List<RoomFeatures> getAllByParentId(String parentId);

    //endregion

    //region Room Info Types
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(RoomFeatureDetail roomFeatureDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(RoomFeatureDetail roomFeatureDetail);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllType(List<RoomFeatureDetail> roomFeatureDetailList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllType(List<RoomFeatureDetail> roomFeatureDetailList);

    @Query("DELETE FROM RoomFeatureDetail WHERE rinfo_grp_id = :groupId ")
    public abstract void deleteRoomInfoTypes(String groupId);

    @Query("SELECT * FROM RoomFeatureDetail WHERE rinfo_grp_id = :roomGrpId")
    public abstract List<RoomFeatureDetail> getAllByGroupId(String roomGrpId);

    public List<RoomFeatures> getRoomFeatureByParentId(String parentId){

        List<RoomFeatures> roomFeaturesList = getAllByParentId(parentId);

        for(int i = 0; i< roomFeaturesList.size(); i++ ){
            roomFeaturesList.get(i).rinf_types_arr = getAllByGroupId(roomFeaturesList.get(i).rinfo_grp_id);
        }

        return roomFeaturesList;
    }

    //endregion
}
