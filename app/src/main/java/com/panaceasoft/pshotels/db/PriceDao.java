package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.panaceasoft.pshotels.viewobject.Price;
import java.util.List;

/**
 * Created by Panacea-Soft on 4/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public interface PriceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Price price);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Price> priceList);

    @Query("SELECT * FROM Price LIMIT '1'")
    LiveData<Price> get();

    @Query("SELECT * FROM Price")
    LiveData<List<Price>> getAll();

    @Query("DELETE FROM Price")
    void deleteTable();

}