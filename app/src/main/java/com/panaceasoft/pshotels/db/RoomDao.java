package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.RoomByHotelId;

import java.util.List;

@Dao
public abstract class RoomDao {

    //region Common Room

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(Room room);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(Room room);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Room> roomList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAll(List<Room> roomList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertAllNewOnly(List<Room> roomList);

    @Query("SELECT * FROM Room WHERE room_id = :rid limit 1")
    public abstract LiveData<Room> getById(String rid);

    //endregion

    // region room by hotel id

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(RoomByHotelId roomByHotelId);

    @Query("DELETE FROM ROOMBYHOTELID")
    public abstract void deleteRoomByHotelIds();

    @Query("SELECT * FROM Room WHERE room_id IN (SELECT room_id FROM ROOMBYHOTELID) ORDER BY added_date_str DESC")
    public abstract LiveData<List<Room>> getAllRoomByHotelId();

    // endregion
}
