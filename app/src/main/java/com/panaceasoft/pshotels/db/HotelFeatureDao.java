package com.panaceasoft.pshotels.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.panaceasoft.pshotels.viewobject.HotelFeatureDetail;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;

import java.util.List;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public abstract class HotelFeatureDao {

    //region Hotel Info Group
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(HotelFeatures hotelFeatures);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(HotelFeatures hotelFeatures);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllGroup(List<HotelFeatures> hotelFeaturesList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllGroup(List<HotelFeatures> hotelFeaturesList);

    @Query("DELETE FROM HotelFeatures WHERE hinfo_grp_id = :groupId ")
    public abstract void deleteHotelInfoGroup(String groupId);

    @Query("SELECT * FROM HotelFeatures")
    public abstract List<HotelFeatures> getAll();

    @Query("SELECT * FROM HotelFeatures WHERE hinfo_parent_id =:parentId")
    public abstract List<HotelFeatures> getAllByParentId(String parentId);

    //endregion

    //region Hotel Info Types
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(HotelFeatureDetail hotelFeatureDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(HotelFeatureDetail hotelFeatureDetail);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAllType(List<HotelFeatureDetail> hotelFeatureDetailList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAllType(List<HotelFeatureDetail> hotelFeatureDetailList);

    @Query("DELETE FROM HotelFeatureDetail WHERE hinfo_grp_id = :groupId ")
    public abstract void deleteHotelInfoTypes(String groupId);

    @Query("SELECT * FROM HotelFeatureDetail WHERE hinfo_grp_id = :hotelGrpId AND hinfo_parent_id = :parentId")
    public abstract List<HotelFeatureDetail> getAllByGroupId(String hotelGrpId, String parentId);

    @Query("DELETE FROM HotelFeatureDetail WHERE hinfo_parent_id = :parentId ")
    public abstract void deleteHotelFeatureByParentId(String parentId);

    public List<HotelFeatures> getHotelFeatureByParentId(String parentId) {

        List<HotelFeatures> hotelFeaturesList = getAllByParentId(parentId);

        for (int i = 0; i < hotelFeaturesList.size(); i++) {
            hotelFeaturesList.get(i).hinf_types_arr = getAllByGroupId(hotelFeaturesList.get(i).hinfo_grp_id, parentId);
        }

        return hotelFeaturesList;

    }

    //endregion

}
