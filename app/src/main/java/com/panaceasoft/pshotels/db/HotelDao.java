package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.panaceasoft.pshotels.viewobject.FavouriteHotel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.PopularHotel;
import com.panaceasoft.pshotels.viewobject.PromotionHotel;
import com.panaceasoft.pshotels.viewobject.RecommendedHotel;
import com.panaceasoft.pshotels.viewobject.SearchHotel;

import java.util.List;


/**
 * Created by Panacea-Soft on 3/30/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public abstract class HotelDao {

    //region Common Hotel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(Hotel hotel);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void update(Hotel hotel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Hotel> hotelList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateAll(List<Hotel> hotelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertAllNewOnly(List<Hotel> hotelList);

    @Query("SELECT * FROM Hotel WHERE hotel_id = :hotelId")
    public abstract Hotel getDirectNewsById(String hotelId);

    @Query("SELECT * FROM Hotel WHERE hotel_id = :hid limit 1")
    public abstract LiveData<Hotel> getById(String hid);

    //endregion

    //region Recommended Hotel List

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(RecommendedHotel recommendedHotel);

    @Query("DELETE FROM RECOMMENDEDHOTEL")
    public abstract void deleteRecommendedHotels();

    @Query("SELECT * FROM Hotel WHERE hotel_id IN (SELECT hotel_id FROM RECOMMENDEDHOTEL) ORDER BY touch_count DESC, added_date_str DESC")
    public abstract LiveData<List<Hotel>> getAllRecommendedHotel();

    //endregion

    // region popular hotel list

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(PopularHotel popularHotel);

    @Query("DELETE FROM POPULARHOTEL")
    public abstract void deletePopularHotels();

    @Query("SELECT * FROM Hotel WHERE hotel_id IN (SELECT hotel_id FROM POPULARHOTEL) ORDER BY touch_count DESC, added_date_str DESC")
    public abstract LiveData<List<Hotel>> getAllPopularHotel();

    // endregion

    // region promotion hotel list

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(PromotionHotel popularHotel);

    @Query("DELETE FROM PROMOTIONHOTEL")
    public abstract void deletePromotionHotels();

    @Query("SELECT * FROM Hotel WHERE hotel_id IN (SELECT hotel_id FROM PROMOTIONHOTEL) ORDER BY touch_count DESC, added_date_str DESC")
    public abstract LiveData<List<Hotel>> getAllPromotionHotel();

    // endregion

    // region favourite hotel list

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(FavouriteHotel favouriteHotel);

    @Query("DELETE FROM FAVOURITEHOTEL")
    public abstract void deleteFavouriteHotels();

    @Query("SELECT * FROM Hotel WHERE is_user_favourited = 'true' and hotel_id IN (SELECT hotel_id FROM FAVOURITEHOTEL) ORDER BY added_date_str DESC")
    public abstract LiveData<List<Hotel>> getAllFavouriteHotels();

    // endregion

    //region Search Hotel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(SearchHotel searchHotel);

    @Query("SELECT * FROM Hotel WHERE hotel_id IN (SELECT hotel_id FROM SEARCHHOTEL) ORDER BY added_date DESC")
    public abstract LiveData<List<Hotel>> getSearchHotel();

    // need to improve
    // still need to check others (e.g. popular hotel, recommended hotel, ... )
    @Query("DELETE FROM Hotel ")
    public abstract void deleteSearchHotel();

    @Query("DELETE FROM SEARCHHOTEL ")
    public abstract void deleteSearchHotelIds();

    //endregion
}
