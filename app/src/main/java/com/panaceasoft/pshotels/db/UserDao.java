package com.panaceasoft.pshotels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.panaceasoft.pshotels.viewobject.User;
import com.panaceasoft.pshotels.viewobject.UserLogin;
import java.util.List;

/**
 * Created by Panacea-Soft on 12/6/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<User> userList);

    @Query("SELECT * FROM User")
    LiveData<List<User>> getAll();

    @Query("SELECT * FROM User WHERE user_id = :user_id")
    LiveData<User> getUserData(String user_id);

    @Query("SELECT * FROM User LIMIT :limit")
    LiveData<List<User>> getWithLimit(String limit);

    @Query("SELECT * FROM User WHERE user_id = :user_id")
    LiveData<User> findById(String user_id);

    @Query("DELETE FROM User WHERE user_id = :user_id")
    void deleteById(String user_id);

    @Query("DELETE FROM User")
    void deleteTable();


    //region User Login Related

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserLogin userLogin);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(UserLogin userLogin);

    @Query("SELECT * FROM UserLogin WHERE user_id = :user_id")
    LiveData<UserLogin> getUserLoginData(String user_id);

    @Query("SELECT * FROM UserLogin")
    LiveData<List<UserLogin>> getUserLoginData();

    @Query("DELETE FROM UserLogin")
    void deleteUserLogin();

    //endregion
}
