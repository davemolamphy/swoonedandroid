package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "hinfo_grp_id")
public class HotelFeatures {

    @SerializedName("hinfo_grp_id")
    @NonNull
    public final String hinfo_grp_id;

    @SerializedName("hinfo_grp_name")
    public final String hinfo_grp_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("hinfo_parent_id")
    public final String hinfo_parent_id;

    @Embedded(prefix = "photo_")
    @SerializedName("default_photo")
    public final Image default_photo;

    @SerializedName("types")
    @Ignore
    public List<HotelFeatureDetail> hinf_types_arr;


    public HotelFeatures(@NonNull String hinfo_grp_id, String hinfo_grp_name, String status, String added_date, String hinfo_parent_id, Image default_photo) {
        this.hinfo_grp_id = hinfo_grp_id;
        this.hinfo_grp_name = hinfo_grp_name;
        this.status = status;
        this.added_date = added_date;
        this.hinfo_parent_id = hinfo_parent_id;
        this.default_photo = default_photo;
    }
}
