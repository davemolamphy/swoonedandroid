package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */


@Entity(primaryKeys = "booking_id")
public class Booking {

    @SerializedName("booking_id")
    @NonNull
    public final String booking_id;

    @SerializedName("user_id")
    public final String user_id;

    @SerializedName("booking_user_name")
    public final String booking_user_name;

    @SerializedName("booking_user_email")
    public final String booking_user_email;

    @SerializedName("booking_user_phone")
    public final String booking_user_phone;

    @SerializedName("hotel_id")
    public final String hotel_id;

    @SerializedName("room_id")
    public final String room_id;

    @SerializedName("booking_adult_count")
    public final String booking_adult_count;

    @SerializedName("booking_kid_count")
    public final String booking_kid_count;

    @SerializedName("booking_start_date")
    public final String booking_start_date;

    @SerializedName("booking_end_date")
    public final String booking_end_date;

    @SerializedName("booking_extra_bed")
    public final String booking_extra_bed;

    @SerializedName("booking_remark")
    public final String booking_remark;

    @SerializedName("booking_status")
    public final String booking_status;

    @SerializedName("added_date")
    public final String added_date;

    @Embedded(prefix = "hotel_")
    @SerializedName("hotel")
    public final Hotel hotel;

    @Embedded(prefix = "room_")
    @SerializedName("room")
    public final Room room;


    public Booking(@NonNull String booking_id, String user_id, String booking_user_name, String booking_user_email, String booking_user_phone, String hotel_id, String room_id, String booking_adult_count, String booking_kid_count, String booking_start_date, String booking_end_date, String booking_extra_bed, String booking_remark, String booking_status, String added_date, Hotel hotel, Room room) {

        this.booking_id = booking_id;
        this.user_id = user_id;
        this.booking_user_name = booking_user_name;
        this.booking_user_email = booking_user_email;
        this.booking_user_phone = booking_user_phone;
        this.hotel_id = hotel_id;
        this.room_id = room_id;
        this.booking_adult_count = booking_adult_count;
        this.booking_kid_count = booking_kid_count;
        this.booking_start_date = booking_start_date;
        this.booking_end_date = booking_end_date;
        this.booking_extra_bed = booking_extra_bed;
        this.booking_remark = booking_remark;
        this.booking_status = booking_status;
        this.added_date = added_date;
        this.hotel = hotel;
        this.room = room;

    }

}
