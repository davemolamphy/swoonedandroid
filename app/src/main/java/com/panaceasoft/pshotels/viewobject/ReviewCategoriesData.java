package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "rvcat_id")
public class ReviewCategoriesData {


    @SerializedName("review_parent_id")
    public final String review_parent_id;

    @SerializedName("rvcat_id")
    @NonNull
    public final String rvcat_id;

    @SerializedName("rvcat_name")
    public final String rvcat_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added")
    public final String added;

    @Embedded(prefix = "rat_")
    @SerializedName("rating")
    public final Rating rating;

    public ReviewCategoriesData(@NonNull String rvcat_id, String rvcat_name, String status, String added,String review_parent_id, Rating rating) {
        this.rvcat_id = rvcat_id;
        this.rvcat_name = rvcat_name;
        this.status = status;
        this.added = added;
        this.review_parent_id = review_parent_id;
        this.rating = rating;
    }
}

