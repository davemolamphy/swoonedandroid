package com.panaceasoft.pshotels.viewobject;

import com.google.gson.annotations.SerializedName;

public class PostRating {

    @SerializedName("rvcat_id")
    public final String rvcat_id;

    @SerializedName("rvrating_rate")
    public final String rvrating_rate;

    public PostRating(String rvcat_id, String rvrating_rate) {
        this.rvcat_id = rvcat_id;
        this.rvrating_rate = rvrating_rate;
    }
}
