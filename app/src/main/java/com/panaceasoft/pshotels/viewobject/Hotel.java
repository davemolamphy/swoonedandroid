package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 3/30/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "hotel_id")
public class Hotel {

    @SerializedName("hotel_id")
    @NonNull
    public final String hotel_id;

    @SerializedName("city_id")
    public final String city_id;

    @SerializedName("hotel_name")
    public final String hotel_name;

    @SerializedName("hotel_desc")
    public final String hotel_desc;

    @SerializedName("hotel_address")
    public final String hotel_address;

    @SerializedName("hotel_lat")
    public final String hotel_lat;

    @SerializedName("hotel_lng")
    public final String hotel_lng;

    @SerializedName("hotel_phone")
    public final String hotel_phone;

    @SerializedName("hotel_email")
    public final String hotel_email;

    @SerializedName("hotel_min_price")
    public final String hotel_min_price;

    @SerializedName("hotel_max_price")
    public final String hotel_max_price;

    @SerializedName("hotel_star_rating")
    public final String hotel_star_rating;

    @SerializedName("hotel_check_in")
    public final String hotel_check_in;

    @SerializedName("hotel_check_out")
    public final String hotel_check_out;

    @SerializedName("is_recommended")
    public final String is_recommended;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("added_date_str")
    public final String added_date_str;

    @Embedded(prefix = "photo_")
    @SerializedName("default_photo")
    public final Image default_photo;

    @SerializedName("currency_symbol")
    public final String currency_symbol;

    @SerializedName("currency_short_form")
    public final String currency_short_form;

    @Embedded(prefix = "promotion_")
    @SerializedName("promotion")
    public final Promotion promotion;

    @Embedded(prefix = "rating_")
    @SerializedName("rating")
    public final Rating rating;

    @SerializedName("touch_count")
    public final String touch_count;

    @SerializedName("image_count")
    public final String image_count;

    @SerializedName("is_user_favourited")
    public String is_user_favourited;

    public Hotel(@NonNull String hotel_id, String city_id, String hotel_name, String hotel_desc, String hotel_address, String hotel_lat, String hotel_lng, String hotel_phone, String hotel_email, String hotel_min_price, String hotel_max_price, String hotel_star_rating, String hotel_check_in, String hotel_check_out, String is_recommended, String status, String added_date, String added_date_str, Image default_photo, String currency_symbol, String currency_short_form, Promotion promotion, Rating rating, String touch_count, String image_count, String is_user_favourited) {
        this.hotel_id = hotel_id;
        this.city_id = city_id;
        this.hotel_name = hotel_name;
        this.hotel_desc = hotel_desc;
        this.hotel_address = hotel_address;
        this.hotel_lat = hotel_lat;
        this.hotel_lng = hotel_lng;
        this.hotel_phone = hotel_phone;
        this.hotel_email = hotel_email;
        this.hotel_min_price = hotel_min_price;
        this.hotel_max_price = hotel_max_price;
        this.hotel_star_rating = hotel_star_rating;
        this.hotel_check_in = hotel_check_in;
        this.hotel_check_out = hotel_check_out;
        this.is_recommended = is_recommended;
        this.status = status;
        this.added_date = added_date;
        this.added_date_str = added_date_str;
        this.default_photo = default_photo;
        this.currency_symbol = currency_symbol;
        this.currency_short_form = currency_short_form;
        this.promotion = promotion;
        this.rating = rating;
        this.touch_count = touch_count;
        this.image_count = image_count;
        this.is_user_favourited = is_user_favourited;
    }
}
