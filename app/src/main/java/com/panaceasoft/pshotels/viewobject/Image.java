package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 11/25/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "img_id")
public class Image {

    @SerializedName("img_id")
    @NonNull
    public final String img_id;

    @SerializedName("img_parent_id")
    public final String img_parent_id;

    @SerializedName("img_type")
    public final String img_type;

    @SerializedName("img_path")
    public final String img_path;

    @SerializedName("Img_width")
    public final String Img_width;

    @SerializedName("img_height")
    public final String img_height;

    @SerializedName("img_desc")
    public final String img_desc;

    public Image(@NonNull String img_id, String img_parent_id, String img_type, String img_path, String Img_width, String img_height, String img_desc) {
        this.img_id = img_id;
        this.img_parent_id = img_parent_id;
        this.img_type = img_type;
        this.img_path = img_path;
        this.Img_width = Img_width;
        this.img_height = img_height;
        this.img_desc = img_desc;
    }
}
