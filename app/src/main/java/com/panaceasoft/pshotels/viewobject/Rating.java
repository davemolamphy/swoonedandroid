package com.panaceasoft.pshotels.viewobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 3/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class Rating {

    @SerializedName("final_rating")
    public final String final_rating;

    @SerializedName("rating_text")
    public final String rating_text;

    @SerializedName("review_count")
    public final String review_count;

    public Rating(String final_rating, String rating_text, String review_count) {
        this.final_rating = final_rating;
        this.rating_text = rating_text;
        this.review_count = review_count;
    }

}
