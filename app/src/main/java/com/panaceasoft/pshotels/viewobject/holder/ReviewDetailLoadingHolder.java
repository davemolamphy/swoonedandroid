package com.panaceasoft.pshotels.viewobject.holder;

public class ReviewDetailLoadingHolder extends LoadingHolder {

    public final String apiKey;
    public final String loginUserId;
    public final String roomId;
    public final String hotelId;

    public ReviewDetailLoadingHolder(String limit, String offset, String apiKey, String loginUserId, String roomId, String hotelId) {
        super(limit, offset);
        this.apiKey = apiKey;
        this.loginUserId = loginUserId;
        this.roomId = roomId;
        this.hotelId = hotelId;
    }
}
