package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "promo_id")
public class Promotion {

    @SerializedName("promo_id")
    @NonNull
    public final String promo_id;

    @SerializedName("promo_name")
    public final String promo_name;

    @SerializedName("promo_desc")
    public final String promo_desc;

    @SerializedName("promo_percent")
    public final String promo_percent;

    @SerializedName("promo_start_time")
    public final String promo_start_time;


    @SerializedName("promo_end_time")
    public final String promo_end_time;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    public Promotion(@NonNull String promo_id, String promo_name, String promo_desc, String promo_percent, String promo_start_time, String promo_end_time, String status, String added_date) {
        this.promo_id = promo_id;
        this.promo_name = promo_name;
        this.promo_desc = promo_desc;
        this.promo_percent = promo_percent;
        this.promo_start_time = promo_start_time;
        this.promo_end_time = promo_end_time;
        this.status = status;
        this.added_date = added_date;
    }
}