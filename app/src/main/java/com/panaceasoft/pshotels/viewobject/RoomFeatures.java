package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "rinfo_grp_id")
public class RoomFeatures {

    @SerializedName("rinfo_grp_id")
    @NonNull
    public final String rinfo_grp_id;

    @SerializedName("rinfo_grp_name")
    public final String rinfo_grp_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("rinfo_parent_id")
    public final String rinfo_parent_id;

    @Embedded(prefix = "photo_")
    @SerializedName("default_photo")
    public final Image default_photo;

    @SerializedName("types")
    @Ignore
    public List<RoomFeatureDetail> rinf_types_arr;


    public RoomFeatures(@NonNull String rinfo_grp_id, String rinfo_grp_name, String status, String added_date, String rinfo_parent_id, Image default_photo) {
        this.rinfo_grp_id = rinfo_grp_id;
        this.rinfo_grp_name = rinfo_grp_name;
        this.status = status;
        this.added_date = added_date;
        this.rinfo_parent_id = rinfo_parent_id;
        this.default_photo = default_photo;
    }
}

