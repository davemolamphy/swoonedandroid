package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "country_id")
public class Country {

    @SerializedName("country_id")
    @NonNull
    public final String country_id;

    @SerializedName("country_name")
    public final String country_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    public Country(@NonNull String country_id, String country_name, String status, String added_date) {
        this.country_id = country_id;
        this.country_name = country_name;
        this.status = status;
        this.added_date = added_date;
    }
}