package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "hotel_id")
public class SearchHotel {

    @NonNull
    public final String hotel_id;

    public SearchHotel(@NonNull String hotel_id) {
        this.hotel_id = hotel_id;
    }
}
