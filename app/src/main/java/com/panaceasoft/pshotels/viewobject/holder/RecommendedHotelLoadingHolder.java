package com.panaceasoft.pshotels.viewobject.holder;

public class RecommendedHotelLoadingHolder extends LoadingHolder {

    public String loginUserId;

    public RecommendedHotelLoadingHolder( String loginUserId, String limit, String offset) {
        super(limit, offset);
        this.loginUserId = loginUserId;
    }
}
