package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(primaryKeys = "hotel_id")
public class RecommendedHotel {

    @NonNull
    public final String hotel_id;

    public RecommendedHotel(@NonNull String hotel_id) {
        this.hotel_id = hotel_id;
    }
}