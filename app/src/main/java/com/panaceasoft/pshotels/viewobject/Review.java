package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Panacea-Soft on 3/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "review_parent_id")
public class Review {

    @SerializedName("review_parent_id")
    @NonNull
    public final String review_parent_id;

    @Embedded(prefix = "rat_")
    @SerializedName("rating")
    public final Rating rating;

    @Ignore
    @SerializedName("review_categories")
    public List<ReviewCategoriesData> review_categories;

    public Review(@NonNull String review_parent_id, Rating rating) {
        this.review_parent_id = review_parent_id;
        this.rating = rating;
    }
}


