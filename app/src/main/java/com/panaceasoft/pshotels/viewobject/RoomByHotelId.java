package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(primaryKeys = "room_id")
public class RoomByHotelId {

    @NonNull
    public final String room_id;

    public RoomByHotelId(@NonNull String room_id) {
        this.room_id = room_id;
    }
}
