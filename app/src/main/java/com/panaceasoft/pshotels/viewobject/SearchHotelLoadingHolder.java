package com.panaceasoft.pshotels.viewobject;

import com.panaceasoft.pshotels.viewobject.holder.LoadingHolder;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class SearchHotelLoadingHolder extends LoadingHolder {

    public final String apiKey;
    public final String loginUserId;
    public final String cityId;
    public final String hotelName;
    public final String hotelStarRating;
    public final String hotelMinPrice;
    public final String hotelMaxPrice;
    public final String filterByInfoType;
    public final String minUserRating;

    public SearchHotelLoadingHolder(String limit, String offset, String apiKey, String loginUserId, String cityId, String hotelName, String hotelStarRating, String hotelMinPrice, String hotelMaxPrice, String filterByInfoType, String minUserRating) {
        super(limit, offset);
        this.apiKey = apiKey;
        this.loginUserId = loginUserId;
        this.cityId = cityId;
        this.hotelName = hotelName;
        this.hotelStarRating = hotelStarRating;
        this.hotelMinPrice = hotelMinPrice;
        this.hotelMaxPrice = hotelMaxPrice;
        this.filterByInfoType = filterByInfoType;
        this.minUserRating = minUserRating;
    }
}
