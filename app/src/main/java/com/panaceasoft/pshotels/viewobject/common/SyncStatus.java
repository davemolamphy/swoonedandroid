package com.panaceasoft.pshotels.viewobject.common;

/**
 * Created by Panacea-Soft on 12/12/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

public class SyncStatus {
    public static final String SERVER_SELECTED = "true";
    public static final String SERVER_NOT_SELECTED = "false";
    public static final String LOCAL_SELECTED = "local_selected";
    public static final String LOCAL_NOT_SELECTED  = "local_not_selected";
}
