package com.panaceasoft.pshotels.viewobject.holder;

public class RoomByHotelIdLoadingHolder extends LoadingHolder {

    public final String loginUserId;
    public final String hotelId;
    public final String apiKey;

    public RoomByHotelIdLoadingHolder(String limit, String offset, String loginUserId, String hotelId, String apiKey) {
        super(limit, offset);
        this.loginUserId = loginUserId;
        this.hotelId = hotelId;
        this.apiKey = apiKey;
    }
}
