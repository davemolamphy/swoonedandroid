package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "city_id")
public class City {

    @SerializedName("city_id")
    @NonNull
    public final String city_id;

    @SerializedName("country_id")
    public final String country_id;

    @SerializedName("city_name")
    public final String city_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("added_date_str")
    public final String added_date_str;

    @Embedded(prefix = "country_")
    @SerializedName("country")
    public final Country country;

    public City(@NonNull String city_id, String country_id, String city_name, String status, String added_date, String added_date_str, Country country) {
        this.city_id = city_id;
        this.country_id = country_id;
        this.city_name = city_name;
        this.status = status;
        this.added_date = added_date;
        this.added_date_str = added_date_str;
        this.country = country;
    }
}