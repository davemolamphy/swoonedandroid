package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Entity(primaryKeys = "review_id")
public class ReviewDetail {

    @SerializedName("review_id")
    @NonNull
    public final String review_id;

    @SerializedName("room_id")
    public final String room_id;

    @SerializedName("user_id")
    public final String user_id;

    @SerializedName("review_desc")
    public final String review_desc;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("added_date_str")
    public final String added_date_str;

    @SerializedName("room_name")
    public final String room_name;

    @Embedded(prefix = "user_")
    @SerializedName("user")
    public final User user;

    @Embedded(prefix = "rat_")
    @SerializedName("rating")
    public final Rating rating;

    @SerializedName("review_parent_id")
    public final String review_parent_id;

    public ReviewDetail(@NonNull String review_id, String room_id, String user_id, String review_desc, String status, String added_date, String added_date_str, String room_name, User user, Rating rating, String review_parent_id) {
        this.review_id = review_id;
        this.room_id = room_id;
        this.user_id = user_id;
        this.review_desc = review_desc;
        this.status = status;
        this.added_date = added_date;
        this.added_date_str = added_date_str;
        this.room_name = room_name;
        this.user = user;
        this.rating = rating;
        this.review_parent_id = review_parent_id;
    }
}
