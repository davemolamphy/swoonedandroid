package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(primaryKeys = "room_id")
public class Room {

    @SerializedName("room_id")
    @NonNull
    public final String room_id;

    @SerializedName("hotel_id")
    public final String hotel_id;

    @SerializedName("room_name")
    public final String room_name;

    @SerializedName("room_desc")
    public final String room_desc;

    @SerializedName("room_size")
    public final String room_size;

    @SerializedName("room_price")
    public final String room_price;

    @SerializedName("room_no_of_beds")
    public final String room_no_of_beds;

    @SerializedName("room_adult_limit")
    public final String room_adult_limit;

    @SerializedName("room_kid_limit")
    public final String room_kid_limit;

    @SerializedName("room_extra_bed_price")
    public final String room_extra_bed_price;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("added_date_str")
    public final String added_date_str;

    @SerializedName("touch_count")
    public final String touch_count;

    @SerializedName("image_count")
    public final String image_count;

    @SerializedName("currency_symbol")
    public final String currency_symbol;

    @SerializedName("currency_short_form")
    public final String currency_short_form;

    @Embedded(prefix = "photo_")
    @SerializedName("default_photo")
    public final Image default_photo;

    @Embedded(prefix = "rating_")
    @SerializedName("rating")
    public final Rating rating;

    @Embedded(prefix = "promotion_")
    @SerializedName("promotion")
    public final Promotion promotion;

    @SerializedName("is_available")
    public final String is_available;

    public Room(@NonNull String room_id, String hotel_id, String room_name, String room_desc, String room_size, String room_price, String room_no_of_beds, String room_adult_limit, String room_kid_limit, String room_extra_bed_price, String status, String added_date, String added_date_str, String touch_count, String image_count, String currency_symbol, String currency_short_form, Image default_photo, Rating rating, Promotion promotion, String is_available) {
        this.room_id = room_id;
        this.hotel_id = hotel_id;
        this.room_name = room_name;
        this.room_desc = room_desc;
        this.room_size = room_size;
        this.room_price = room_price;
        this.room_no_of_beds = room_no_of_beds;
        this.room_adult_limit = room_adult_limit;
        this.room_kid_limit = room_kid_limit;
        this.room_extra_bed_price = room_extra_bed_price;
        this.status = status;
        this.added_date = added_date;
        this.added_date_str = added_date_str;
        this.touch_count = touch_count;
        this.image_count = image_count;
        this.currency_symbol = currency_symbol;
        this.currency_short_form = currency_short_form;
        this.default_photo = default_photo;
        this.rating = rating;
        this.promotion = promotion;
        this.is_available = is_available;
    }
}
