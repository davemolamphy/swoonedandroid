package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "hinfo_typ_id")
public class HotelFeatureDetail {

    @SerializedName("hinfo_typ_id")
    @NonNull
    public final String hinfo_typ_id;


    @SerializedName("hinfo_grp_id")
    public final String hinfo_grp_id;

    @SerializedName("hinfo_typ_name")
    public final String hinfo_typ_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("hinfo_parent_id")
    public final String hinfo_parent_id;


    @Ignore
    public List<HotelFeatureDetail> hotelFeatureDetailList;

    public HotelFeatureDetail(@NonNull String hinfo_typ_id, String hinfo_grp_id, String hinfo_typ_name, String status, String added_date, String hinfo_parent_id) {
        this.hinfo_typ_id = hinfo_typ_id;
        this.hinfo_grp_id = hinfo_grp_id;
        this.hinfo_typ_name = hinfo_typ_name;
        this.status = status;
        this.added_date = added_date;
        this.hinfo_parent_id = hinfo_parent_id;
    }
}
