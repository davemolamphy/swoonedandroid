package com.panaceasoft.pshotels.viewobject.holder;

public class FavouriteHotelLoadingHolder extends LoadingHolder {

    public String loginUserId;

    public FavouriteHotelLoadingHolder(String limit, String offset, String loginUserId) {
        super(limit, offset);
        this.loginUserId = loginUserId;
    }
}
