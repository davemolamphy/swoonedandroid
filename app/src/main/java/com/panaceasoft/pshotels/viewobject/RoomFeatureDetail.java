package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "rinfo_typ_id")
public class RoomFeatureDetail {

    @SerializedName("rinfo_typ_id")
    @NonNull
    public final String rinfo_typ_id;

    @SerializedName("rinfo_grp_id")
    public final String rinfo_grp_id;

    @SerializedName("rinfo_typ_name")
    public final String rinfo_typ_name;

    @SerializedName("status")
    public final String status;

    @SerializedName("added_date")
    public final String added_date;

    @SerializedName("rinfo_parent_id")
    public final String rinfo_parent_id;

    public RoomFeatureDetail(@NonNull String rinfo_typ_id, String rinfo_grp_id, String rinfo_typ_name, String status, String added_date, String rinfo_parent_id) {
        this.rinfo_typ_id = rinfo_typ_id;
        this.rinfo_grp_id = rinfo_grp_id;
        this.rinfo_typ_name = rinfo_typ_name;
        this.status = status;
        this.added_date = added_date;
        this.rinfo_parent_id = rinfo_parent_id;
    }
}
