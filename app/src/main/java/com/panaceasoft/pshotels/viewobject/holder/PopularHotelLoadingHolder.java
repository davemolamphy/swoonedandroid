package com.panaceasoft.pshotels.viewobject.holder;

public class PopularHotelLoadingHolder extends LoadingHolder {

    public String loginUserId;

    public PopularHotelLoadingHolder(String limit, String offset, String loginUserId) {
        super(limit, offset);
        this.loginUserId = loginUserId;
    }
}