package com.panaceasoft.pshotels.viewobject.holder;

public class PromotionHotelLoadingHolder extends LoadingHolder {

    public String loginUserId;

    public PromotionHotelLoadingHolder(String limit, String offset, String loginUserId) {
        super(limit, offset);
        this.loginUserId = loginUserId;
    }
}
