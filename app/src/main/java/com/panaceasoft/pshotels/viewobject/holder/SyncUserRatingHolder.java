package com.panaceasoft.pshotels.viewobject.holder;

import com.panaceasoft.pshotels.viewobject.PostRating;

import java.util.List;

public class SyncUserRatingHolder {

    public List<PostRating> ratings;

    public SyncUserRatingHolder(List<PostRating> ratings) {
        this.ratings = ratings;
    }
}
