package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Panacea-Soft on 4/4/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class ReviewSummaryWithCategoryData {


    @Embedded
    public Review review;

    @SerializedName("review_categories")
    @Relation(parentColumn = "review_parent_id", entityColumn = "review_parent_id", entity = ReviewCategoriesData.class)
    public List<ReviewCategoriesData> review_categories;
}
