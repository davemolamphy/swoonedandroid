package com.panaceasoft.pshotels.viewobject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Panacea-Soft on 4/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Entity(primaryKeys = "max_price")
public class Price {

    @SerializedName("max_price")
    @NonNull
    public final String max_price;

    @SerializedName("currency_symbol")
    public final String currency_symbol;

    @SerializedName("currency_short_form")
    public final String currency_short_form;

    public Price(@NonNull String max_price, String currency_symbol, String currency_short_form) {

        this.max_price = max_price;
        this.currency_symbol = currency_symbol;
        this.currency_short_form = currency_short_form;
    }
}
