package com.panaceasoft.pshotels.repository.booking.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class PostBookingTask implements Runnable {

    //region Variables
    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    //api service and db
    private final PSApiService service;
    private final PSCoreDb db;

    //booking data
    private final String apiKey;
    private final String loginUserId;
    private final String userName;
    private final String userEmail;
    private final String userPhone;
    private final String hotelId;
    private final String roomId;
    private final String adultCount;
    private final String kidCount;
    private final String startDate;
    private final String endDate;
    private final String extraBed;
    private final String remark;


    //region Override Methods

    public PostBookingTask(PSApiService service, PSCoreDb db, String apiKey, String loginUserId, String userName, String userEmail, String userPhone, String hotelId, String roomId, String adultCount, String kidCount, String startDate, String endDate, String extraBed, String remark) {
        this.service = service;
        this.db = db;
        this.apiKey = apiKey;
        this.loginUserId = loginUserId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.hotelId = hotelId;
        this.roomId = roomId;
        this.adultCount = adultCount;
        this.kidCount = kidCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.extraBed = extraBed;
        this.remark = remark;
    }

    @Override
    public void run() {
        try {

            // Call the API Service
            Response<Booking> response = service.rawPostBooking(
                    apiKey,
                    loginUserId,
                    userName,
                    userEmail,
                    userPhone,
                    hotelId,
                    roomId,
                    adultCount,
                    kidCount,
                    startDate,
                    endDate,
                    extraBed,
                    remark
            ).execute();

            // Wrap with APIResponse Class
            ApiResponse<Booking> apiResponse = new ApiResponse<>(response);
            Utils.psLog("apiResponse " + apiResponse);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                statusLiveData.postValue(Resource.success(true));

            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));

            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion

    //region public methods

    /**
     * This function will return Status of Process
     *
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}

