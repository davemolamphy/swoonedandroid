package com.panaceasoft.pshotels.repository.hotel.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.common.SyncStatus;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 4/25/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class FavouriteHotelTask implements Runnable {


    //region Variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    private final PSApiService service;
    private final PSCoreDb db;
    private final String loginUserId;
    private final String hotelId;

    //endregion


    //region Constructor

    public FavouriteHotelTask(PSApiService service, PSCoreDb db, String loginUserId, String hotelId) {
        this.service = service;
        this.db = db;
        this.loginUserId = loginUserId;
        this.hotelId = hotelId;
    }

    //endregion


    //region Override Methods

    @Override
    public void run() {
        try {

            // Save to local first
            Hotel hotel = db.hotelDao().getDirectNewsById(hotelId);

            if(hotel == null) {
                return;
            }

            if(hotel.is_user_favourited.equals(SyncStatus.SERVER_SELECTED) ||
                    hotel.is_user_favourited.equals(SyncStatus.LOCAL_SELECTED)) {
                hotel.is_user_favourited = SyncStatus.LOCAL_NOT_SELECTED;
            }else {
                hotel.is_user_favourited = SyncStatus.LOCAL_SELECTED;
            }

            try {
                db.beginTransaction();

                db.hotelDao().update(hotel);
                db.setTransactionSuccessful();

            } finally {
                db.endTransaction();
            }


            // Call the API Service
            Response<Hotel> response = service.rawPostFavHotel(Config.API_KEY, hotelId, loginUserId).execute();

            // Wrap with APIResponse Class
            ApiResponse<Hotel> apiResponse = new ApiResponse<>(response);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                try {
                    db.beginTransaction();
                    db.hotelDao().update(apiResponse.body);
                    db.setTransactionSuccessful();

                } finally {
                    db.endTransaction();
                }

                statusLiveData.postValue(Resource.success(true));

            } else {

                // Transaction Fail to server
                // So, Update back the original one.
                if(hotel.is_user_favourited.equals(SyncStatus.LOCAL_SELECTED)) {
                    hotel.is_user_favourited = SyncStatus.SERVER_NOT_SELECTED;
                }else {
                    hotel.is_user_favourited = SyncStatus.SERVER_SELECTED;
                }

                try {
                    db.beginTransaction();

                    db.hotelDao().update(hotel);
                    db.setTransactionSuccessful();

                } finally {
                    db.endTransaction();
                }


                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion


    //region public methods

    /**
     * This function will return Status of Process
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion


}