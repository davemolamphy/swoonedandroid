package com.panaceasoft.pshotels.repository.room;


import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.db.RoomDao;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.repository.hotel.task.TouchCountTask;
import com.panaceasoft.pshotels.repository.room.task.FetchNextPageRoomByHotelIdTask;
import com.panaceasoft.pshotels.repository.room.task.RoomTouchCountTask;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.RoomByHotelId;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class RoomRepository extends PSRepository {

    // region variables

    private final RoomDao roomDao;

    // end region

    @Inject
    public RoomRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, RoomDao roomDao) {
        super(psApiService, appExecutors, db);
        this.roomDao = roomDao;
    }

    // region get room by room id list

    public LiveData<Resource<List<Room>>> getRoomByHotelIdList(RoomByHotelIdLoadingHolder holder) {
        return new NetworkBoundResource<List<Room>, List<Room>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Room> itemList) {
                Utils.psLog("SaveCallResult of Room By Hotel Id List");

                db.beginTransaction();
                try {
                    roomDao.deleteRoomByHotelIds();

                    for(Room room: itemList){
                        roomDao.insert(new RoomByHotelId(room.room_id));
                    }

                    roomDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting getRoomByHotelIdList.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Room> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Room>> loadFromDb() {
                Utils.psLog("Load Room By Hotel Id From DB.");

                return roomDao.getAllRoomByHotelId();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Room>>> createCall() {
                Utils.psLog("Call getRoomByHotelIdList WebService");
                return psApiService.getRoomListByHotelId(
                        holder.apiKey,
                        Utils.checkUserId( holder.loginUserId ),
                        holder.hotelId,
                        String.valueOf(Config.ROOM_BY_HOTEL_COUNT),
                        holder.offset
                );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of getRoomByHotelIdList WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<List<Room>>> getAllRoomByHotelIdList(String apiKey, String hotelId) {
        return new NetworkBoundResource<List<Room>, List<Room>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Room> itemList) {
                Utils.psLog("SaveCallResult of Room By Hotel Id List");

                db.beginTransaction();
                try {
                    roomDao.deleteRoomByHotelIds();

                    for(Room room: itemList){
                        roomDao.insert(new RoomByHotelId(room.room_id));
                    }

                    roomDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting getAllRoomByHotelIdList.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Room> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Room>> loadFromDb() {
                Utils.psLog("Load Room By Hotel Id From DB.");

                return roomDao.getAllRoomByHotelId();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Room>>> createCall() {
                Utils.psLog("Call getAllRoomByHotelIdList WebService");
                return psApiService.getAllRoomListByHotelId(
                        apiKey,
                        hotelId
                );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getAllRoomByHotelIdList)" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPageRoomByHotelIds(RoomByHotelIdLoadingHolder loadingHolder) {
        FetchNextPageRoomByHotelIdTask fetchNextPageRoomByHotelIdTask = new FetchNextPageRoomByHotelIdTask(
                psApiService, db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageRoomByHotelIdTask);

        return fetchNextPageRoomByHotelIdTask.getStatusLiveData();
    }

    public LiveData<Resource<Room>> getRoomByRoomId(String apiKey, String loginUserId, String roomId ) {

        String functionKey = "key"+roomId;

        return new NetworkBoundResource<Room, List<Room>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Room> item) {

                Utils.psLog("SaveCallResult of getRoomByRoomId.");

                db.beginTransaction();

                try {

                    roomDao.insertAll(item);

                    db.setTransactionSuccessful();
                } catch (Exception e) {

                    Utils.psErrorLog("Error in doing transaction of getRoomByRoomId.", e);
                } finally {

                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable Room data) {

                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<Room> loadFromDb() {

                Utils.psLog("Load Room From DB By RoomID");
                return roomDao.getById(roomId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Room>>> createCall() {

                Utils.psLog("Call API Service to getRoomByRoomId.");
                return psApiService.getRoomById( apiKey, Utils.checkUserId( loginUserId ), roomId );
            }

            @Override
            protected void onFetchFailed(String message) {

                rateLimiter.reset(functionKey);
                Utils.psLog("Fetch Failed in getRoomByRoomId.");
            }

        }.asLiveData();
    }

    /**
     * Function to call background task to submit Open Count
     *
     * @param loginUserId Current Login User Id
     * @param roomId roomId
     * @return Status of Process
     */
    public LiveData<Resource<Boolean>> doPostTouchCount(String loginUserId, String roomId) {
        RoomTouchCountTask touchCountTask = new RoomTouchCountTask(
                psApiService,  db, loginUserId, roomId);

        Utils.psLog("Do Touch : repository.");
        appExecutors.networkIO().execute(touchCountTask);

        return touchCountTask.getStatusLiveData();
    }

    // end region
}
