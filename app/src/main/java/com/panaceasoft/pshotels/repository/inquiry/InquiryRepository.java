package com.panaceasoft.pshotels.repository.inquiry;

import android.arch.lifecycle.LiveData;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.repository.inquiry.task.PostInquiryTask;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import javax.inject.Inject;

public class InquiryRepository extends PSRepository {

    @Inject
    InquiryRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db) {
        super(psApiService, appExecutors, db);

    }

    public LiveData<Resource<Boolean>> postInquiry(String apiKey, String hotelId, String roomId, String userId, String inqName, String inqDesc, String contactName, String contactEmail, String contactPhone ) {

        PostInquiryTask postInquiryTask = new PostInquiryTask(
                psApiService,
                db,
                apiKey,
                hotelId,
                roomId,
                userId,
                inqName,
                inqDesc,
                contactName,
                contactEmail,
                contactPhone
        );

        appExecutors.networkIO().execute(postInquiryTask);

        return postInquiryTask.getStatusLiveData();
    }
}

