package com.panaceasoft.pshotels.repository.hotel.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.viewobject.ApiStatus;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 4/26/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class TouchCountTask implements Runnable {


    //region Variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    private final PSApiService service;
    private final PSCoreDb db;
    private final String loginUserId;
    private final String hotelId;

    //endregion


    //region Constructor

    public TouchCountTask(PSApiService service, PSCoreDb db, String loginUserId, String hotelId) {
        this.service = service;
        this.db = db;
        this.loginUserId = loginUserId;
        this.hotelId = hotelId;
    }

    //endregion


    //region Override Methods

    @Override
    public void run() {
        try {

            // Call the API Service
            Response<ApiStatus> response = service.rawPostTouchCount(Config.API_KEY, hotelId, loginUserId).execute();

            // Wrap with APIResponse Class
            ApiResponse<ApiStatus> apiResponse = new ApiResponse<>(response);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                statusLiveData.postValue(Resource.success(true));
            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion


    //region public methods

    /**
     * This function will return Status of Process
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion

}