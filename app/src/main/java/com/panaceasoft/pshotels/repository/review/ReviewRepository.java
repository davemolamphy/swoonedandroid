package com.panaceasoft.pshotels.repository.review;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.db.ReviewDao;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.repository.review.task.FetchNextPageHotelReviewDetailTask;
import com.panaceasoft.pshotels.repository.review.task.FetchNextPageRoomReviewDetailTask;
import com.panaceasoft.pshotels.repository.review.task.PostReviewTask;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Review;
import com.panaceasoft.pshotels.viewobject.ReviewCategoriesData;
import com.panaceasoft.pshotels.viewobject.ReviewCategory;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.ReviewSummaryWithCategoryData;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;
import com.panaceasoft.pshotels.viewobject.holder.SyncUserRatingHolder;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 3/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Singleton
public class ReviewRepository extends PSRepository {

    //region Variables

    private final ReviewDao reviewDao;

    //endregion


    //region Constructor

    @Inject
    public ReviewRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, ReviewDao reviewDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside ReviewRepository");

        this.reviewDao = reviewDao;
    }

    //endregion


    //region Functions for ViewModel

    /**
     * Function to get hotel reviews summary
     *
     * @param apiKey APIKey to access web services
     * @param hotelId Hotel Id
     * @return List of Hotel Info
     */
    public LiveData<Resource<ReviewSummaryWithCategoryData>> getHotelReviewSummary(String apiKey, String hotelId) {

        return new NetworkBoundResource<ReviewSummaryWithCategoryData, Review>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull Review itemList) {
                Utils.psLog("SaveCallResult of getHotelReviewSummary.");

                db.beginTransaction();

                try {

                    List<ReviewCategoriesData> roomReviewCategories = new ArrayList<>();

                    roomReviewCategories.addAll(itemList.review_categories);

                    reviewDao.insert(itemList);

                    reviewDao.insertAllReviewCategory(roomReviewCategories);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getHotelReviewSummary.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable ReviewSummaryWithCategoryData data) {

                // always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<ReviewSummaryWithCategoryData> loadFromDb() {
                Utils.psLog("Load getHotelReviewSummary From Db");

                return reviewDao.getReviewDataWithParentId(hotelId);

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<Review>> createCall() {
                Utils.psLog("Call API Service to getHotelReviewSummary.");

                return psApiService.getHotelReviewSummary(apiKey, hotelId);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getHotelReviewSummary) : " + message);
            }

        }.asLiveData();
    }


    /**
     * Function to get hotel reviews detail
     *
     * @param apiKey APIKey to access web services
     * @param hotelId Hotel Id
     * @param offset offset
     * @return List of Hotel Info
     */
    public LiveData<Resource<List<ReviewDetail>>> getHotelReviewDetail(String apiKey, String hotelId, String offset) {

        return new NetworkBoundResource<List<ReviewDetail>, List<ReviewDetail>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<ReviewDetail> itemList) {
                Utils.psLog("SaveCallResult of getHotelReviewDetail.");

                db.beginTransaction();

                try {

                    reviewDao.deleteReviewDetailWithParentId(hotelId);

                    reviewDao.insertAllReviewDetail(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getHotelReviewDetail.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ReviewDetail> data) {

                // always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<ReviewDetail>> loadFromDb() {
                Utils.psLog("Load getHotelReviewDetail From Db");

                return reviewDao.getReviewDetailDataWithParentId(hotelId);

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<ReviewDetail>>> createCall() {
                Utils.psLog("Call API Service to getHotelReviewDetail.");

                return psApiService.getHotelReviewDetail(apiKey, hotelId, String.valueOf(Config.REVIEW_COUNT), offset);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getRecentNewsList) : " + message);
            }

        }.asLiveData();
    }


    //endregion

    /**
     * Function to get room reviews summary
     *
     * @param apiKey APIKey to access web services
     * @param roomId Room Id
     * @return List of Room Info
     */
    public LiveData<Resource<ReviewSummaryWithCategoryData>> getRoomReviewSummary(String apiKey, String roomId) {

        return new NetworkBoundResource<ReviewSummaryWithCategoryData, Review>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull Review itemList) {
                Utils.psLog("SaveCallResult of getRoomReviewSummary.");

                db.beginTransaction();

                try {

                    List<ReviewCategoriesData> roomReviewCategories = new ArrayList<>();

                    roomReviewCategories.addAll(itemList.review_categories);

                    reviewDao.insert(itemList);

                    reviewDao.insertAllReviewCategory(roomReviewCategories);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getRoomReviewSummary.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable ReviewSummaryWithCategoryData data) {

                // always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<ReviewSummaryWithCategoryData> loadFromDb() {
                Utils.psLog("Load getRoomReviewSummary From Db");

                return reviewDao.getReviewDataWithParentId(roomId);

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<Review>> createCall() {
                Utils.psLog("Call API Service to getRoomReviewSummary.");

                return psApiService.getRoomReviewSummary(apiKey, roomId);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getRoomReviewSummary) : " + message);
            }

        }.asLiveData();
    }


    /**
     * Function to get room reviews detail
     *
     * @param apiKey APIKey to access web services
     * @param roomId Room Id
     * @param offset offset
     * @return List of Room Info
     */
    public LiveData<Resource<List<ReviewDetail>>> getRoomReviewDetail(String apiKey, String roomId, String offset) {

        return new NetworkBoundResource<List<ReviewDetail>, List<ReviewDetail>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<ReviewDetail> itemList) {
                Utils.psLog("SaveCallResult of getRoomReviewDetail.");

                db.beginTransaction();

                try {

                    reviewDao.deleteReviewDetailWithParentId(roomId);

                    reviewDao.insertAllReviewDetail(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getRoomReviewDetail.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ReviewDetail> data) {

                // always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<ReviewDetail>> loadFromDb() {
                Utils.psLog("Load getRoomReviewDetail From Db");

                return reviewDao.getReviewDetailDataWithParentId(roomId);

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<ReviewDetail>>> createCall() {
                Utils.psLog("Call API Service to getRoomReviewDetail.");

                return psApiService.getRoomReviewDetail(apiKey, roomId, String.valueOf(Config.REVIEW_COUNT), offset);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getRoomReviewDetail) : " + message);
            }

        }.asLiveData();
    }


    //endregion

    public LiveData<Resource<Boolean>> getNextPageRoomReviews(ReviewDetailLoadingHolder loadingHolder) {
        FetchNextPageRoomReviewDetailTask fetchNextPageReviewTask = new FetchNextPageRoomReviewDetailTask(
                psApiService,  db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageReviewTask);

        return fetchNextPageReviewTask.getStatusLiveData();

    }

    public LiveData<Resource<Boolean>> getNextPageHotelReviews(ReviewDetailLoadingHolder loadingHolder) {
        FetchNextPageHotelReviewDetailTask fetchNextPageReviewTask = new FetchNextPageHotelReviewDetailTask(
                psApiService,  db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageReviewTask);

        return fetchNextPageReviewTask.getStatusLiveData();

    }

    //region post review
    public LiveData<Resource<Boolean>> postReview(String apiKey, String roomId, String userId, String reviewDesc, SyncUserRatingHolder syncUserRatingHolder) {

        PostReviewTask postReviewTask = new PostReviewTask(
                psApiService,
                db,
                apiKey,
                roomId,
                userId,
                reviewDesc,
                syncUserRatingHolder
        );

        appExecutors.networkIO().execute(postReviewTask);

        return postReviewTask.getStatusLiveData();

    }
    //endregion

    //region get all review category
    public LiveData<Resource<List<ReviewCategory>>> getAllReviewCategory(String apiKey) {

        return new NetworkBoundResource<List<ReviewCategory>, List<ReviewCategory>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<ReviewCategory> itemList) {
                Utils.psLog("SaveCallResult of getAllReviewCategory.");

                db.beginTransaction();

                try {

                    reviewDao.deleteAllReviewCategory();

                    reviewDao.insertReviewCategoryList(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {

                    Utils.psErrorLog("Error in doing transaction of getAllReviewCategory.", e);
                } finally {

                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ReviewCategory> data) {

                // always load from server
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<ReviewCategory>> loadFromDb() {
                Utils.psLog("Load getAllReviewCategory From Db");

                return reviewDao.getAllReviewCategory();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<ReviewCategory>>> createCall() {
                Utils.psLog("Call API Service to get getAllReviewCategory.");

                return psApiService.getAllReviewCategory(apiKey);
            }

            @Override
            protected void onFetchFailed(String message) {

                Utils.psLog("Fetch Failed (getAllReviewCategory) : " + message);
            }

        }.asLiveData();
    }

    //endregion
}
