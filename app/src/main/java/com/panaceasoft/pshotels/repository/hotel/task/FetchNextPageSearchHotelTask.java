package com.panaceasoft.pshotels.repository.hotel.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.SearchHotel;
import com.panaceasoft.pshotels.viewobject.SearchHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class FetchNextPageSearchHotelTask implements Runnable {



    //region Variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();
    private final PSApiService psApiService;
    private final PSCoreDb db;
    private SearchHotelLoadingHolder holder;

    //endregion


    //region Constructor

    public FetchNextPageSearchHotelTask(PSApiService psApiService, PSCoreDb db, SearchHotelLoadingHolder holder) {
        this.psApiService = psApiService;
        this.db = db;
        this.holder = holder;
    }

    //endregion


    //region Override Methods

    @Override
    public void run() {

        try {

            // Call the API Service
            Response<List<Hotel>> response;


            response = psApiService
                        .rawPostSearchHotel(Config.API_KEY,
                                Utils.checkUserId(holder.loginUserId),
                                holder.limit,
                                holder.offset,
                                holder.cityId,
                                holder.hotelName,
                                holder.hotelStarRating,
                                holder.hotelMinPrice,
                                holder.hotelMaxPrice,
                                holder.filterByInfoType,
                                holder.minUserRating
                        ).execute();

            // Wrap with APIResponse Class
            ApiResponse<List<Hotel>> apiResponse = new ApiResponse<>(response);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                try {
                    db.beginTransaction();

                    if(apiResponse.body != null) {
                        for (Hotel hotel : apiResponse.body) {
                            db.hotelDao().insert(new SearchHotel(hotel.hotel_id));
                        }

                        db.hotelDao().insertAll(apiResponse.body);
                    }

                    db.setTransactionSuccessful();
                } catch (NullPointerException ne){
                    Utils.psErrorLog("Null Pointer Exception : ", ne);
                } catch (Exception e) {
                    Utils.psErrorLog("Exception : ", e);
                }finally {
                    db.endTransaction();
                }

                statusLiveData.postValue(Resource.success(apiResponse.getNextPage() != null));

            } else {
                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }

        } catch (IOException e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }

    }

    //endregion


    //region public methods

    /**
     * This function will return Status of Process
     * @return statusLiveData
     */

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion

}
