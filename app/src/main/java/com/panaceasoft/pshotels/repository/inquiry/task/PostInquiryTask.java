package com.panaceasoft.pshotels.repository.inquiry.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.ApiStatus;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import retrofit2.Response;

public class PostInquiryTask implements Runnable {

    //region Variables
    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    //api service and db
    private final PSApiService service;
    private final PSCoreDb db;

    //inquiry data
    private final String hotelId;
    private final String roomId;
    private final String userId;
    private final String inqName;
    private final String inqDesc;
    private final String contactName;
    private final String contactEmail;
    private final String contactPhone;
    private final String apiKey;

    //region Override Methods

    public PostInquiryTask(PSApiService service, PSCoreDb db, String apiKey, String hotelId, String roomId, String userId, String inqName, String inqDesc, String contactName, String contactEmail, String contactPhone) {
        this.service = service;
        this.db = db;
        this.apiKey = apiKey;
        this.hotelId = hotelId;
        this.roomId = roomId;
        this.userId = userId;
        this.inqName = inqName;
        this.inqDesc = inqDesc;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactPhone = contactPhone;
    }

    @Override
    public void run() {
        try {

            // Call the API Service
            Response<ApiStatus> response = service.rawPostInquiry(
                    apiKey,
                    hotelId,
                    roomId,
                    userId,
                    inqName,
                    inqDesc,
                    contactName,
                    contactEmail,
                    contactPhone
            ).execute();

            // Wrap with APIResponse Class
            ApiResponse<ApiStatus> apiResponse = new ApiResponse<>(response);
            Utils.psLog("apiResponse " + apiResponse);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                statusLiveData.postValue(Resource.success(true));
            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion

    //region public methods

    /**
     * This function will return Status of Process
     *
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}
