package com.panaceasoft.pshotels.repository.booking;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.BookingDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.booking.task.FetchNextPageBookingTask;
import com.panaceasoft.pshotels.repository.booking.task.PostBookingTask;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class BookingRepository extends PSRepository {

    //region Variables

    private final BookingDao bookingDao;

    //endregion

    //region Constructor
    @Inject
    BookingRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, BookingDao bookingDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside BookingRepository");

        this.bookingDao = bookingDao;
    }

    public LiveData<Resource<Boolean>> postBooking(String apiKey,
                                                   String loginUserId,
                                                   String userName,
                                                   String userEmail,
                                                   String userPhone,
                                                   String hotelId,
                                                   String roomId,
                                                   String adultCount,
                                                   String kidCount,
                                                   String startDate,
                                                   String endDate,
                                                   String extraBed,
                                                   String remark ) {


        PostBookingTask postBookingTask = new PostBookingTask(
                psApiService,
                db,
                apiKey,
                loginUserId,
                userName,
                userEmail,
                userPhone,
                hotelId,
                roomId,
                adultCount,
                kidCount,
                startDate,
                endDate,
                extraBed,
                remark
        );

        appExecutors.networkIO().execute(postBookingTask);

        return postBookingTask.getStatusLiveData();
    }

    //endregion

    //region booking list

    public LiveData<Resource<List<Booking>>> getBookingList(String apiKey, String loginUserId, String offset) {
        return new NetworkBoundResource<List<Booking>, List<Booking>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Booking> itemList) {
                Utils.psLog("SaveCallResult of booking List");

                db.beginTransaction();
                try {

                    bookingDao.deleteTable();

                    bookingDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting booking list.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Booking> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Booking>> loadFromDb() {
                Utils.psLog("Load Popular Booking From DB.");

                return bookingDao.getAll();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Booking>>> createCall() {
                Utils.psLog("Call Popular Booking WebService");
                return psApiService.getBookingList(apiKey, loginUserId, String.valueOf(Config.BOOKING_COUNT), offset);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of booking WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPagePopularBookings(String apiKey, String loginUserId, String limit, String offset) {
        FetchNextPageBookingTask fetchNextPageBookingTask = new FetchNextPageBookingTask(
                psApiService, db, apiKey, loginUserId, limit, offset);

        appExecutors.networkIO().execute(fetchNextPageBookingTask);

        return fetchNextPageBookingTask.getStatusLiveData();
    }


    // Get Booking By Id
    public LiveData<Booking> getBookingById(String bookingId) {
        return bookingDao.getBookingById(bookingId);
    }

    //endregion
}
