package com.panaceasoft.pshotels.repository.price;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PriceDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Price;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 4/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Singleton
public class PriceRepository extends PSRepository {


    //region Variables

    private final PriceDao priceDao;

    //endregion


    //region Constructor

    @Inject
    PriceRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, PriceDao priceDao) {
        super(psApiService, appExecutors, db);

        this.priceDao = priceDao;
    }

    //endregion


    //region Price Repository Functions for ViewModel

    /**
     * Load About Us
     */
    public LiveData<Resource<Price>> getMaxPrice(String apiKey) {

        String functionKey = "getMaxPrice";

        return new NetworkBoundResource<Price, Price>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull Price item) {
                Utils.psLog("SaveCallResult of get Max Price.");

                db.beginTransaction();
                try {

                    priceDao.deleteTable();

                    priceDao.insert(item);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in inserting Price.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable Price data) {
                // API level cache
                //if (connectivity.isConnected())
                //if (data == null || rateLimiter.shouldFetch(functionKey)) return true;
                //return false;

                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<Price> loadFromDb() {
                Utils.psLog("Load Price From DB.");

                return priceDao.get();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<Price>> createCall() {
                Utils.psLog("Call Price webservice.");
                return psApiService.getMaxPrice(apiKey);
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of price");
                rateLimiter.reset(functionKey);
            }

        }.asLiveData();
    }


    //endregion


}
