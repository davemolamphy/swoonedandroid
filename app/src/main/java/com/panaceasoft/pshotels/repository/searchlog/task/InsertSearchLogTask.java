package com.panaceasoft.pshotels.repository.searchlog.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.viewobject.SearchLog;
import com.panaceasoft.pshotels.viewobject.common.Resource;


public class InsertSearchLogTask implements Runnable {

    //region Variables
    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    //api service and db
    private final PSCoreDb db;

    //review data
    private final SearchLog searchLog;

    public InsertSearchLogTask(PSCoreDb db, SearchLog searchLog) {
        this.db = db;
        this.searchLog = searchLog;
    }

    @Override
    public void run() {

        try {

            db.searchLogDao().deleteAll();

            db.searchLogDao().insert(searchLog);

            statusLiveData.postValue(Resource.success(true));

        } catch (Exception e) {

            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //region public methods

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}
