package com.panaceasoft.pshotels.repository.searchlog;

import android.arch.lifecycle.LiveData;
import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.db.SearchLogDao;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.repository.searchlog.task.InsertSearchLogTask;
import com.panaceasoft.pshotels.viewobject.SearchLog;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import javax.inject.Inject;

public class SearchLogRepository extends PSRepository {

    //region variables
    private final SearchLogDao searchLogDao;
    //endregion

    @Inject
    public SearchLogRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, SearchLogDao searchLogDao) {
        super(psApiService, appExecutors, db);
        this.searchLogDao = searchLogDao;
    }

    public LiveData<SearchLog> getSearchLog()
    {
        return searchLogDao.get();
    }

    public LiveData<Resource<Boolean>> insertSearchLog(SearchLog searchLog) {
        InsertSearchLogTask insertSearchLogTask = new InsertSearchLogTask(db, searchLog);

        appExecutors.networkIO().execute(insertSearchLogTask);

        return insertSearchLogTask.getStatusLiveData();
    }
}
