package com.panaceasoft.pshotels.repository.room.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.RoomByHotelId;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class FetchNextPageRoomByHotelIdTask implements Runnable {

    // region variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();
    private final PSApiService psApiService;
    private final PSCoreDb db;
    private final RoomByHotelIdLoadingHolder holder;

    // end region

    // region constructors

    public FetchNextPageRoomByHotelIdTask(PSApiService psApiService, PSCoreDb db, RoomByHotelIdLoadingHolder holder) {
        this.psApiService = psApiService;
        this.db = db;
        this.holder = holder;
    }

    // end region

    // region override methods

    @Override
    public void run() {

        try {

            // call API service
            Response<List<Room>> response = psApiService.getRawRoomListByHotelId(
                    Config.API_KEY,
                    Utils.checkUserId(holder.loginUserId),
                    holder.hotelId,
                    holder.limit,
                    holder.offset
            ).execute();

            // wrap with APIResponse class
            ApiResponse<List<Room>> apiResponse = new ApiResponse<List<Room>>(response);

            // if response is successful
            if ( apiResponse.isSuccessful()) {

                try {
                    db.beginTransaction();

                    if(apiResponse.body != null) {

                        for (Room room : apiResponse.body) {

                            db.roomDao().insert(new RoomByHotelId( room.room_id ));
                        }

                        db.roomDao().insertAll(apiResponse.body);
                    }

                    db.setTransactionSuccessful();

                } catch (NullPointerException ne){

                    Utils.psErrorLog("Null Pointer Exception : ", ne);
                } catch (Exception e) {

                    Utils.psErrorLog("Exception : ", e);
                }finally {

                    db.endTransaction();
                }

                statusLiveData.postValue( Resource.success( apiResponse.getNextPage() != null ));

            } else {

                statusLiveData.postValue( Resource.error( apiResponse.errorMessage, true ));
            }

        } catch (IOException e) {

            statusLiveData.postValue( Resource.error( e.getMessage(), true ));
        }
    }

    // end region

    //region public methods

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}
