package com.panaceasoft.pshotels.repository.review.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Review;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 4/25/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class FetchNextPageHotelReviewDetailTask  implements Runnable {

    // region variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();
    private final PSApiService psApiService;
    private final PSCoreDb db;
    private ReviewDetailLoadingHolder holder;

    // end region

    public FetchNextPageHotelReviewDetailTask(PSApiService psApiService, PSCoreDb db, ReviewDetailLoadingHolder holder) {
        this.psApiService = psApiService;
        this.db = db;
        this.holder = holder;
    }

    @Override
    public void run() {

        try {

            // Call API service
            Response<List<ReviewDetail>> response = psApiService.getRawHotelReviewDetail(
                    Config.API_KEY,
                    holder.hotelId,
                    holder.limit,
                    holder.offset
            ).execute();

            // Warp with API Response Class
            ApiResponse<List<ReviewDetail>> apiResponse = new ApiResponse<>(response);

            // if response is successful
            if ( apiResponse.isSuccessful()) {

                try {

                    db.beginTransaction();

                    if ( apiResponse.body != null ) {

                        for ( ReviewDetail review: apiResponse.body) {
                            db.reviewDao().insert(new Review( review.review_parent_id, review.rating ));
                        }

                        db.reviewDao().insertAllReviewDetail( apiResponse.body );
                    }

                    db.setTransactionSuccessful();
                } catch ( NullPointerException ne ) {

                    Utils.psErrorLog("Null Pointer Exception : ", ne);
                } catch ( Exception e ) {

                    Utils.psErrorLog("Exception : ", e);
                }finally {

                    db.endTransaction();
                }

                statusLiveData.postValue(Resource.success(apiResponse.getNextPage() != null));
            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }

        } catch (IOException e) {

            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //region public methods

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}