package com.panaceasoft.pshotels.repository.hotel.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.FavouriteHotel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.FavouriteHotelLoadingHolder;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class FetchNextPageFavouriteHotelTask implements Runnable {

    // region variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();
    private final PSApiService psApiService;
    private final PSCoreDb db;
    private FavouriteHotelLoadingHolder holder;

    // end region

    public FetchNextPageFavouriteHotelTask(PSApiService psApiService, PSCoreDb db, FavouriteHotelLoadingHolder holder) {
        this.psApiService = psApiService;
        this.db = db;
        this.holder = holder;
    }

    @Override
    public void run() {

        try {

            // Call API service
            Response<List<Hotel>> response = psApiService.getRawFavouritedHotel(
                    Config.API_KEY,
                    Utils.checkUserId( holder.loginUserId ),
                    holder.limit,
                    holder.offset,
                    "1"
            ).execute();

            // Warp with API Response Class
            ApiResponse<List<Hotel>> apiResponse = new ApiResponse<>(response);

            // if response is successful
            if ( apiResponse.isSuccessful()) {

                try {

                    db.beginTransaction();

                    if ( apiResponse.body != null ) {

                        for ( Hotel hotel: apiResponse.body) {
                            db.hotelDao().insert(new FavouriteHotel( hotel.hotel_id ));
                        }

                        db.hotelDao().insertAll(apiResponse.body);
                    }

                    db.setTransactionSuccessful();
                } catch ( NullPointerException ne ) {

                    Utils.psErrorLog("Null Pointer Exception : ", ne);
                } catch ( Exception e ) {

                    Utils.psErrorLog("Exception : ", e);
                }finally {

                    db.endTransaction();
                }

                statusLiveData.postValue(Resource.success(apiResponse.getNextPage() != null));
            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }

        } catch (IOException e) {

            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //region public methods

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}