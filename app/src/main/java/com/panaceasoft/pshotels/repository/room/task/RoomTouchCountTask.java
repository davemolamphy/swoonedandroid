package com.panaceasoft.pshotels.repository.room.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.viewobject.ApiStatus;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 4/29/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class RoomTouchCountTask implements Runnable {


    //region Variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    private final PSApiService service;
    private final PSCoreDb db;
    private final String loginUserId;
    private final String roomId;

    //endregion


    //region Constructor

    public RoomTouchCountTask(PSApiService service, PSCoreDb db, String loginUserId, String roomId) {
        this.service = service;
        this.db = db;
        this.loginUserId = loginUserId;
        this.roomId = roomId;
    }

    //endregion


    //region Override Methods

    @Override
    public void run() {
        try {

            // Call the API Service
            Response<ApiStatus> response = service.rawRoomPostTouchCount(Config.API_KEY, roomId, loginUserId).execute();

            // Wrap with APIResponse Class
            ApiResponse<ApiStatus> apiResponse = new ApiResponse<>(response);

            // If response is successful
            if (apiResponse.isSuccessful()) {

                statusLiveData.postValue(Resource.success(true));

            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));

            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion


    //region public methods

    /**
     * This function will return Status of Process
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion

}