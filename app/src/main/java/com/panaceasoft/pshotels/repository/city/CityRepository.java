package com.panaceasoft.pshotels.repository.city;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.CityDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.City;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Singleton
public class CityRepository extends PSRepository {

    //region Variables

    private final CityDao cityDao;

    //endregion


    //region Constructor

    @Inject
    public CityRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, CityDao cityDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside CityRepository");

        this.cityDao = cityDao;
    }

    //endregion


    // region get city list

    public LiveData<Resource<List<City>>> getAllCityList(String apiKey) {
        return new NetworkBoundResource<List<City>, List<City>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<City> itemList) {
                Utils.psLog("SaveCallResult of City List");

                db.beginTransaction();
                try {
                    cityDao.deleteAll();

                    cityDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting City List.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<City> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<City>> loadFromDb() {
                Utils.psLog("Load Recommended City From DB.");

                return cityDao.getAllCity();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<City>>> createCall() {
                Utils.psLog("Call Recommend City WebService");

                return psApiService.getCities(apiKey);
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of Trending WebService" + message);
            }

        }.asLiveData();
    }


    // end region

}
