package com.panaceasoft.pshotels.repository.hotelinfo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.HotelFeatureDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.HotelFeatureDetail;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Singleton
public class HotelFeatureRepository extends PSRepository {

    //region Variables

    private final HotelFeatureDao hotelFeatureDao;

    //endregion


    //region Constructor

    @Inject
    public HotelFeatureRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, HotelFeatureDao hotelFeatureDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside HotelFeatureRepository");

        this.hotelFeatureDao = hotelFeatureDao;
    }

    //endregion


    //region Functions for ViewModel

    /**
     * Function getHotelFeaturesByHotelId
     *
     * @param apiKey  APIKey to access web services
     * @param hotelId Hotel Id
     * @return List of Hotel Info
     */
    public LiveData<Resource<List<HotelFeatures>>> getHotelFeaturesByHotelId(String apiKey, String hotelId) {

        return new NetworkBoundResource<List<HotelFeatures>, List<HotelFeatures>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<HotelFeatures> itemList) {
                Utils.psLog("SaveCallResult of getHotelFeaturesByHotelId.");

                db.beginTransaction();

                try {

                    List<HotelFeatureDetail> hotelFeatureDetailList = new ArrayList<>();

                    for (int i = 0; i < itemList.size(); i++) {
                        hotelFeatureDetailList.addAll(itemList.get(i).hinf_types_arr);
                    }

                    hotelFeatureDao.insertAllGroup(itemList);

                    hotelFeatureDao.insertAllType(hotelFeatureDetailList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getHotelFeaturesByHotelId.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<HotelFeatures> data) {

                // getHotelFeaturesByCityId always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<HotelFeatures>> loadFromDb() {
                Utils.psLog("Load getHotelFeaturesByHotelId From Db");

                MutableLiveData<List<HotelFeatures>> hotelInfoGroupList = new MutableLiveData<>();
                appExecutors.diskIO().execute(() -> {
                    List<HotelFeatures> groupList = hotelFeatureDao.getHotelFeatureByParentId(hotelId);
                    appExecutors.mainThread().execute(() ->
                            hotelInfoGroupList.setValue(groupList)
                    );
                });

                return hotelInfoGroupList;

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<HotelFeatures>>> createCall() {
                Utils.psLog("Call API Service to getHotelFeaturesByHotelId.");

                return psApiService.getHotelFeatureByHotelId(apiKey, hotelId);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getHotelFeaturesByHotelId) : " + message);
            }

        }.asLiveData();
    }


    /**
     * Function getHotelFeaturesByCityId
     *
     * @param apiKey APIKey to access web services
     * @param cityId Hotel Id
     * @return List of Hotel Info
     */
    public LiveData<Resource<List<HotelFeatures>>> getHotelFeaturesByCityId(String apiKey, String cityId) {

        return new NetworkBoundResource<List<HotelFeatures>, List<HotelFeatures>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<HotelFeatures> itemList) {
                Utils.psLog("SaveCallResult of getHotelFeaturesByCityId.");

                db.beginTransaction();

                try {

                    hotelFeatureDao.deleteHotelFeatureByParentId(cityId);

                    List<HotelFeatureDetail> hotelFeatureDetailList = new ArrayList<>();

                    for (int i = 0; i < itemList.size(); i++) {
                        hotelFeatureDetailList.addAll(itemList.get(i).hinf_types_arr);
                    }

                    hotelFeatureDao.insertAllGroup(itemList);

                    hotelFeatureDao.insertAllType(hotelFeatureDetailList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getHotelFeaturesByCityId.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<HotelFeatures> data) {

                // getHotelFeaturesByCityId always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<HotelFeatures>> loadFromDb() {
                Utils.psLog("Load HotelFeaturesByCityId From Db");


                MutableLiveData<List<HotelFeatures>> hotelInfoGroupList = new MutableLiveData<>();
                appExecutors.diskIO().execute(() -> {
                    List<HotelFeatures> groupList = hotelFeatureDao.getHotelFeatureByParentId(cityId);
                    appExecutors.mainThread().execute(() ->
                            hotelInfoGroupList.setValue(groupList)
                    );
                });

                return hotelInfoGroupList;

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<HotelFeatures>>> createCall() {
                Utils.psLog("Call API Service to getHotelFeaturesByCityId.");

                return psApiService.getHotelFeaturesByCityId(apiKey, cityId);

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getHotelFeaturesByCityId) : " + message);
            }

        }.asLiveData();
    }

    //endregion

}
