package com.panaceasoft.pshotels.repository.booking.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.PopularHotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class FetchNextPageBookingTask implements Runnable {

    // region variables

    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();
    private final PSApiService psApiService;
    private final PSCoreDb db;
    private String apiKey;
    private String loginUserId;
    private String offset;
    private String limit;

    // end region

    // region constructor

    public FetchNextPageBookingTask(PSApiService psApiService, PSCoreDb db, String apiKey, String loginUserId, String limit, String offset) {
        this.psApiService = psApiService;
        this.db = db;
        this.apiKey = apiKey;
        this.loginUserId = loginUserId;
        this.offset = offset;
        this.limit = limit;
    }

    // end region

    //region Override Methods

    @Override
    public void run() {

        try {

            // call API service
            Response<List<Booking>> response = psApiService.getRawBookingList(
                    Config.API_KEY,
                    Utils.checkUserId( loginUserId ),
                    limit,
                    offset
            ).execute();

            // Wrap with APIResponse Class
            ApiResponse<List<Booking>> apiResponse = new ApiResponse<>(response);

            // If response is successful
            if(apiResponse.isSuccessful()){

                try {
                    db.beginTransaction();

                    if(apiResponse.body != null) {

                        db.bookingDao().insertAll(apiResponse.body);

                    }

                    db.setTransactionSuccessful();
                } catch (NullPointerException ne){
                    Utils.psErrorLog("Null Pointer Exception : ", ne);
                } catch (Exception e) {
                    Utils.psErrorLog("Exception : ", e);
                }finally {
                    db.endTransaction();
                }

                statusLiveData.postValue(Resource.success(apiResponse.getNextPage() != null));

            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }

        } catch (IOException e) {

            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion

    //region public methods

    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion
}


