package com.panaceasoft.pshotels.repository.roominfo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.RoomFeatureDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.RoomFeatureDetail;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Singleton
public class RoomFeatureRepository extends PSRepository {

    //region Variables

    private final RoomFeatureDao roomFeatureDao;

    //endregion


    //region Constructor

    @Inject
    public RoomFeatureRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, RoomFeatureDao roomFeatureDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside RoomFeatureRepository");

        this.roomFeatureDao = roomFeatureDao;
    }

    //endregion


    //region Functions for ViewModel

    /**
     * Function to get room info groups
     *
     * @param apiKey APIKey to access web services
     * @param roomId Room Id
     * @return List of Room Info
     */
    public LiveData<Resource<List<RoomFeatures>>> getRoomFeaturesByRoomId(String apiKey, String roomId) {

        return new NetworkBoundResource<List<RoomFeatures>, List<RoomFeatures>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<RoomFeatures> itemList) {
                Utils.psLog("SaveCallResult of getRoomFeaturesByRoomId.");

                db.beginTransaction();

                try {

                    List<RoomFeatureDetail> roomFeatureDetailList = new ArrayList<>();

                    for(int i=0; i<itemList.size(); i++) {
                        roomFeatureDetailList.addAll(itemList.get(i).rinf_types_arr);
                    }

                    roomFeatureDao.insertAllGroup(itemList);

                    roomFeatureDao.insertAllType(roomFeatureDetailList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of getRoomFeaturesByRoomId.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<RoomFeatures> data) {

                // always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<RoomFeatures>> loadFromDb() {
                Utils.psLog("Load getRoomFeaturesByRoomId From Db");


                MutableLiveData<List<RoomFeatures>> roomInfoGroupList = new MutableLiveData<>();
                appExecutors.diskIO().execute(() -> {
                    List<RoomFeatures> groupList = roomFeatureDao.getRoomFeatureByParentId(roomId);
                    appExecutors.mainThread().execute(() ->
                            roomInfoGroupList.setValue(groupList)
                    );
                });

                return roomInfoGroupList;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<RoomFeatures>>> createCall() {
                Utils.psLog("Call API Service to getRoomFeaturesByRoomId.");

                return psApiService.getRoomFeatureByRoomId( apiKey, roomId );

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (getRoomFeaturesByRoomId) : " + message);
            }

        }.asLiveData();
    }
}
