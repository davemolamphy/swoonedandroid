package com.panaceasoft.pshotels.repository.review.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.ApiStatus;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.SyncUserRatingHolder;

import retrofit2.Response;

public class PostReviewTask implements Runnable {

    //region Variables
    private final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

    //api service and db
    private final PSApiService service;
    private final PSCoreDb db;

    //review data
    private final String roomId;
    private final String userId;
    private final String reviewDesc;
    private final SyncUserRatingHolder syncUserRatingHolder;
    private final String apiKey;


    //region Override Methods
    public PostReviewTask(PSApiService service, PSCoreDb db, String apiKey, String roomId, String userId, String reviewDesc, SyncUserRatingHolder syncUserRatingHolder) {
        this.service = service;
        this.db = db;
        this.apiKey = apiKey;
        this.roomId = roomId;
        this.userId = userId;
        this.reviewDesc = reviewDesc;
        this.syncUserRatingHolder = syncUserRatingHolder;
    }

    @Override
    public void run() {
        try {

            Gson gson = new Gson();
            String userRatingString = gson.toJson(syncUserRatingHolder.ratings);

            // Call the API Service
            Response<ApiStatus> response = service.rawPostReview(
                    apiKey,
                    roomId,
                    Utils.checkUserId( userId ),
                    reviewDesc,
                    userRatingString
            ).execute();

            // Wrap with APIResponse Class
            ApiResponse<ApiStatus> apiResponse = new ApiResponse<>(response);

            Utils.psLog("apiResponse " + apiResponse);
            // If response is successful
            if (apiResponse.isSuccessful()) {

                statusLiveData.postValue(Resource.success(true));
            } else {

                statusLiveData.postValue(Resource.error(apiResponse.errorMessage, true));
            }
        } catch (Exception e) {
            statusLiveData.postValue(Resource.error(e.getMessage(), true));
        }
    }

    //endregion

    //region public methods

    /**
     * This function will return Status of Process
     * @return statusLiveData
     */
    public LiveData<Resource<Boolean>> getStatusLiveData() {
        return statusLiveData;
    }

    //endregion

}