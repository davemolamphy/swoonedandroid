package com.panaceasoft.pshotels.repository.hotel;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.panaceasoft.pshotels.AppExecutors;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.ApiResponse;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.HotelDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.repository.common.NetworkBoundResource;
import com.panaceasoft.pshotels.repository.common.NotificationTask;
import com.panaceasoft.pshotels.repository.common.PSRepository;
import com.panaceasoft.pshotels.repository.hotel.task.FavouriteHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.FetchNextPageFavouriteHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.FetchNextPagePopularHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.FetchNextPagePromotionHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.FetchNextPageRecommendedHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.FetchNextPageSearchHotelTask;
import com.panaceasoft.pshotels.repository.hotel.task.TouchCountTask;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.FavouriteHotel;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.PopularHotel;
import com.panaceasoft.pshotels.viewobject.PromotionHotel;
import com.panaceasoft.pshotels.viewobject.RecommendedHotel;
import com.panaceasoft.pshotels.viewobject.SearchHotel;
import com.panaceasoft.pshotels.viewobject.SearchHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.FavouriteHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.holder.PromotionHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.holder.RecommendedHotelLoadingHolder;

import java.util.List;


import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Singleton
public class HotelRepository extends PSRepository {

    //region Variables

    private final HotelDao hotelDao;

    //endregion


    //region Constructor

    @Inject
    public HotelRepository(PSApiService psApiService, AppExecutors appExecutors, PSCoreDb db, HotelDao hotelDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside HotelRepository");

        this.hotelDao = hotelDao;
    }

    //endregion


    /**
     * Function to load search hotel list
     *
     * @param holder Search Parameters
     */
    public LiveData<Resource<List<Hotel>>> postSearchHotel(SearchHotelLoadingHolder holder) {

        return new NetworkBoundResource<List<Hotel>, List<Hotel>>(appExecutors) {

            private boolean isFromServer = false;
            @Override
            protected void saveCallResult(@NonNull List<Hotel> itemList) {
                Utils.psLog("SaveCallResult of search hotels.");

                db.beginTransaction();

                try {
                    hotelDao.deleteSearchHotel();

                    hotelDao.insertAll(itemList);

                    for (Hotel item: itemList) {
                        hotelDao.insert(new SearchHotel(item.hotel_id));
                    }

                    isFromServer = true;

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in doing transaction of search hotels.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Hotel> data) {

                // Recent hotel always load from server
                return connectivity.isConnected();

            }

            @NonNull
            @Override
            protected LiveData<List<Hotel>> loadFromDb() {
                Utils.psLog("Load Search Hotel From Db");

                if(connectivity.isConnected()) {
                    if(isFromServer) {
                        return hotelDao.getSearchHotel();
                    }else {
                        return AbsentLiveData.create();
                    }
                }else {
                    return hotelDao.getSearchHotel();
                }

            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {
                Utils.psLog("Call API Service to search hotel.");

                return psApiService.postSearchHotel(holder.apiKey,
                        Utils.checkUserId(holder.loginUserId),
                        String.valueOf(Config.SEARCH_COUNT),
                        holder.offset,
                        holder.cityId,
                        holder.hotelName,
                        holder.hotelStarRating,
                        holder.hotelMinPrice,
                        holder.hotelMaxPrice,
                        holder.filterByInfoType,
                        holder.minUserRating
                        );

            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed (SearchHotel) : " + message);
                appExecutors.diskIO().execute(() -> {

                    db.beginTransaction();

                    try {

                        hotelDao.deleteSearchHotel();

                        db.setTransactionSuccessful();

                    } catch (Exception e) {
                        Utils.psErrorLog("Error in doing transaction of search hotels.", e);
                    } finally {
                        db.endTransaction();
                    }

                });
            }

        }.asLiveData();
    }

    /**
     * Function to call background task to get next news page ( Search )
     *
     * @param loadingHolder Parameters to able to load next news page.
     * @return Parameters to able to load next news page.
     */
    public LiveData<Resource<Boolean>> getNextPageSearchHotels(SearchHotelLoadingHolder loadingHolder) {
        FetchNextPageSearchHotelTask fetchNextPageSearchHotelTask = new FetchNextPageSearchHotelTask(
                psApiService,  db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageSearchHotelTask);

        return fetchNextPageSearchHotelTask.getStatusLiveData();

    }

    // region recommended hotel list

    public LiveData<Resource<List<Hotel>>> getRecommendedHotelList(String apiKey, String loginUserId, String offset) {
        return new NetworkBoundResource<List<Hotel>, List<Hotel>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Hotel> itemList) {
                Utils.psLog("SaveCallResult of recommended hotel List");

                db.beginTransaction();
                try {
                    hotelDao.deleteRecommendedHotels();

                    for(Hotel hotel: itemList){
                        hotelDao.insert(new RecommendedHotel(hotel.hotel_id));
                    }

                    hotelDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting recommended hotel list.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Hotel> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Hotel>> loadFromDb() {
                Utils.psLog("Load Recommended Hotel From DB.");

                return hotelDao.getAllRecommendedHotel();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {
                Utils.psLog("Call Recommend Hotel WebService");

                return psApiService.getRecommendedHotel(apiKey, loginUserId, String.valueOf(Config.RECOMMENDED_HOTEL_COUNT), offset, "1" );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of Recommended WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPageRecommendedHotels(RecommendedHotelLoadingHolder loadingHolder) {
        FetchNextPageRecommendedHotelTask fetchNextPageRecommendedHotelTask = new FetchNextPageRecommendedHotelTask(
                psApiService, db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageRecommendedHotelTask);

        return fetchNextPageRecommendedHotelTask.getStatusLiveData();
    }

    // end region

    // region popular hotel list

    public LiveData<Resource<List<Hotel>>> getPopularHotelList(String apiKey, String loginUserId, String offset) {
        return new NetworkBoundResource<List<Hotel>, List<Hotel>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Hotel> itemList) {
                Utils.psLog("SaveCallResult of popular hotel List");

                db.beginTransaction();
                try {
                    hotelDao.deletePopularHotels();

                    for(Hotel hotel: itemList){
                        hotelDao.insert(new PopularHotel(hotel.hotel_id));
                    }

                    hotelDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting popular hotel.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Hotel> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Hotel>> loadFromDb() {
                Utils.psLog("Load Popular Hotel From DB.");

                return hotelDao.getAllPopularHotel();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {
                Utils.psLog("Call Popular Hotel WebService");
                return psApiService.getPopularHotel(apiKey, loginUserId, String.valueOf(Config.POPULAR_HOTEL_COUNT), offset, "1" );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of Popular WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPagePopularHotels(PopularHotelLoadingHolder loadingHolder) {
        FetchNextPagePopularHotelTask fetchNextPagePopularHotelTask = new FetchNextPagePopularHotelTask(
                psApiService, db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPagePopularHotelTask);

        return fetchNextPagePopularHotelTask.getStatusLiveData();
    }

    // end region

    // region promotion hotel list

    public LiveData<Resource<List<Hotel>>> getPromotionHotelList(String apiKey, String loginUserId, String offset) {
        return new NetworkBoundResource<List<Hotel>, List<Hotel>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Hotel> itemList) {
                Utils.psLog("SaveCallResult of Promotion Hotel List");

                db.beginTransaction();
                try {
                    hotelDao.deletePromotionHotels();

                    for(Hotel hotel: itemList){
                        hotelDao.insert(new PromotionHotel(hotel.hotel_id));
                    }

                    hotelDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Utils.psErrorLog("Error in Inserting Promotion Hotel.", e);
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Hotel> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Hotel>> loadFromDb() {
                Utils.psLog("Load Promotion Hotel From DB.");

                return hotelDao.getAllPromotionHotel();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {
                Utils.psLog("Call Promotion Hotel WebService");
                return psApiService.getPromotionHotel(
                        apiKey,
                        loginUserId,
                        String.valueOf(Config.PROMOTION_HOTEL_COUNT), offset, "1"
                );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of Promotion WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPagePromotionHotels(PromotionHotelLoadingHolder loadingHolder) {
        FetchNextPagePromotionHotelTask fetchNextPagePromotionHotelTask = new FetchNextPagePromotionHotelTask(
                psApiService, db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPagePromotionHotelTask);

        return fetchNextPagePromotionHotelTask.getStatusLiveData();
    }

    public LiveData<Resource<Hotel>> getHotelByHotelId(String apiKey,  String loginUserId, String hotelId ) {

        String functionKey = "key"+hotelId;

        return new NetworkBoundResource<Hotel, List<Hotel>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Hotel> item) {

                Utils.psLog("SaveCallResult of getHotelByHotelId.");

                db.beginTransaction();

                try {

                    hotelDao.insertAll(item);

                    db.setTransactionSuccessful();
                } catch (Exception e) {

                    Utils.psErrorLog("Error in doing transaction of getHotelByHotelId.", e);
                } finally {

                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable Hotel data) {

                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<Hotel> loadFromDb() {

                Utils.psLog("Load Hotel From DB By HotelID");
                return hotelDao.getById(hotelId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {

                Utils.psLog("Call API Service to get hotel by id.");
                return psApiService.getHotelById( apiKey, Utils.checkUserId( loginUserId ), hotelId );
            }

            @Override
            protected void onFetchFailed(String message) {

                rateLimiter.reset(functionKey);
                Utils.psLog("Fetch Failed in getting hotel by id.");
            }

        }.asLiveData();
    }

    // end region

    // region favourite hotel list

    public LiveData<Resource<List<Hotel>>> getFavouriteHotelList(String apiKey, String loginUserId, String offset) {

        return new NetworkBoundResource<List<Hotel>, List<Hotel>>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Hotel> itemList) {
                Utils.psLog("SaveCallResult of Favourite Hotel List");

                db.beginTransaction();

                try {

                    hotelDao.deleteFavouriteHotels();

                    for(Hotel hotel: itemList){
                        hotelDao.insert(new FavouriteHotel(hotel.hotel_id));
                    }

                    hotelDao.insertAll(itemList);

                    db.setTransactionSuccessful();

                } catch (Exception e) {

                    Utils.psErrorLog("Error in Inserting Favourite Hotel.", e);
                } finally {

                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Hotel> data) {
                return connectivity.isConnected();
            }

            @NonNull
            @Override
            protected LiveData<List<Hotel>> loadFromDb() {

                Utils.psLog("Load Favourite Hotel From DB.");

                return hotelDao.getAllFavouriteHotels();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Hotel>>> createCall() {
                Utils.psLog("Call Favourite Hotel WebService");
                return psApiService.getFavouritedHotel(
                        apiKey,
                        loginUserId,
                        String.valueOf(Config.RECOMMENDED_HOTEL_COUNT),
                        offset,
                        "1"
                );
            }

            @Override
            protected void onFetchFailed(String message) {
                Utils.psLog("Fetch Failed of Favourite WebService" + message);
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getNextPageFavouriteHotels(FavouriteHotelLoadingHolder loadingHolder) {
        FetchNextPageFavouriteHotelTask fetchNextPageFavouriteHotelTask = new FetchNextPageFavouriteHotelTask(
                psApiService, db, loadingHolder);

        appExecutors.networkIO().execute(fetchNextPageFavouriteHotelTask);

        return fetchNextPageFavouriteHotelTask.getStatusLiveData();
    }


    //endregion


    /**
     * Function to call background task to register Notification
     *
     * @param platform Current Platform
     * @return Status of Process
     */
    public LiveData<Resource<Boolean>> registerNotification(Context context, String platform, String token) {
        NotificationTask notificationTask = new NotificationTask( context,
                psApiService, platform, true, token);

        Utils.psLog("Register Notification : News repository.");
        appExecutors.networkIO().execute(notificationTask);

        return notificationTask.getStatusLiveData();
    }


    /**
     * Function to call background task to un-register notification.
     *
     * @param platform Current Platform
     * @return Status of Process
     */
    public LiveData<Resource<Boolean>> unregisterNotification(Context context, String platform, String token) {
        NotificationTask notificationTask = new NotificationTask(context,
                psApiService, platform, false, token);

        Utils.psLog("Unregister Notification : News repository.");
        appExecutors.networkIO().execute(notificationTask);

        return notificationTask.getStatusLiveData();
    }


    /**
     * Function to call background task to do Favourite
     *
     * @param loginUserId Current Login User Id
     * @param newsId News Id
     * @return Status of Process
     */
    public LiveData<Resource<Boolean>> doFavourite(String loginUserId, String newsId) {
        FavouriteHotelTask favouriteHotelTask = new FavouriteHotelTask(
                psApiService,  db, loginUserId, newsId);

        Utils.psLog("Do Favourite : repository.");
        appExecutors.networkIO().execute(favouriteHotelTask);

        return favouriteHotelTask.getStatusLiveData();
    }

    /**
     * Function to call background task to submit Open Count
     *
     * @param loginUserId Current Login User Id
     * @param hotelId News Id
     * @return Status of Process
     */
    public LiveData<Resource<Boolean>> doPostTouchCount(String loginUserId, String hotelId) {
        TouchCountTask touchCountTask = new TouchCountTask(
                psApiService,  db, loginUserId, hotelId);

        Utils.psLog("Do Touch : repository.");
        appExecutors.networkIO().execute(touchCountTask);

        return touchCountTask.getStatusLiveData();
    }

}