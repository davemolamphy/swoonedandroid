package com.panaceasoft.pshotels.di;

import com.panaceasoft.pshotels.ui.aboutus.AboutUsFragment;
import com.panaceasoft.pshotels.ui.booking.detail.BookingDetailFragment;
import com.panaceasoft.pshotels.ui.booking.entry.BookingFragment;
import com.panaceasoft.pshotels.ui.booking.list.BookingListFragment;
import com.panaceasoft.pshotels.ui.contactus.ContactUsFragment;
import com.panaceasoft.pshotels.ui.detail.hotel.HotelDetailFragment;
import com.panaceasoft.pshotels.ui.detail.room.RoomDetailFragment;
import com.panaceasoft.pshotels.ui.gallery.GalleryDetailFragment;
import com.panaceasoft.pshotels.ui.gallery.GalleryFragment;
import com.panaceasoft.pshotels.ui.hotel.favourite.FavouriteHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.popular.PopularHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.promotion.PromotionHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.recommended.RecommendedHotelListFragment;
import com.panaceasoft.pshotels.ui.hotel.hotelView.HotelViewFragment;
import com.panaceasoft.pshotels.ui.hotel.search.destinationfilter.CityListFragment;
import com.panaceasoft.pshotels.ui.hotel.search.filter.FilterFragment;
import com.panaceasoft.pshotels.ui.hotel.search.filter.HotelFilterFragment;
import com.panaceasoft.pshotels.ui.hotel.search.SearchResultListFragment;
import com.panaceasoft.pshotels.ui.inquiry.InquiryFragment;
import com.panaceasoft.pshotels.ui.hotel.search.SearchFragment;
import com.panaceasoft.pshotels.ui.notification.NotificationSettingFragment;
import com.panaceasoft.pshotels.ui.review.entry.RoomListFilterFragment;
import com.panaceasoft.pshotels.ui.review.entry.SubmitReviewFragment;
import com.panaceasoft.pshotels.ui.review.hotel.HotelReviewListFragment;
import com.panaceasoft.pshotels.ui.review.room.RoomReviewListFragment;
import com.panaceasoft.pshotels.ui.user.PasswordChangeFragment;
import com.panaceasoft.pshotels.ui.user.ProfileEditFragment;
import com.panaceasoft.pshotels.ui.user.ProfileFragment;
import com.panaceasoft.pshotels.ui.user.UserForgotPasswordFragment;
import com.panaceasoft.pshotels.ui.user.UserLoginFragment;
import com.panaceasoft.pshotels.ui.user.UserRegisterFragment;
import com.panaceasoft.pshotels.ui.welcome.WelcomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract UserLoginFragment contributeUserLoginFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract ProfileEditFragment contributeProfileEditFragment();

    @ContributesAndroidInjector
    abstract UserRegisterFragment contributeUserRegisterFragment();

    @ContributesAndroidInjector
    abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragmentFragment();

    @ContributesAndroidInjector
    abstract AboutUsFragment contributeAboutUsFragmentFragment();

    @ContributesAndroidInjector
    abstract SearchResultListFragment contributeSearchResultNewsListFragment();

    @ContributesAndroidInjector
    abstract GalleryFragment contributeGalleryFragment();

    @ContributesAndroidInjector
    abstract GalleryDetailFragment contributeGalleryDetailFragment();

    @ContributesAndroidInjector
    abstract PasswordChangeFragment contributePasswordChangeFragment();

    @ContributesAndroidInjector
    abstract NotificationSettingFragment contributeNotificationSettingFragment();

    @ContributesAndroidInjector
    abstract ContactUsFragment contributeContactUsFragment();

    @ContributesAndroidInjector
    abstract RecommendedHotelListFragment contributeRecommendedHotelListFragment();

    @ContributesAndroidInjector
    abstract PopularHotelListFragment contributePopularHotelListFragment();

    @ContributesAndroidInjector
    abstract PromotionHotelListFragment contributePromotionHotelListFragment();

    @ContributesAndroidInjector
    abstract HotelViewFragment contributeHotelViewFragment();

    @ContributesAndroidInjector
    abstract FavouriteHotelListFragment contributeFavouriteHotelListFragment();

    @ContributesAndroidInjector
    abstract FilterFragment contributeFilterFragment();

    @ContributesAndroidInjector
    abstract HotelDetailFragment contributeHotelDetailFragment();

    @ContributesAndroidInjector
    abstract HotelFilterFragment contributeRoomFilterFragment();

    @ContributesAndroidInjector
    abstract HotelReviewListFragment contributeHotelReviewListFragment();

    @ContributesAndroidInjector
    abstract RoomDetailFragment contributeRoomDetailFragment();

    @ContributesAndroidInjector
    abstract RoomReviewListFragment contributeRoomReviewListFragment();

    @ContributesAndroidInjector
    abstract CityListFragment contributeCityListFragment();

    @ContributesAndroidInjector
    abstract InquiryFragment contributeInquiryFragment();

    @ContributesAndroidInjector
    abstract SubmitReviewFragment contributeSubmitReviewFragment();

    @ContributesAndroidInjector
    abstract RoomListFilterFragment contributeRoomListFilterFragment();

    @ContributesAndroidInjector
    abstract BookingFragment contributeBookingFragment();

    @ContributesAndroidInjector
    abstract BookingListFragment contributeBookingListFragment();

    @ContributesAndroidInjector
    abstract BookingDetailFragment contributeBookingDetailFragment();

    @ContributesAndroidInjector
    abstract WelcomeFragment contributeWelcomeFragment();
}
