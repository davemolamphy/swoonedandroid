package com.panaceasoft.pshotels.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.api.PSApiService;
import com.panaceasoft.pshotels.db.AboutUsDao;
import com.panaceasoft.pshotels.db.BookingDao;
import com.panaceasoft.pshotels.db.CityDao;
import com.panaceasoft.pshotels.db.HotelDao;
import com.panaceasoft.pshotels.db.HotelFeatureDao;
import com.panaceasoft.pshotels.db.ImageDao;
import com.panaceasoft.pshotels.db.PSCoreDb;
import com.panaceasoft.pshotels.db.PriceDao;
import com.panaceasoft.pshotels.db.RoomDao;
import com.panaceasoft.pshotels.db.ReviewDao;
import com.panaceasoft.pshotels.db.RoomFeatureDao;
import com.panaceasoft.pshotels.db.SearchLogDao;
import com.panaceasoft.pshotels.db.UserDao;
import com.panaceasoft.pshotels.utils.Connectivity;
import com.panaceasoft.pshotels.utils.LiveDataCallAdapterFactory;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.panaceasoft.pshotels.db.PSCoreDb.MIGRATION_2_3;
import static com.panaceasoft.pshotels.db.PSCoreDb.MIGRATION_3_4;


/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module(includes = ViewModelModule.class)
class AppModule {

    @Singleton
    @Provides
    PSApiService providePSNewsService() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        return new Retrofit.Builder()
                .baseUrl(Config.APP_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(PSApiService.class);
        
    }

    @Singleton @Provides
    PSCoreDb provideDb(Application app) {
        return Room.databaseBuilder(app, PSCoreDb.class,"pshotels.db")
                .addMigrations(MIGRATION_2_3, MIGRATION_3_4)
                .build();
    }

    @Singleton @Provides
    Connectivity provideConnectivity(Application app) {
        return new Connectivity(app);
    }

    @Singleton @Provides
    SharedPreferences provideSharedPreferences(Application app){
        return PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
    }


    @Singleton @Provides
    HotelDao provideHotelDao(PSCoreDb db) {
        return db.hotelDao();
    }

    @Singleton @Provides
    CityDao provideCityDao(PSCoreDb db) {
        return db.cityDao();
    }

    @Singleton @Provides
    UserDao provideUserDao(PSCoreDb db) {
        return db.userDao();
    }

    @Singleton @Provides
    AboutUsDao provideAboutUsDao(PSCoreDb db) {
        return db.aboutUsDao();
    }

    @Singleton @Provides
    ImageDao provideImageDao(PSCoreDb db) {
        return db.imageDao();
    }

    @Singleton @Provides
    HotelFeatureDao provideHotelInfoDao(PSCoreDb db) {
        return db.hotelInfoDao();
    }

    @Singleton @Provides
    ReviewDao provideReviewDao(PSCoreDb db) {
        return db.reviewDao();
    }

    @Singleton @Provides
    RoomDao provideRoomDao(PSCoreDb db) {
        return db.roomDao();
    }

    @Singleton @Provides
    RoomFeatureDao provideRoomFeatureDao(PSCoreDb db) { return db.roomFeatureDao(); }

    @Singleton @Provides
    SearchLogDao provideSearchLogDao(PSCoreDb db) { return db.searchLogDao(); }

    @Singleton @Provides
    PriceDao providePriceDao(PSCoreDb db) { return db.priceDao(); }

    @Singleton @Provides
    BookingDao provideBookingDao(PSCoreDb db) {
        return db.bookingDao();
    }
}
