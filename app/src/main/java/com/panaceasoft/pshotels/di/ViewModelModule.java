package com.panaceasoft.pshotels.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import com.panaceasoft.pshotels.viewmodel.aboutus.AboutUsViewModel;
import com.panaceasoft.pshotels.viewmodel.booking.BookingViewModel;
import com.panaceasoft.pshotels.viewmodel.city.CityViewModel;
import com.panaceasoft.pshotels.viewmodel.contactus.ContactUsViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.FavouriteHotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.HotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.PopularHotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.PromotionHotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.RecommendedHotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotel.SearchHotelViewModel;
import com.panaceasoft.pshotels.viewmodel.hotelInfo.HotelFeatureViewModel;
import com.panaceasoft.pshotels.viewmodel.image.ImageViewModel;
import com.panaceasoft.pshotels.viewmodel.common.NotificationViewModel;
import com.panaceasoft.pshotels.viewmodel.common.PSViewModelFactory;
import com.panaceasoft.pshotels.viewmodel.inquiry.InquiryViewModel;
import com.panaceasoft.pshotels.viewmodel.price.PriceViewModel;
import com.panaceasoft.pshotels.viewmodel.review.RoomListFilterViewModel;
import com.panaceasoft.pshotels.viewmodel.review.SubmitReviewViewModel;
import com.panaceasoft.pshotels.viewmodel.room.RoomByHotelIdViewModel;
import com.panaceasoft.pshotels.viewmodel.review.ReviewViewModel;
import com.panaceasoft.pshotels.viewmodel.room.RoomViewModel;
import com.panaceasoft.pshotels.viewmodel.roomInfo.RoomFeatureViewModel;
import com.panaceasoft.pshotels.viewmodel.searchlog.SearchLogViewModel;
import com.panaceasoft.pshotels.viewmodel.user.UserViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Panacea-Soft on 11/16/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module
abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(PSViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel.class)
    abstract ViewModel bindUserViewModel(UserViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AboutUsViewModel.class)
    abstract ViewModel bindAboutUsViewModel(AboutUsViewModel aboutUsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ImageViewModel.class)
    abstract ViewModel bindImageViewModel(ImageViewModel imageViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel.class)
    abstract ViewModel bindNotificationViewModel(NotificationViewModel notificationViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ContactUsViewModel.class)
    abstract ViewModel bindContactUsViewModel(ContactUsViewModel contactUsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HotelFeatureViewModel.class)
    abstract ViewModel bindHotelInfoViewModel(HotelFeatureViewModel hotelFeatureViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReviewViewModel.class)
    abstract ViewModel bindReviewViewModel(ReviewViewModel reviewViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchHotelViewModel.class)
    abstract ViewModel bindSearchHotelViewModel(SearchHotelViewModel searchHotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RecommendedHotelViewModel.class)
    abstract ViewModel bindRecommendedHotelViewModel(RecommendedHotelViewModel recommendedHotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PopularHotelViewModel.class)
    abstract ViewModel bindPopularHotelViewModel(PopularHotelViewModel popularHotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PromotionHotelViewModel.class)
    abstract ViewModel bindPromotionHotelViewModel(PromotionHotelViewModel promotionHotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RoomByHotelIdViewModel.class)
    abstract ViewModel bindRoomByHotelIdViewModel(RoomByHotelIdViewModel roomByHotelIdViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HotelViewModel.class)
    abstract ViewModel bindHotelViewModel(HotelViewModel hotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FavouriteHotelViewModel.class)
    abstract ViewModel bindFavouriteHotelViewModel(FavouriteHotelViewModel favouriteHotelViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RoomViewModel.class)
    abstract ViewModel bindRoomViewModel(RoomViewModel roomViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RoomFeatureViewModel.class)
    abstract ViewModel bindRoomFeatureViewModel(RoomFeatureViewModel roomFeatureViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CityViewModel.class)
    abstract ViewModel bindCityViewModel(CityViewModel cityViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InquiryViewModel.class)
    abstract ViewModel bindInquiryViewModel(InquiryViewModel inquiryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SubmitReviewViewModel.class)
    abstract ViewModel bindSubmitReviewViewModel(SubmitReviewViewModel submitReviewViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RoomListFilterViewModel.class)
    abstract ViewModel bindRoomListFilterViewModel(RoomListFilterViewModel roomListFilterViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchLogViewModel.class)
    abstract ViewModel bindSearchLogViewModel(SearchLogViewModel searchLogViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PriceViewModel.class)
    abstract ViewModel bindPriceViewModel(PriceViewModel priceViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BookingViewModel.class)
    abstract ViewModel bindBookingViewModel(BookingViewModel bookingViewModel);
}
