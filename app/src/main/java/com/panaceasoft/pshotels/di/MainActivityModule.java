package com.panaceasoft.pshotels.di;

import com.panaceasoft.pshotels.MainActivity;
import com.panaceasoft.pshotels.ui.aboutus.AboutUsActivity;
import com.panaceasoft.pshotels.ui.booking.detail.BookingDetailActivity;
import com.panaceasoft.pshotels.ui.booking.entry.BookingActivity;
import com.panaceasoft.pshotels.ui.booking.list.BookingListActivity;
import com.panaceasoft.pshotels.ui.detail.hotel.HotelDetailActivity;
import com.panaceasoft.pshotels.ui.detail.room.RoomDetailActivity;
import com.panaceasoft.pshotels.ui.gallery.GalleryActivity;
import com.panaceasoft.pshotels.ui.gallery.GalleryDetailActivity;
import com.panaceasoft.pshotels.ui.hotel.hotelView.HotelViewActivity;
import com.panaceasoft.pshotels.ui.hotel.search.destinationfilter.CityListActivity;
import com.panaceasoft.pshotels.ui.hotel.search.filter.FilterActivity;
import com.panaceasoft.pshotels.ui.hotel.search.filter.HotelFilterActivity;
import com.panaceasoft.pshotels.ui.inquiry.InquiryActivity;
import com.panaceasoft.pshotels.ui.hotel.search.SearchResultListActivity;
import com.panaceasoft.pshotels.ui.review.entry.RoomListFilterActivity;
import com.panaceasoft.pshotels.ui.review.entry.SubmitReviewActivity;
import com.panaceasoft.pshotels.ui.review.hotel.HotelReviewListActivity;
import com.panaceasoft.pshotels.ui.review.room.RoomReviewListActivity;
import com.panaceasoft.pshotels.ui.user.PasswordChangeActivity;
import com.panaceasoft.pshotels.ui.user.ProfileEditActivity;
import com.panaceasoft.pshotels.ui.user.UserForgotPasswordActivity;
import com.panaceasoft.pshotels.ui.user.UserLoginActivity;
import com.panaceasoft.pshotels.ui.user.UserRegisterActivity;
import com.panaceasoft.pshotels.ui.welcome.WelcomeActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract ProfileEditActivity contributeProfileEditActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract UserRegisterActivity contributeUserRegisterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract UserForgotPasswordActivity contributeUserForgotPasswordActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SearchResultListActivity contributeSearchResultListActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract AboutUsActivity contributeAboutUsActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract UserLoginActivity contributeUserLoginActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract GalleryActivity contributeGalleryActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract GalleryDetailActivity contributeGalleryDetailActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract PasswordChangeActivity contributePasswordChangeActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract HotelViewActivity contributeHotelViewActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract FilterActivity contributeFilterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract HotelDetailActivity contributeHotelDetailActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract HotelFilterActivity contributeRoomFilterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract HotelReviewListActivity contributeHotelReviewListActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract RoomDetailActivity contributeRoomDetailActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract InquiryActivity contributeInquiryActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract RoomReviewListActivity contributeRoomReviewListActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract CityListActivity contributeCityListActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SubmitReviewActivity contributeSubmitReviewActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract RoomListFilterActivity contributeRoomListFilterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BookingActivity contributeBookingActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BookingListActivity contributeBookingListActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BookingDetailActivity contributeBookingDetailActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract WelcomeActivity contributeWelcomeActivity();
}
