package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.promotion.PromotionHotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.PromotionHotelLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class PromotionHotelViewModel extends ViewModel {

    // region task handler

    private PromotionHotelTaskHandler backgroundTaskHandler;

    // for trending news
    private final LiveData<Resource<List<Hotel>>> promotionHotelData;
    private MutableLiveData<PromotionHotelLoadingHolder> promotionHotelObj = new MutableLiveData<>();


    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;

    //endregion

    @Inject
    PromotionHotelViewModel(HotelRepository repository) {
        Utils.psLog("Inside PromotionHotelViewModel");

        backgroundTaskHandler = new PromotionHotelTaskHandler(repository);

        promotionHotelData = Transformations.switchMap(promotionHotelObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Promotion Hotel List");

            return repository.getPromotionHotelList(
                    Config.API_KEY,
                    Utils.checkUserId(obj.loginUserId),
                    obj.offset
            );
        });
    }

    //region Public Methods


    // Trending News List

    public LiveData<Resource<List<Hotel>>> getPromotionHotelList() {

        return promotionHotelData;

    }

    public void setPromotionHotelList(String limit, String offset, String loginUserId) {

        PromotionHotelLoadingHolder holder = new PromotionHotelLoadingHolder(limit, offset, loginUserId);

        promotionHotelObj.setValue(holder);

    }


    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {
        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(PromotionHotelLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

    //end for pagination test


    //endregion
}