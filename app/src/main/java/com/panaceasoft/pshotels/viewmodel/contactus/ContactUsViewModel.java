package com.panaceasoft.pshotels.viewmodel.contactus;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.repository.contactus.ContactUsRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.contactus.ContactUsBackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.Utils;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 6/2/18.
 * Contact Email : teamps.is.cool@gmail.com
 * Website : http://www.panacea-soft.com
 */

public class ContactUsViewModel extends ViewModel {

    private final ContactUsBackgroundTaskHandler backgroundTaskHandler;
    public boolean isLoading = false;
    public String contactName = "";
    public String contactEmail = "";
    public String contactDesc = "";
    public String contactPhone = "";


    @Inject
    ContactUsViewModel(ContactUsRepository repository) {
        Utils.psLog("Inside ContactUsViewModel");
        backgroundTaskHandler = new ContactUsBackgroundTaskHandler(repository);


    }

    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadingStatus() {
        return backgroundTaskHandler.getLoadingState();
    }

    public void postContactUs(String apiKey,
                              String contactName,
                              String contactEmail,
                              String contactDesc,
                              String contactPhone) {

        backgroundTaskHandler.postContactUs(apiKey,
                contactName,
                contactEmail,
                contactDesc,
                contactPhone);
    }

}
