package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.popular.PopularHotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class PopularHotelViewModel extends ViewModel {

    // region task handler

    private PopularHotelTaskHandler backgroundTaskHandler;

    // for trending news
    private final LiveData<Resource<List<Hotel>>> popularHotelData;
    private MutableLiveData<PopularHotelLoadingHolder> popularHotelObj = new MutableLiveData<>();


    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;

    //endregion

    @Inject
    PopularHotelViewModel(HotelRepository repository) {
        Utils.psLog("Inside PopularHotelViewModel");

        backgroundTaskHandler = new PopularHotelTaskHandler(repository);

        popularHotelData = Transformations.switchMap(popularHotelObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Popular Hotel List");

            return repository.getPopularHotelList(Config.API_KEY, Utils.checkUserId(obj.loginUserId), obj.offset);
        });
    }


    //region Public Methods


    // Trending News List

    public LiveData<Resource<List<Hotel>>> getPopularHotelList() {

        return popularHotelData;

    }

    public void setPopularHotelList(String limit, String offset, String loginUserId) {

        PopularHotelLoadingHolder holder = new PopularHotelLoadingHolder(limit, offset, loginUserId);

        popularHotelObj.setValue(holder);
    }


    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {
        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(PopularHotelLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

    //end for pagination test


    //endregion
}