package com.panaceasoft.pshotels.viewmodel.review;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.review.ReviewRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.review.hotel.HotelReviewTaskHandler;
import com.panaceasoft.pshotels.ui.review.room.RoomReviewTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.ReviewDetail;
import com.panaceasoft.pshotels.viewobject.ReviewSummaryWithCategoryData;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.ReviewDetailLoadingHolder;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 3/3/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class ReviewViewModel extends ViewModel {

    //region Variables

    private RoomReviewTaskHandler roomReviewTaskHandler;
    private HotelReviewTaskHandler hotelReviewTaskHandler;

    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;
    public String hotelId = "";
    public String roomId = "";

    // For Hotel Review Summary
    private final LiveData<Resource<ReviewSummaryWithCategoryData>> hotelReviewSummaryData;
    private MutableLiveData<ReviewViewModel.TmpDataHolder> hotelReviewSummaryObj = new MutableLiveData<>();

    // For Hotel Review Detail
    private final LiveData<Resource<List<ReviewDetail>>> hotelReviewDetailData;
    private MutableLiveData<ReviewViewModel.TmpDataHolder> hotelReviewDetailObj = new MutableLiveData<>();

    // For Room Review Summary
    private final LiveData<Resource<ReviewSummaryWithCategoryData>> roomReviewSummaryData;
    private MutableLiveData<ReviewViewModel.TmpDataHolder> roomReviewSummaryObj = new MutableLiveData<>();

    // For Room Review Detail
    private final LiveData<Resource<List<ReviewDetail>>> roomReviewDetailData;
    private MutableLiveData<ReviewViewModel.TmpDataHolder> roomReviewDetailObj = new MutableLiveData<>();

    //endregion


    //region Constructor

    @Inject
    ReviewViewModel(ReviewRepository repository) {
        Utils.psLog("Inside ReviewViewModel");

        roomReviewTaskHandler = new RoomReviewTaskHandler( repository );
        hotelReviewTaskHandler = new HotelReviewTaskHandler( repository);

        // Hotel Review Summary
        hotelReviewSummaryData = Transformations.switchMap(hotelReviewSummaryObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("Review Summary List.");
            return repository.getHotelReviewSummary(Config.API_KEY, obj.hotelId);
        });

        // Hotel Review Detail
        hotelReviewDetailData = Transformations.switchMap(hotelReviewDetailObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("Hotel Review Detail.");
            return repository.getHotelReviewDetail(Config.API_KEY, obj.hotelId, obj.offset );
        });

        // Room Review Summary
        roomReviewSummaryData = Transformations.switchMap(roomReviewSummaryObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("Room Review Summary.");
            return repository.getRoomReviewSummary(Config.API_KEY, obj.roomId);
        });

        // Room Review Detail
        roomReviewDetailData = Transformations.switchMap(roomReviewDetailObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("Room Review Detail.");
            return repository.getRoomReviewDetail(Config.API_KEY, obj.roomId, obj.offset );
        });
    }

    //endregion


    //region Public Methods

    // Hotel Review Summary
    public LiveData<Resource<ReviewSummaryWithCategoryData>> getHotelReviewSummaryData() {

        return hotelReviewSummaryData;

    }
    public void setHotelReviewSummaryObj(String hotelId) {

        ReviewViewModel.TmpDataHolder tmpDataHolder = new ReviewViewModel.TmpDataHolder();
        tmpDataHolder.hotelId = hotelId;

        hotelReviewSummaryObj.setValue(tmpDataHolder);

    }

    // Hotel Review Deatil
    public LiveData<Resource<List<ReviewDetail>>> getHotelReviewDetailData() {

        return hotelReviewDetailData;

    }
    public void setHotelReviewDetailObj(String hotelId, String offset) {

        ReviewViewModel.TmpDataHolder tmpDataHolder = new ReviewViewModel.TmpDataHolder();
        tmpDataHolder.hotelId = hotelId;
        tmpDataHolder.offset = offset;

        hotelReviewDetailObj.setValue(tmpDataHolder);

    }

    // Room Review Summary
    public LiveData<Resource<ReviewSummaryWithCategoryData>> getRoomReviewSummaryData() {

        return roomReviewSummaryData;

    }
    public void setRoomReviewSummaryObj(String roomId) {

        ReviewViewModel.TmpDataHolder tmpDataHolder = new ReviewViewModel.TmpDataHolder();
        tmpDataHolder.roomId = roomId;

        roomReviewSummaryObj.setValue(tmpDataHolder);

    }

    // Room Review Deatil
    public LiveData<Resource<List<ReviewDetail>>> getRoomReviewDetailData() {

        return roomReviewDetailData;

    }
    public void setRoomReviewDetailObj(String roomId, String offset) {

        ReviewViewModel.TmpDataHolder tmpDataHolder = new ReviewViewModel.TmpDataHolder();
        tmpDataHolder.roomId = roomId;
        tmpDataHolder.offset = offset;

        roomReviewDetailObj.setValue(tmpDataHolder);

    }

    //for review list pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatusOfRoomReviews() {

        return roomReviewTaskHandler.getLoadingState();
    }

    public void loadNextPageRoomReviews(ReviewDetailLoadingHolder nextPageHolder) {

        if (nextPageHolder == null ) {
            return;
        }

        roomReviewTaskHandler.getNextPage(nextPageHolder);
    }

    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatusOfHotelReviews() {

        return hotelReviewTaskHandler.getLoadingState();
    }

    public void loadNextPageHotelReviews(ReviewDetailLoadingHolder nextPageHolder) {

        if (nextPageHolder == null ) {
            return;
        }

        hotelReviewTaskHandler.getNextPage(nextPageHolder);
    }

    //endregion

    //region Holder

    class TmpDataHolder {
        public String hotelId = "";
        public String roomId = "";
        public String loginUserId = "";
        public String offset = "";
        public Boolean isConnected = false;
    }

    //endregion
}
