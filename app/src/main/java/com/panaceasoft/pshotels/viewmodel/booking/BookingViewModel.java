package com.panaceasoft.pshotels.viewmodel.booking;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.booking.BookingRepository;
import com.panaceasoft.pshotels.repository.inquiry.InquiryRepository;
import com.panaceasoft.pshotels.ui.booking.BookingBackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.inquiry.InquiryBackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Booking;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.PopularHotelLoadingHolder;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 5/5/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class BookingViewModel extends ViewModel {

    private final BookingBackgroundTaskHandler backgroundTaskHandler;
    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;

    // for booking list
    private final LiveData<Resource<List<Booking>>> bookingData;
    private MutableLiveData<TmpDataHolder> bookingObj = new MutableLiveData<>();


    // for booking by id
    private final LiveData<Booking> bookingByIdData;
    private MutableLiveData<TmpDataHolder> bookingByIdObj = new MutableLiveData<>();

    //booking data
    public String hotelId = "";
    public String roomId = "";
    public String userId = "";
    public String bookingId = "";

    public String userName = "";
    public String userEmail = "";
    public String userPhone = "";
    public String adultCount = "";
    public String kidCount = "";
    public String startDate = "";
    public String endDate = "";
    public String extraBed = "";
    public String remark = "";

    @Inject
    BookingViewModel(BookingRepository repository) {

        Utils.psLog("Inside BookingViewModel");
        backgroundTaskHandler = new BookingBackgroundTaskHandler(repository);


        bookingData = Transformations.switchMap(bookingObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Getting Booking List");

            return repository.getBookingList(Config.API_KEY, Utils.checkUserId(obj.loginUserId), obj.offset);
        });


        bookingByIdData = Transformations.switchMap(bookingByIdObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Getting Booking By Id");

            return repository.getBookingById(obj.bookingId);
        });
    }


    // Booking List

    public LiveData<Resource<List<Booking>>> getBookingList() {

        return bookingData;

    }

    public void setBookingObj(String limit, String offset, String loginUserId) {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.loginUserId = loginUserId;
        tmpDataHolder.offset = offset;
        bookingObj.setValue(tmpDataHolder);
    }

    public LiveData<Booking> getBookingById() {

        return bookingByIdData;

    }

    public void setBookingByIdObj(String bookingId) {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.bookingId = bookingId;

        bookingByIdObj.setValue(tmpDataHolder);
    }

    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadingStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(String loginUserId, String limit, String offset ) {

        if (loginUserId == null || offset == null || limit == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(Config.API_KEY, loginUserId, limit, offset);
    }

    public void postInquiry(String apiKey,
                            String loginUserId,
                            String userName,
                            String userEmail,
                            String userPhone,
                            String hotelId,
                            String roomId,
                            String adultCount,
                            String kidCount,
                            String startDate,
                            String endDate,
                            String extraBed,
                            String remark) {

        backgroundTaskHandler.postBooking(
                apiKey,
                loginUserId,
                userName,
                userEmail,
                userPhone,
                hotelId,
                roomId,
                adultCount,
                kidCount,
                startDate,
                endDate,
                extraBed,
                remark
        );
    }


    //region Holder

    class TmpDataHolder {

        public String offset = "";
        public String loginUserId = "";
        public String bookingId = "";

    }

    //endregion
}
