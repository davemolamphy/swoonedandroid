package com.panaceasoft.pshotels.viewmodel.review;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.review.ReviewRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.review.room.RoomReviewTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.ReviewCategory;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.SyncUserRatingHolder;

import java.util.List;

import javax.inject.Inject;

public class SubmitReviewViewModel extends ViewModel {

    private final RoomReviewTaskHandler backgroundTaskHandler;
    public boolean isLoading = false;

    public String roomId = "";
    public String roomName = "";
    public String hotelId = "";
    public String userId = "";
    public String reviewDesc = "";

    // For Review Category
    private final LiveData<Resource<List<ReviewCategory>>> reviewCategoryData;
    private MutableLiveData<TmpDataHolder> reviewCategoryObj = new MutableLiveData<>();
    public List<ReviewCategory> reviewCategories;

    @Inject
    SubmitReviewViewModel(ReviewRepository repository) {

        Utils.psLog("Inside SubmitReviewViewModel");
        backgroundTaskHandler = new RoomReviewTaskHandler(repository);

        // For Review Category
        reviewCategoryData = Transformations.switchMap(reviewCategoryObj, obj -> {
            if (obj == null) {

                return AbsentLiveData.create();
            }

            Utils.psLog("For Review Category");

            return repository.getAllReviewCategory(Config.API_KEY);
        });
    }

    //for pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadingStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void postSubmitReview(String apiKey, String roomId, String userId, String reviewDesc, SyncUserRatingHolder syncUserRatingHolder) {

        backgroundTaskHandler.postReview(
                apiKey,
                roomId,
                userId,
                reviewDesc,
                syncUserRatingHolder
        );
    }

    public LiveData<Resource<List<ReviewCategory>>> getAllReviewCategory() {

        return reviewCategoryData;
    }

    public void setReviewCategory() {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        reviewCategoryObj.setValue(tmpDataHolder);
    }

    //region Holder

    class TmpDataHolder {
        public Boolean isConnected = false;
    }

    //endregion
}
