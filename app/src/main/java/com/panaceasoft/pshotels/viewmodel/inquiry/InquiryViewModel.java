package com.panaceasoft.pshotels.viewmodel.inquiry;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.repository.inquiry.InquiryRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.inquiry.InquiryBackgroundTaskHandler;
import com.panaceasoft.pshotels.utils.Utils;

import javax.inject.Inject;

public class InquiryViewModel extends ViewModel {

    private final InquiryBackgroundTaskHandler backgroundTaskHandler;
    public boolean isLoading = false;

    //inquiry data
    public String hotelId = "";
    public String roomId = "";
    public String userId = "";
    public String inqName = "";
    public String inqDesc = "";
    public String contactName = "";
    public String contactEmail = "";
    public String contactPhone = "";

    @Inject
    InquiryViewModel(InquiryRepository repository) {

        Utils.psLog("Inside InquiryViewModel");
        backgroundTaskHandler = new InquiryBackgroundTaskHandler(repository);
    }

    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadingStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void postInquiry(String apiKey,
                            String hotelId,
                            String roomId,
                            String userId,
                            String inqName,
                            String inqDesc,
                            String contactName,
                            String contactEmail,
                            String contactPhone) {

        backgroundTaskHandler.postInquiry(
                apiKey,
                hotelId,
                roomId,
                userId,
                inqName,
                inqDesc,
                contactName,
                contactEmail,
                contactPhone
        );
    }
}