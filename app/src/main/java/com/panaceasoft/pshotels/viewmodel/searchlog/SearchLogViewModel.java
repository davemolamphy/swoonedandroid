package com.panaceasoft.pshotels.viewmodel.searchlog;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.repository.searchlog.SearchLogRepository;
import com.panaceasoft.pshotels.ui.hotel.search.InsertSearchLogTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.SearchLog;

import javax.inject.Inject;

public class SearchLogViewModel extends ViewModel {

    //region Variables
    private final LiveData<SearchLog> searchLogLiveData;
    private MutableLiveData<String> searchLogObj = new MutableLiveData<>();

    private InsertSearchLogTaskHandler backgroundTaskHandler;
    //endregion

    @Inject
    SearchLogViewModel(SearchLogRepository repository) {
        Utils.psLog("SearchLog ViewModel...");

        backgroundTaskHandler = new InsertSearchLogTaskHandler(repository);

        searchLogLiveData = Transformations.switchMap(searchLogObj, searchLog -> {
            if ( searchLog.isEmpty()) {

                return AbsentLiveData.create();
            }

            return repository.getSearchLog();
        });
    }

    //region Public Methods

    public void setSearchLogObj(String searchLogObj) {
        this.searchLogObj.setValue(searchLogObj);
    }

    public LiveData<SearchLog> getSearchLogLiveData() {
        return searchLogLiveData;
    }

    public void insertSearchLog(SearchLog searchLog) {

        if (searchLog == null ) {
            return;
        }
        backgroundTaskHandler.insertSearchLog(searchLog);
    }

    //endregion
}
