package com.panaceasoft.pshotels.viewmodel.room;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.room.RoomRepository;
import com.panaceasoft.pshotels.ui.hotel.HotelTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.RoomTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import javax.inject.Inject;

public class RoomViewModel extends ViewModel {

    private final LiveData<Resource<Room>> roomData;
    private MutableLiveData<TmpDataHolder> roomObj = new MutableLiveData<>();
    private final RoomTaskHandler backgroundTaskHandler;
    public String roomId;
    public String loginUserId;
    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;

    @Inject
    RoomViewModel(RoomRepository repository) {

        Utils.psLog("Inside RoomViewModel");

        backgroundTaskHandler = new RoomTaskHandler(repository);

        // Get Room Data By Id
        roomData = Transformations.switchMap(roomObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Get Room Data By Id");

            return repository.getRoomByRoomId(Config.API_KEY, obj.loginUserId, obj.roomId);
        });
    }

    public LiveData<Resource<Room>> getRoomData() {

        return roomData;
    }

    public void setRoomData(String loginUserId, String roomId) {

        TmpDataHolder holder = new TmpDataHolder();
        holder.roomId = roomId;
        holder.loginUserId = loginUserId;
        roomObj.setValue(holder);
    }

    public void doRoomTouch(String loginUserId, String hotelId) {
        if (loginUserId == null || hotelId == null) return;

        if (loginUserId.equals("") || hotelId.equals("")) return;

        backgroundTaskHandler.doRoomTouch(loginUserId, hotelId);
    }

    //region Holder

    class TmpDataHolder {

        public String roomId = "";
        public String loginUserId = "";
    }

    //endregion
}