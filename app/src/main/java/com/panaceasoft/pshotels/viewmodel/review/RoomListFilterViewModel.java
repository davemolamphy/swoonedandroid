package com.panaceasoft.pshotels.viewmodel.review;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.room.RoomRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.review.entry.RoomByHotelIdTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RoomListFilterViewModel extends ViewModel {

    // region variables

    private RoomByHotelIdTaskHandler backgroundTaskHandler;

    private final LiveData<Resource<List<Room>>> roomListData;
    private MutableLiveData<TmpDataHolder> roomListObj = new MutableLiveData<>();
    private List<Room> filteredRoomListData = new ArrayList<>();

    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;

    public String hotelId = "";
    public boolean isFilter = false;
    public String query = "";

    // endregion variables

    @Inject
    public RoomListFilterViewModel(RoomRepository roomRepository) {
        Utils.psLog("Inside RecommendedHotelViewModel");

        backgroundTaskHandler = new RoomByHotelIdTaskHandler(roomRepository);

        roomListData = Transformations.switchMap(roomListObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Room By Hotel Id List");

            return roomRepository.getAllRoomByHotelIdList(Config.API_KEY, obj.hotelId);
        });
    }

    public LiveData<Resource<List<Room>>> getRoomListData() {

        return roomListData;
    }

    public void setRoomByHotelIdList(String hotelId) {

        TmpDataHolder holder = new TmpDataHolder();
        holder.hotelId = hotelId;
        roomListObj.setValue(holder);
    }

    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(RoomByHotelIdLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

    public List<Room> getRoomList() {

        if (roomListData != null
                && roomListData.getValue() != null
                ) {

            if (isFilter) {

                List<Room> tmpRoomList = roomListData.getValue().data;

                if (tmpRoomList != null && tmpRoomList.size() > 0) {
                    filteredRoomListData.clear();

                    for (int i = 0; i < tmpRoomList.size(); i++) {
                        if (tmpRoomList.get(i).room_name.toLowerCase().contains(query.toLowerCase())) {

                            filteredRoomListData.add(tmpRoomList.get(i));
                        }
                    }

                    return filteredRoomListData;

                }else {
                    return new ArrayList<>();
                }
            } else {

                return roomListData.getValue().data;
            }
        }else {
            return new ArrayList<>();
        }

    }

    //region Holder

    class TmpDataHolder {

        public String hotelId = "";
        public String loginUserId = "";
    }

    //endregion
}
