package com.panaceasoft.pshotels.viewmodel.hotelInfo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotelinfo.HotelFeatureRepository;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.HotelFeatures;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class HotelFeatureViewModel extends ViewModel {

    //region Variables

    // For Hotel Feature by Hotel Id
    private final LiveData<Resource<List<HotelFeatures>>> hotelFeatureByHotelIdData;
    private MutableLiveData<TmpDataHolder> hotelFeatureByHotelIdObj = new MutableLiveData<>();


    // For Hotel Feature by City Id
    private final LiveData<Resource<List<HotelFeatures>>> hotelFeatureByCityIdData;
    private MutableLiveData<TmpDataHolder> hotelFeatureByCityIdObj = new MutableLiveData<>();


    //endregion


    //region Constructor

    @Inject
    HotelFeatureViewModel(HotelFeatureRepository repository) {
        Utils.psLog("Inside HotelFeatureViewModel");


        // By Hotel Id
        hotelFeatureByHotelIdData = Transformations.switchMap(hotelFeatureByHotelIdObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog(" By Hotel Id.");
            return repository.getHotelFeaturesByHotelId(Config.API_KEY, obj.hotelId);
        });

        // By City Id
        hotelFeatureByCityIdData = Transformations.switchMap(hotelFeatureByCityIdObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("By City Id.");
            return repository.getHotelFeaturesByCityId(Config.API_KEY, obj.cityId);
        });

    }

    //endregion


    //region Public Methods

    public LiveData<Resource<List<HotelFeatures>>> getHotelFeatureByHotelIdData() {

        return hotelFeatureByHotelIdData;

    }

    public void setHotelFeaturesByHotelIdObj(String hotelId) {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.hotelId = hotelId;

        hotelFeatureByHotelIdObj.setValue(tmpDataHolder);

    }


    public LiveData<Resource<List<HotelFeatures>>> getHotelFeaturesByCityIdData() {

        return hotelFeatureByCityIdData;

    }

    public void setHotelFeaturesByCityIdObj(String cityId) {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.cityId = cityId;

        hotelFeatureByCityIdObj.setValue(tmpDataHolder);

    }

    //endregion

    //region Holder

    class TmpDataHolder {
        public String hotelId = "";
        public String cityId = "";
        public String loginUserId = "";
        public String offset = "";
        public Boolean isConnected = false;


    }

    //endregion
}
