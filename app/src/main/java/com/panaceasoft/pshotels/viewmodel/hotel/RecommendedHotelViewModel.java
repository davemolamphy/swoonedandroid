package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.recommended.RecommendedHotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RecommendedHotelLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class RecommendedHotelViewModel extends ViewModel {

    // region task handler

    private RecommendedHotelTaskHandler backgroundTaskHandler;

    // for trending news
    private final LiveData<Resource<List<Hotel>>> recommendedHotelData;
    private MutableLiveData<RecommendedHotelLoadingHolder> recommendedHotelObj = new MutableLiveData<>();


    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;
    public String loginUserId = "";

    //endregion

    @Inject
    RecommendedHotelViewModel(HotelRepository repository) {
        Utils.psLog("Inside RecommendedHotelViewModel");

        backgroundTaskHandler = new RecommendedHotelTaskHandler(repository);

        recommendedHotelData = Transformations.switchMap(recommendedHotelObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Recommended Hotel List");

            return repository.getRecommendedHotelList(Config.API_KEY, Utils.checkUserId(obj.loginUserId), obj.offset);
        });
    }


    //region Public Methods


    // Trending News List

    public LiveData<Resource<List<Hotel>>> getRecommendedHotelList() {

        return recommendedHotelData;

    }

    public void setRecommendedHotelList(String loginUserId, String limit, String offset) {

        RecommendedHotelLoadingHolder holder = new RecommendedHotelLoadingHolder(
                loginUserId,
                limit,
                offset
        );

        recommendedHotelObj.setValue(holder);
    }

    //for news pagination
    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {
        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(RecommendedHotelLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

    //end for pagination test


    //endregion
}