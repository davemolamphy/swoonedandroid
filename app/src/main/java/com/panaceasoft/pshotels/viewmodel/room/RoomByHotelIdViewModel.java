package com.panaceasoft.pshotels.viewmodel.room;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.room.RoomRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.review.entry.RoomByHotelIdTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Room;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.RoomByHotelIdLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class RoomByHotelIdViewModel extends ViewModel {

    // region variables

    private RoomByHotelIdTaskHandler backgroundTaskHandler;

    private final LiveData<Resource<List<Room>>> roomByHotelIdData;
    private MutableLiveData<RoomByHotelIdLoadingHolder> roomByHotelIdObj = new MutableLiveData<>();

    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;
    public String loginUserId = "";
    public String hotelId = "";

    // endregion variables

    @Inject
    public RoomByHotelIdViewModel(RoomRepository roomRepository) {
        Utils.psLog("Inside RecommendedHotelViewModel");

        backgroundTaskHandler = new RoomByHotelIdTaskHandler(roomRepository);

        roomByHotelIdData = Transformations.switchMap(roomByHotelIdObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Room By Hotel Id List");

            return roomRepository.getRoomByHotelIdList(obj);
        });
    }

    public LiveData<Resource<List<Room>>> getRoomByHotelIdList() {

        return roomByHotelIdData;
    }

    public void setRoomByHotelIdList(String limit, String offset, String loginUserId, String hotelId) {

        RoomByHotelIdLoadingHolder holder = new RoomByHotelIdLoadingHolder(
                limit,
                offset,
                loginUserId,
                hotelId,
                Config.API_KEY
        );
        roomByHotelIdObj.setValue(holder);
    }

    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(RoomByHotelIdLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

}
