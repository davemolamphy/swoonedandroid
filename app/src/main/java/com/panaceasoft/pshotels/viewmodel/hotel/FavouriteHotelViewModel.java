package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.favourite.FavouriteHotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;
import com.panaceasoft.pshotels.viewobject.holder.FavouriteHotelLoadingHolder;

import java.util.List;

import javax.inject.Inject;

public class FavouriteHotelViewModel extends ViewModel {

    // region task handler

    private FavouriteHotelTaskHandler backgroundTaskHandler;

    private final LiveData<Resource<List<Hotel>>> favouriteHotelData;
    private MutableLiveData<FavouriteHotelLoadingHolder> favouriteHotelObj = new MutableLiveData<>();


    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;
    public String loginUserId = "";

    //endregion

    @Inject
    FavouriteHotelViewModel(HotelRepository repository) {

        Utils.psLog("Inside FavouriteHotelViewModel");

        backgroundTaskHandler = new FavouriteHotelTaskHandler(repository);

        favouriteHotelData = Transformations.switchMap( favouriteHotelObj, obj->{

            if (obj == null) {
                return AbsentLiveData.create();
            }

            return repository.getFavouriteHotelList(
                    Config.API_KEY,
                    Utils.checkUserId( obj.loginUserId ),
                    obj.offset
            );
        });
    }

    public LiveData<Resource<List<Hotel>>> getFavouriteHotelList() {

        return favouriteHotelData;
    }

    public void setFavouriteHotelList(String loginUserId,
                                      String limit,
                                      String offset) {

        FavouriteHotelLoadingHolder holder = new FavouriteHotelLoadingHolder(limit, offset, loginUserId);

        favouriteHotelObj.setValue(holder);
    }

    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {

        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(FavouriteHotelLoadingHolder nextPageHolder) {

        if (nextPageHolder == null ) {

            return;
        }

        backgroundTaskHandler.getNextPage(nextPageHolder);
    }
}
