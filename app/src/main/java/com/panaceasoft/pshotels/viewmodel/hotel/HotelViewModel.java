package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.hotel.HotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import javax.inject.Inject;

public class HotelViewModel extends ViewModel {

    private final LiveData<Resource<Hotel>> hotelData;
    private MutableLiveData<TmpDataHolder> hotelObj = new MutableLiveData<>();
    private final HotelTaskHandler backgroundTaskHandler;
    public String hotelId;
    public String loginUserId;
    public boolean isLoading = false;

    @Inject
    HotelViewModel(HotelRepository repository) {

        Utils.psLog("Inside HotelViewModel");

        backgroundTaskHandler = new HotelTaskHandler(repository);

        hotelData = Transformations.switchMap(hotelObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            Utils.psLog("Get Hotel By Id");

            return repository.getHotelByHotelId(Config.API_KEY, obj.loginUserId, obj.hotelId);
        });
    }

    public LiveData<Resource<Hotel>> getHotelData() {

        return hotelData;
    }

    public void setHotelData(String loginUserId, String hotelId) {

        TmpDataHolder holder = new TmpDataHolder();
        holder.hotelId = hotelId;
        holder.loginUserId = loginUserId;
        hotelObj.setValue(holder);
    }


    public void doFavourite(String loginUserId, String newsId) {
        if (loginUserId == null || newsId == null) return;

        if (loginUserId.equals("") || newsId.equals("")) return;

        backgroundTaskHandler.doFavourite(loginUserId, newsId);
    }

    public void doTouch(String loginUserId, String hotelId) {
        if (loginUserId == null || hotelId == null) return;

        if (loginUserId.equals("") || hotelId.equals("")) return;

        backgroundTaskHandler.doHotelTouch(loginUserId, hotelId);
    }


    //region Holder

    class TmpDataHolder {

        public String hotelId = "";
        public String loginUserId = "";
    }

    //endregion
}