package com.panaceasoft.pshotels.viewmodel.city;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.city.CityRepository;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.City;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 4/6/18.
 * Contact Email : teamps.is.cool@gmail.com
 */

public class CityViewModel extends ViewModel {

    private final LiveData<Resource<List<City>>> cityListData;
    private List<City> filteredCityListData = new ArrayList<>();
    private MutableLiveData<TmpDataHolder> cityListObj = new MutableLiveData<>();


    public boolean isFilter = false;
    public String query = "";
    public String cityId;
    public String loginUserId;

    @Inject
    CityViewModel(CityRepository repository) {

        Utils.psLog("Inside CityViewModel");

        cityListData = Transformations.switchMap(cityListObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            return repository.getAllCityList(Config.API_KEY);
        });
    }

    public List<City> getFilteredCityListData() {

        if (cityListData != null && cityListData.getValue() != null) {

            if (isFilter) {

                List<City> tmpCityList = cityListData.getValue().data;

                if (tmpCityList != null && tmpCityList.size() > 0) {

                    filteredCityListData.clear();

                    for (int i = 0; i < tmpCityList.size(); i++) {
                        if (tmpCityList.get(i).city_name.toLowerCase().contains(query.toLowerCase())) {
                            filteredCityListData.add(tmpCityList.get(i));
                        }
                    }

                    return filteredCityListData;

                } else {

                    return new ArrayList<>();

                }


            } else {
                return cityListData.getValue().data;
            }
        } else {
            return new ArrayList<>();
        }
    }

    public LiveData<Resource<List<City>>> getCityListData() {

        return cityListData;
    }

    public void setCityListData() {

        TmpDataHolder holder = new TmpDataHolder();

        cityListObj.setValue(holder);
    }

    //region Holder

    class TmpDataHolder {

        public String cityId = "";
        public String loginUserId = "";
    }

    //endregion
}