package com.panaceasoft.pshotels.viewmodel.hotel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.hotel.HotelRepository;
import com.panaceasoft.pshotels.ui.common.BackgroundTaskHandler;
import com.panaceasoft.pshotels.ui.hotel.search.SearchHotelTaskHandler;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Hotel;
import com.panaceasoft.pshotels.viewobject.SearchHotelLoadingHolder;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 3/31/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class SearchHotelViewModel extends ViewModel {

    //region Variables

    private SearchHotelTaskHandler backgroundTaskHandler;

    // for news by search
    private final LiveData<Resource<List<Hotel>>> searchHotelData;
    private MutableLiveData<SearchHotelLoadingHolder> searchHotelObj = new MutableLiveData<>();


    public int offset = 0;
    public boolean forceEndLoading = false;
    public boolean isLoading = false;
    public String cityId = "";
    public String hotelName = "";
    public String hotelStarRating = "";
    public String hotelMinPrice = "";
    public String hotelMaxPrice = "";
    public String filterByInfoType = "";
    public String minUserRating = "";

    //endregion


    //region Constructor

    @Inject
    SearchHotelViewModel(HotelRepository repository) {
        Utils.psLog("Inside SearchNewsViewModel");

        backgroundTaskHandler = new SearchHotelTaskHandler(repository);

        // News By Search
        searchHotelData = Transformations.switchMap(searchHotelObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            return repository.postSearchHotel(obj);
        });


    }
    //endregion


    //region Public Methods


    // News List By Search
    public LiveData<Resource<List<Hotel>>> getSearchHotelList() {

        return searchHotelData;

    }

    public void setSearchHotelList(String loginUserId,
                                   String offset,
                                   String cityId,
                                   String hotelName,
                                   String hotelStarRating,
                                   String hotelMinPrice,
                                   String hotelMaxPrice,
                                   String filterByInfoType,
                                   String minUserRating
    ) {

        SearchHotelLoadingHolder holder = new SearchHotelLoadingHolder(Config.SEARCH_COUNT + "",
                offset,
                Config.API_KEY,
                loginUserId,
                cityId,
                hotelName,
                hotelStarRating,
                hotelMinPrice,
                hotelMaxPrice,
                filterByInfoType,
                minUserRating
        );

        searchHotelObj.setValue(holder);

    }


    //for news pagination

    public LiveData<BackgroundTaskHandler.LoadingState> getLoadMoreStatus() {
        return backgroundTaskHandler.getLoadingState();
    }

    public void loadNextPage(SearchHotelLoadingHolder nextPageHolder) {

        if (nextPageHolder == null) {
            return;
        }
        backgroundTaskHandler.getNextPage(nextPageHolder);
    }

    //end for pagination test


    //endregion

}
