package com.panaceasoft.pshotels.viewmodel.image;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.image.ImageRepository;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Image;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 12/8/17.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class ImageViewModel extends ViewModel {


    //region Variables

    // Get Image Video List
    private final LiveData<Resource<List<Image>>> imageListLiveData;
    private MutableLiveData<String> imageParentObj = new MutableLiveData<>();

    public String newsId;
    public List<Image> newsImageList;
    public String imgId;

    //endregion


    //region Constructors

    @Inject
    ImageViewModel(ImageRepository repository) {
        Utils.psLog("ImageViewModel...");

        imageListLiveData = Transformations.switchMap(imageParentObj, newsId -> {
            if (newsId.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.getImageList(Config.API_KEY, newsId);
        });

    }

    //endregion


    //region Methods

    public void setImageParentId(String imageParentId) {
        this.imageParentObj.setValue(imageParentId);
    }

    public LiveData<Resource<List<Image>>> getImageListLiveData() {
        return imageListLiveData;
    }

    //endregion

}
