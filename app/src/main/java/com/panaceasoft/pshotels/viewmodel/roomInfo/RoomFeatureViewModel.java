package com.panaceasoft.pshotels.viewmodel.roomInfo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.roominfo.RoomFeatureRepository;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.RoomFeatures;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 2/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class RoomFeatureViewModel extends ViewModel {

    //region Variables

    // For Room Feature by Room Id
    private final LiveData<Resource<List<RoomFeatures>>> roomFeatureByRoomIdData;
    private MutableLiveData<TmpDataHolder> roomFeatureByRoomIdObj = new MutableLiveData<>();

    //endregion


    //region Constructor

    @Inject
    RoomFeatureViewModel(RoomFeatureRepository repository) {
        Utils.psLog("Inside RoomFeatureViewModel");


        // Room Feature By Room Id
        roomFeatureByRoomIdData = Transformations.switchMap(roomFeatureByRoomIdObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("Room Feature By Room Id");
            return repository.getRoomFeaturesByRoomId(Config.API_KEY, obj.roomId);
        });
    }

    //endregion


    //region Public Methods

    public LiveData<Resource<List<RoomFeatures>>> getRoomFeatureByRoomIdData() {

        return roomFeatureByRoomIdData;

    }
    public void setRoomFeaturesByRoomIdObj(String roomId) {

        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.roomId = roomId;

        roomFeatureByRoomIdObj.setValue(tmpDataHolder);

    }

    //endregion

    //region Holder

    class TmpDataHolder {
        public String roomId = "";
        public String loginUserId = "";
        public String offset = "";
        public Boolean isConnected = false;

    }

    //endregion
}
