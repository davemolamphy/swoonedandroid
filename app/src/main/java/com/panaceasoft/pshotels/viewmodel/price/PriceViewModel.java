package com.panaceasoft.pshotels.viewmodel.price;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.repository.price.PriceRepository;
import com.panaceasoft.pshotels.utils.AbsentLiveData;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.Price;
import com.panaceasoft.pshotels.viewobject.common.Resource;

import javax.inject.Inject;

/**
 * Created by Panacea-Soft on 4/27/18.
 * Contact Email : teamps.is.cool@gmail.com
 */


public class PriceViewModel extends ViewModel {


    //region Variables

    // Get Price
    private final LiveData<Resource<Price>> priceLiveData;
    private MutableLiveData<String> aboutUsObj = new MutableLiveData<>();

    //endregion


    //region Constructors

    @Inject
    PriceViewModel(PriceRepository repository) {
        Utils.psLog("Price ViewModel...");

        priceLiveData = Transformations.switchMap(aboutUsObj, newsId -> {
            if (newsId.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.getMaxPrice(Config.API_KEY);
        });

    }

    //endregion


    //region Public Methods

    public void setPriceObj(String aboutUsObj) {
        this.aboutUsObj.setValue(aboutUsObj);
    }

    public LiveData<Resource<Price>> getPriceData() {
        return priceLiveData;
    }

    //endregion

}