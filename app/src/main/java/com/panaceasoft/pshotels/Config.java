package com.panaceasoft.pshotels;

/**
 * Created by Panacea-Soft on 7/15/15.
 * Contact Email : teamps.is.cool@gmail.com
 */

public class Config{

    /* APP Setting */
    public static boolean IS_DEVELOPMENT = true; // set false, your app is production


    /* API Related */

    public static final String APP_API_URL = "http://www.panacea-soft.com/pshotels-dev/index.php/";

    public static final String APP_IMAGES_URL = "http://www.panacea-soft.com/pshotels-dev/uploads/";


    public static final String YOUTUBE_IMAGE_BASE_URL = "https://img.youtube.com/vi/%s/0.jpg";

    public static final String GOOGLE_API_KEY = "AIzaSyBO3v9798So2dlAtWdWgM4NSORd6j8l8p0";

    public static final String API_KEY = "teampsisthebest"; // If you change here, you need to update in server.


    /* Admob Setting */
    public static final Boolean SHOW_ADMOB = false;

//
//    /* Filter by user subscribed categories */
//    public static final String IS_FAVOURITE_FILTER = "0"; // Indicate need to filter with subscribed categories or not ( "0" not filter, "1" filter )
//
//    public static final String IS_EDITOR_PICKED_FILTER = "0"; // Indicate need to filter with subscribed categories or not ( "0" not filter, "1" filter )
//
//    public static final String IS_TRENDING_FILTER = "1"; // Indicate need to filter with subscribed categories or not ( "0" not filter, "1" filter )
//

    /* Loading Limit Count Setting */
    public static final int API_SERVICE_CACHE_LIMIT = 5; // Minutes Cache

    public static final int BOOKING_COUNT = 10; // Recommended -> 10

    public static final int SEARCH_COUNT = 10; // Recommended -> 10

    public static final int REVIEW_COUNT = 10; // Recommended -> 10

    public static final int RECOMMENDED_HOTEL_COUNT = 10; // Recommended -> 10

    public static final int POPULAR_HOTEL_COUNT = 10; // Recommended -> 10

    public static final int PROMOTION_HOTEL_COUNT = 10; // Recommended -> 10

    public static final int ROOM_BY_HOTEL_COUNT = 10; // Recommended -> 10

    public static final int FAVOURITE_HOTEL_COUNT = 10; // Recommended -> 10
}
