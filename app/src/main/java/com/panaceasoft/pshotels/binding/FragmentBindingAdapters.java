package com.panaceasoft.pshotels.binding;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.databinding.BindingAdapter;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.panaceasoft.pshotels.Config;
import com.panaceasoft.pshotels.R;
import com.panaceasoft.pshotels.utils.Utils;
import com.panaceasoft.pshotels.viewobject.common.SyncStatus;
import javax.inject.Inject;

/**
 * Binding adapters that work with a fragment instance.
 */
public class FragmentBindingAdapters {
    private final Fragment fragment;

    @Inject
    public FragmentBindingAdapters(Fragment fragment) {
        this.fragment = fragment;
    }

    @BindingAdapter("imageUrl")
    public void bindImage(ImageView imageView, String url) {

        if(isValid(imageView, url)) {

            url = Config.APP_IMAGES_URL + url;
            Utils.psLog("Image : " + url);
            Glide.with(fragment).load(url).apply(new RequestOptions()
                    .placeholder(R.drawable.placeholder_image)
                    .centerCrop()
                    .dontAnimate()
                    .dontTransform()).into(imageView);

        } else {

            if(imageView != null) {
                imageView.setImageResource(R.drawable.placeholder_image);
            }

        }

    }

    @BindingAdapter("imageCircleUrl")
    public void bindCircleImage(ImageView imageView, String url) {

        if(isValid(imageView, url)) {

            url = Config.APP_IMAGES_URL + url;

            Glide.with(fragment).load(url).apply(RequestOptions.circleCropTransform()
                    .placeholder(R.drawable.placeholder_circle_image)
            ).into(imageView);

        }else {

            if(imageView != null) {
                imageView.setImageResource(R.drawable.placeholder_circle_image);
            }
        }
    }

    @BindingAdapter("likeImage")
    public void bindLikeImage(ImageView imageView, String isLiked) {

        if(isValid(imageView, isLiked)) {

            switch (isLiked) {
                case SyncStatus.SERVER_SELECTED:
                    imageView.setImageResource(R.drawable.liked);
                    break;
                case SyncStatus.LOCAL_SELECTED:
                    imageView.setImageResource(R.drawable.liked);
                    break;
                case SyncStatus.LOCAL_NOT_SELECTED:
                    imageView.setImageResource(R.drawable.like);
                    break;
                default:
                    imageView.setImageResource(R.drawable.like);
                    break;
            }

        }else {
            if(imageView != null) {
                imageView.setImageResource(R.drawable.like);
            }
        }
    }

    @BindingAdapter("imageProfileUrl")
    public void bindProfileImage(ImageView imageView, String url) {

        if(isValid(imageView, url)) {

            url = Config.APP_IMAGES_URL + url;
            Utils.psLog("Image Url : " + url);

            Glide.with(fragment).load(url).apply(RequestOptions.circleCropTransform()
                    .placeholder(R.drawable.default_profile)
            ).into(imageView);

        }else {

            if(imageView != null) {
                imageView.setImageResource(R.drawable.default_profile);
            }
        }
    }

    @BindingAdapter("font")
    public void setFont(TextView textView, String type) {
        switch (type) {
            case "normal":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO));
                break;
            case "bold":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO), Typeface.BOLD);
                break;
            case "bold_italic":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO), Typeface.BOLD_ITALIC);
                break;
            case "italic":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO), Typeface.ITALIC);
                break;
            case "medium":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO_MEDIUM));
                break;
            case "light":
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO_LIGHT));
                break;
            default:
                textView.setTypeface(Utils.getTypeFace(textView.getContext(), Utils.Fonts.ROBOTO));
                break;
        }

    }

    @BindingAdapter("font")
    public void setFont(Button button, String type) {
        switch (type) {
            case "normal":
                button.setTypeface(Utils.getTypeFace(button.getContext(), Utils.Fonts.ROBOTO));
                break;
            case "medium":
                button.setTypeface(Utils.getTypeFace(button.getContext(), Utils.Fonts.ROBOTO_MEDIUM));
                break;
            case "light":
                button.setTypeface(Utils.getTypeFace(button.getContext(), Utils.Fonts.ROBOTO_LIGHT));
                break;
            default:
                button.setTypeface(Utils.getTypeFace(button.getContext(), Utils.Fonts.ROBOTO));
                break;
        }

    }

    @BindingAdapter("textSize")
    public void setTextSize(TextView textView, String dimenType) {

        float dimenPix = 0;
        Utils.psLog("dimenType " + dimenType);
        switch (dimenType) {
            case "day_big_size" :

                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.day_big_size);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
            case "header":

                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.header);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
            case "sub_header":
                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.sub_header);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);
                break;
            case "tag":
                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.tag);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
            case "date":
                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.date);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
            case "text":
                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
            case "detail_text":
                dimenPix = textView.getResources().getDimensionPixelOffset(R.dimen.detail_text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
        }
    }

    @BindingAdapter("textSize")
    public void setTextSize(EditText editText, String dimenType) {

        float dimenPix = 0;
        switch (dimenType) {
            case "edit_text":

                dimenPix = editText.getResources().getDimensionPixelOffset(R.dimen.edit_text);
                editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
        }
    }

    @BindingAdapter("textSize")
    public void setTextSize(Button button, String dimenType) {

        float dimenPix = 0;
        switch (dimenType) {
            case "button_text":

                dimenPix = button.getResources().getDimensionPixelOffset(R.dimen.button_text);
                button.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimenPix);

                break;
        }
    }

    @BindingAdapter("youTubeImage")
    public void bindYouTubeImage(ImageView imageView, String youTubeId) {

        if(isValid(imageView, youTubeId)) {

            String url = String.format(Config.YOUTUBE_IMAGE_BASE_URL, youTubeId);
            Glide.with(fragment).load(url).apply(new RequestOptions()
                    .placeholder(R.drawable.placeholder_image)
                    .centerCrop()
                    .dontAnimate()
                    .dontTransform()).into(imageView);

        } else {

            if(imageView != null) {
                imageView.setImageResource(R.drawable.placeholder_image);
            }

        }

    }

    @BindingAdapter("categorySelection2")
    public void bindCategorySelection(ImageView imageView, String userSelected) {

        if(isValid(imageView, userSelected)) {

            Utils.psLog("bindCategorySelection" + userSelected);
            switch (userSelected) {
                case SyncStatus.SERVER_SELECTED:
                    imageView.setImageResource(R.drawable.checked);
                    break;
                case SyncStatus.LOCAL_SELECTED:
                    imageView.setImageResource(R.drawable.checked_local);
                    break;
                default:
                    imageView.setImageResource(0);
                    break;
            }
        }
    }

    private Boolean isValid(ImageView imageView, String url) {
        return !(url == null
                || imageView == null
                || fragment == null
                || url.equals(""));
    }


}

